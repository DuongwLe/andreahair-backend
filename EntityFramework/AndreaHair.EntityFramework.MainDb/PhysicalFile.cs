﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class PhysicalFile
    {
        public PhysicalFile()
        {
            Covers = new HashSet<Cover>();
            ProductImages = new HashSet<ProductImage>();
            Supporters = new HashSet<Supporter>();
        }

        public long PhysicalFileId { get; set; }
        public short PhysicalFileTypeId { get; set; }
        public short PhysicalFileStatusId { get; set; }
        public long FileLengthInBytes { get; set; }
        public Guid? FileMd5 { get; set; }
        public short S3BucketId { get; set; }
        public string S3FileKey { get; set; }
        public string PhysicalFileName { get; set; }
        public string PhysicalFileExtension { get; set; }
        public string S3SmallThumbnailFileKey { get; set; }
        public string S3MediumThumbnailFileKey { get; set; }
        public string S3LargeThumbnailFileKey { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual S3Bucket S3Bucket { get; set; }
        public virtual ICollection<Cover> Covers { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }
        public virtual ICollection<Supporter> Supporters { get; set; }
    }
}
