﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class PackingRule
    {
        public short PackingRulesId { get; set; }
        public string PackingRulesName { get; set; }
    }
}
