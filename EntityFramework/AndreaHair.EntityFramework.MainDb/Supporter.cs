﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class Supporter
    {
        public long SupporterId { get; set; }
        public string SupporterName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FacebookUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string WhatsappPhoneNumber { get; set; }
        public long AvatarId { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual PhysicalFile Avatar { get; set; }
    }
}
