﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class PhysicalFileStatus
    {
        public short PhysicalFileStatusId { get; set; }
        public string PhysicalFileStatusName { get; set; }
    }
}
