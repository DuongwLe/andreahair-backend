﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class UserConfirmationType
    {
        public short ConfirmationTypeId { get; set; }
        public string ConfirmationTypeName { get; set; }
    }
}
