﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class Product
    {
        public Product()
        {
            Contacts = new HashSet<Contact>();
            ProductImages = new HashSet<ProductImage>();
        }

        public long ProductId { get; set; }
        public string ProductNameEn { get; set; }
        public string ProductNameRu { get; set; }
        public long ProductTypeId { get; set; }
        public long ProductGroupId { get; set; }
        public long ColorId { get; set; }
        public short MaterialTypeId { get; set; }
        public short HairStyleId { get; set; }
        public short MeasureUnitLengthId { get; set; }
        public decimal FromLength { get; set; }
        public decimal ToLength { get; set; }
        public short MeasureUnitWeightId { get; set; }
        public decimal Weight { get; set; }
        public string Origin { get; set; }
        public short PackingRuleId { get; set; }
        public string VideoUrl { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public bool IsBestSelling { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual Color Color { get; set; }
        public virtual ProductGroup ProductGroup { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }
    }
}
