﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class MaterialType
    {
        public short MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }
    }
}
