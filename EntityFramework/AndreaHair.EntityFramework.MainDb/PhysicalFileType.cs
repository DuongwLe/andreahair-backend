﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class PhysicalFileType
    {
        public short PhysicalFileTypeId { get; set; }
        public string PhysicalFileTypeName { get; set; }
    }
}
