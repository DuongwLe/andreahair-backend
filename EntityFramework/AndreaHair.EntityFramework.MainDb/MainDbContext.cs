﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class MainDbContext : DbContext
    {
        public MainDbContext()
        {
        }

        public MainDbContext(DbContextOptions<MainDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<ColorMapping> ColorMappings { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Cover> Covers { get; set; }
        public virtual DbSet<HairStyle> HairStyles { get; set; }
        public virtual DbSet<MaterialType> MaterialTypes { get; set; }
        public virtual DbSet<MeasureUnitLength> MeasureUnitLengths { get; set; }
        public virtual DbSet<MeasureUnitWeight> MeasureUnitWeights { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<NotificationObject> NotificationObjects { get; set; }
        public virtual DbSet<PackingRule> PackingRules { get; set; }
        public virtual DbSet<PhysicalFile> PhysicalFiles { get; set; }
        public virtual DbSet<PhysicalFileStatus> PhysicalFileStatuses { get; set; }
        public virtual DbSet<PhysicalFileType> PhysicalFileTypes { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductGroup> ProductGroups { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<S3Bucket> S3Buckets { get; set; }
        public virtual DbSet<Subscriber> Subscribers { get; set; }
        public virtual DbSet<Supporter> Supporters { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserConfirmationCode> UserConfirmationCodes { get; set; }
        public virtual DbSet<UserConfirmationType> UserConfirmationTypes { get; set; }
        public virtual DbSet<UserStatus> UserStatuses { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_general_ci");

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("categories");

                entity.HasIndex(e => e.CategoryNameEn, "category_name_en_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.Property(e => e.CategoryId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("category_id");

                entity.Property(e => e.CategoryNameEn)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("category_name_en");

                entity.Property(e => e.CategoryNameRu)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("category_name_ru");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");
            });

            modelBuilder.Entity<Color>(entity =>
            {
                entity.ToTable("colors");

                entity.HasIndex(e => e.ColorNameEn, "color_name_en_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.Property(e => e.ColorId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("color_id");

                entity.Property(e => e.ColorNameEn)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("color_name_en");

                entity.Property(e => e.ColorNameRu)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("color_name_ru");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");
            });

            modelBuilder.Entity<ColorMapping>(entity =>
            {
                entity.HasKey(e => new { e.ColorId, e.ColorCode })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("color_mappings");

                entity.Property(e => e.ColorId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("color_id");

                entity.Property(e => e.ColorCode)
                    .HasMaxLength(128)
                    .HasColumnName("color_code");

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.ColorMappings)
                    .HasForeignKey(d => d.ColorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("color_mappings_ibfk_1");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("contacts");

                entity.HasIndex(e => e.ProductId, "contacts_ibfk1");

                entity.Property(e => e.ContactId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("contact_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("first_name");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("last_name");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("message");

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("phone_number");

                entity.Property(e => e.ProductId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("contacts_ibfk1");
            });

            modelBuilder.Entity<Cover>(entity =>
            {
                entity.ToTable("covers");

                entity.HasIndex(e => e.CoverName, "cover_name_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.HasIndex(e => e.PhysicalFileId, "physical_file_id");

                entity.Property(e => e.CoverId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("cover_id");

                entity.Property(e => e.CoverName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("cover_name");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.PhysicalFileId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("physical_file_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.PhysicalFile)
                    .WithMany(p => p.Covers)
                    .HasForeignKey(d => d.PhysicalFileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("covers_ibfk_1");
            });

            modelBuilder.Entity<HairStyle>(entity =>
            {
                entity.ToTable("hair_styles");

                entity.Property(e => e.HairStyleId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("hair_style_id");

                entity.Property(e => e.HairStyleName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("hair_style_name");
            });

            modelBuilder.Entity<MaterialType>(entity =>
            {
                entity.ToTable("material_type");

                entity.Property(e => e.MaterialTypeId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("material_type_id");

                entity.Property(e => e.MaterialTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("material_type_name");
            });

            modelBuilder.Entity<MeasureUnitLength>(entity =>
            {
                entity.ToTable("measure_unit_lengths");

                entity.Property(e => e.MeasureUnitLengthId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("measure_unit_length_id");

                entity.Property(e => e.MeasureUnitLengthName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("measure_unit_length_name");
            });

            modelBuilder.Entity<MeasureUnitWeight>(entity =>
            {
                entity.ToTable("measure_unit_weights");

                entity.Property(e => e.MeasureUnitWeightId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("measure_unit_weight_id");

                entity.Property(e => e.MeasureUnitWeightName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("measure_unit_weight_name");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.ToTable("notifications");

                entity.HasIndex(e => e.NotificationObjectId, "notification_object_id");

                entity.Property(e => e.NotificationId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("notification_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsRead).HasColumnName("is_read");

                entity.Property(e => e.NotificationObjectId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("notification_object_id");

                entity.Property(e => e.NotifierId).HasColumnName("notifier_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.NotificationObject)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.NotificationObjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("notifications_ibfk_1");
            });

            modelBuilder.Entity<NotificationObject>(entity =>
            {
                entity.ToTable("notification_objects");

                entity.Property(e => e.NotificationObjectId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("notification_object_id");

                entity.Property(e => e.ActualSentAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("actual_sent_at_utc");

                entity.Property(e => e.BodyEn)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("body_en");

                entity.Property(e => e.BodyVi)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("body_vi");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.DataJson)
                    .HasMaxLength(1024)
                    .HasColumnName("data_json");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsSent).HasColumnName("is_sent");

                entity.Property(e => e.SendAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("send_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.SenderId).HasColumnName("sender_id");

                entity.Property(e => e.TitleEn)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("title_en");

                entity.Property(e => e.TitleVi)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("title_vi");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");
            });

            modelBuilder.Entity<PackingRule>(entity =>
            {
                entity.HasKey(e => e.PackingRulesId)
                    .HasName("PRIMARY");

                entity.ToTable("packing_rules");

                entity.Property(e => e.PackingRulesId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("packing_rules_id");

                entity.Property(e => e.PackingRulesName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("packing_rules_name");
            });

            modelBuilder.Entity<PhysicalFile>(entity =>
            {
                entity.ToTable("physical_files");

                entity.HasIndex(e => e.S3BucketId, "s3_bucket_id");

                entity.Property(e => e.PhysicalFileId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("physical_file_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.FileLengthInBytes)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("file_length_in_bytes");

                entity.Property(e => e.FileMd5).HasColumnName("file_md5");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.PhysicalFileExtension)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("physical_file_extension");

                entity.Property(e => e.PhysicalFileName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("physical_file_name");

                entity.Property(e => e.PhysicalFileStatusId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("physical_file_status_id");

                entity.Property(e => e.PhysicalFileTypeId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("physical_file_type_id");

                entity.Property(e => e.S3BucketId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("s3_bucket_id");

                entity.Property(e => e.S3FileKey)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("s3_file_key");

                entity.Property(e => e.S3LargeThumbnailFileKey)
                    .HasMaxLength(1024)
                    .HasColumnName("s3_large_thumbnail_file_key");

                entity.Property(e => e.S3MediumThumbnailFileKey)
                    .HasMaxLength(1024)
                    .HasColumnName("s3_medium_thumbnail_file_key");

                entity.Property(e => e.S3SmallThumbnailFileKey)
                    .HasMaxLength(1024)
                    .HasColumnName("s3_small_thumbnail_file_key");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.S3Bucket)
                    .WithMany(p => p.PhysicalFiles)
                    .HasForeignKey(d => d.S3BucketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("physical_files_ibfk_1");
            });

            modelBuilder.Entity<PhysicalFileStatus>(entity =>
            {
                entity.ToTable("physical_file_status");

                entity.Property(e => e.PhysicalFileStatusId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("physical_file_status_id");

                entity.Property(e => e.PhysicalFileStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("physical_file_status_name");
            });

            modelBuilder.Entity<PhysicalFileType>(entity =>
            {
                entity.ToTable("physical_file_type");

                entity.Property(e => e.PhysicalFileTypeId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("physical_file_type_id");

                entity.Property(e => e.PhysicalFileTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("physical_file_type_name");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.HasIndex(e => e.ColorId, "color_id");

                entity.HasIndex(e => e.ProductGroupId, "product_group_id");

                entity.HasIndex(e => e.ProductNameEn, "product_name_en_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.HasIndex(e => e.ProductNameRu, "product_name_ru_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.HasIndex(e => e.ProductTypeId, "product_type_id");

                entity.Property(e => e.ProductId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_id");

                entity.Property(e => e.ColorId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("color_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("description_en");

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("description_ru");

                entity.Property(e => e.FromLength)
                    .HasPrecision(18, 2)
                    .HasColumnName("from_length");

                entity.Property(e => e.HairStyleId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("hair_style_id");

                entity.Property(e => e.IsBestSelling).HasColumnName("is_best_selling");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.MaterialTypeId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("material_type_id");

                entity.Property(e => e.MeasureUnitLengthId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("measure_unit_length_id");

                entity.Property(e => e.MeasureUnitWeightId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("measure_unit_weight_id");

                entity.Property(e => e.Origin)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("origin");

                entity.Property(e => e.PackingRuleId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("packing_rule_id");

                entity.Property(e => e.ProductGroupId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_group_id");

                entity.Property(e => e.ProductNameEn)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_name_en");

                entity.Property(e => e.ProductNameRu)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_name_ru");

                entity.Property(e => e.ProductTypeId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_type_id");

                entity.Property(e => e.ToLength)
                    .HasPrecision(18, 2)
                    .HasColumnName("to_length");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.VideoUrl)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("video_url");

                entity.Property(e => e.Weight)
                    .HasPrecision(18, 2)
                    .HasColumnName("weight");

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ColorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("products_ibfk_3");

                entity.HasOne(d => d.ProductGroup)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ProductGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("products_ibfk_2");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("products_ibfk_1");
            });

            modelBuilder.Entity<ProductGroup>(entity =>
            {
                entity.ToTable("product_groups");

                entity.HasIndex(e => e.ProductGroupNameEn, "product_group_name_en_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.HasIndex(e => e.ProductTypeId, "product_groups_ibfk_1_idx");

                entity.Property(e => e.ProductGroupId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_group_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ProductGroupNameEn)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_group_name_en");

                entity.Property(e => e.ProductGroupNameRu)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_group_name_ru");

                entity.Property(e => e.ProductTypeId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_type_id");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.ProductGroups)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_groups_ibfk_1");
            });

            modelBuilder.Entity<ProductImage>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.ImageId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("product_images");

                entity.HasIndex(e => e.ImageId, "image_id");

                entity.Property(e => e.ProductId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_id");

                entity.Property(e => e.ImageId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("image_id");

                entity.Property(e => e.IsMainImage).HasColumnName("is_main_image");

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.ProductImages)
                    .HasForeignKey(d => d.ImageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_images_ibfk_2");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductImages)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_images_ibfk_1");
            });

            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.ToTable("product_types");

                entity.HasIndex(e => e.CategoryId, "category_id");

                entity.HasIndex(e => e.ProductTypeNameEn, "product_type_name_en_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.Property(e => e.ProductTypeId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("product_type_id");

                entity.Property(e => e.CategoryId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("category_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ProductTypeNameEn)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_type_name_en");

                entity.Property(e => e.ProductTypeNameRu)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("product_type_name_ru");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.ProductTypes)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_types_ibfk_1");
            });

            modelBuilder.Entity<S3Bucket>(entity =>
            {
                entity.ToTable("s3_buckets");

                entity.Property(e => e.S3BucketId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("s3_bucket_id");

                entity.Property(e => e.S3BucketName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("s3_bucket_name");
            });

            modelBuilder.Entity<Subscriber>(entity =>
            {
                entity.ToTable("subscribers");

                entity.Property(e => e.SubscriberId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("subscriber_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("email");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");
            });

            modelBuilder.Entity<Supporter>(entity =>
            {
                entity.ToTable("supporters");

                entity.HasIndex(e => e.AvatarId, "avatar_id");

                entity.HasIndex(e => e.SupporterName, "supporter_name_idx")
                    .HasAnnotation("MySql:FullTextIndex", true);

                entity.Property(e => e.SupporterId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("supporter_id");

                entity.Property(e => e.AvatarId)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("avatar_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("email");

                entity.Property(e => e.FacebookUrl)
                    .HasColumnType("text")
                    .HasColumnName("facebook_url");

                entity.Property(e => e.InstagramUrl)
                    .HasColumnType("text")
                    .HasColumnName("instagram_url");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("phone_number");

                entity.Property(e => e.SupporterName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("supporter_name");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.WhatsappPhoneNumber)
                    .HasMaxLength(128)
                    .HasColumnName("whatsapp_phone_number");

                entity.HasOne(d => d.Avatar)
                    .WithMany(p => p.Supporters)
                    .HasForeignKey(d => d.AvatarId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("supporters_ibfk_1");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasColumnName("is_enabled")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.PasswordHashed)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("password_hashed");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .HasColumnName("salt");

                entity.Property(e => e.StatusId)
                    .HasColumnType("int(11)")
                    .HasColumnName("status_id")
                    .HasDefaultValueSql("'500'");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_name");

                entity.Property(e => e.UserTypeId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("user_type_id");
            });

            modelBuilder.Entity<UserConfirmationCode>(entity =>
            {
                entity.HasKey(e => e.ConfirmationId)
                    .HasName("PRIMARY");

                entity.ToTable("user_confirmation_codes");

                entity.HasIndex(e => e.UserId, "user_id");

                entity.Property(e => e.ConfirmationId).HasColumnName("confirmation_id");

                entity.Property(e => e.ConfirmationCode)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("confirmation_code");

                entity.Property(e => e.ConfirmationTypeId)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("confirmation_type_id");

                entity.Property(e => e.CreatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.ExpiredTime)
                    .HasColumnType("datetime")
                    .HasColumnName("expired_time");

                entity.Property(e => e.UpdatedAtUtc)
                    .HasColumnType("datetime")
                    .HasColumnName("updated_at_utc")
                    .HasDefaultValueSql("current_timestamp()");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserConfirmationCodes)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_confirmation_codes_ibfk_1");
            });

            modelBuilder.Entity<UserConfirmationType>(entity =>
            {
                entity.HasKey(e => e.ConfirmationTypeId)
                    .HasName("PRIMARY");

                entity.ToTable("user_confirmation_types");

                entity.Property(e => e.ConfirmationTypeId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("confirmation_type_id");

                entity.Property(e => e.ConfirmationTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("confirmation_type_name");
            });

            modelBuilder.Entity<UserStatus>(entity =>
            {
                entity.ToTable("user_statuses");

                entity.Property(e => e.UserStatusId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("user_status_id");

                entity.Property(e => e.UserStatusName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_status_name");
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.ToTable("user_types");

                entity.Property(e => e.UserTypeId)
                    .HasColumnType("smallint(6)")
                    .ValueGeneratedNever()
                    .HasColumnName("user_type_id");

                entity.Property(e => e.UserTypeName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("user_type_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
