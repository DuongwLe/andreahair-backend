﻿using System;

namespace AndreaHair.EntityFramework.MainDb.Base
{
    public interface IDeleteEntity
    {
        bool IsDeleted { get; set; }
    }

    public interface IUpsertEntity
    {
        DateTime CreatedAtUtc { get; set; }
        DateTime UpdatedAtUtc { get; set; }
        Guid? CreatedByUserId { get; set; }
        Guid? UpdatedByUserId { get; set; }
    }

    public interface IUpsertAndDeleteEntity : IDeleteEntity, IUpsertEntity
    {
    }
}
