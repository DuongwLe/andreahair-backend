﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AndreaHair.EntityFramework.MainDb.Base
{
    public static class OptionBuilderExtensions
    {
        public static DbContextOptionsBuilder StandardOptions(this DbContextOptionsBuilder builder)
        {
            builder = builder
                .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll)
                //.UseLazyLoadingProxies(true)
                ;
            //if (DevelopmentMode.IsEnableInterceptor)
            //{
            //    builder = builder.AddInterceptors(new EnjectWhereInCommandInterceptor());
            //    //builder = builder.ReplaceService<IQueryTranslationPreprocessorFactory, CustomQueryTranslationPreprocessorFactory>();
            //}
            return builder;
        }

        public static DbContextOptionsBuilder<TContext> StandardOptions<TContext>(this DbContextOptionsBuilder<TContext> builder, string connectionString, ILoggerFactory loggerFactory) where TContext : DbContext
        {
            //if (DevelopmentMode.IsEnableRetryOnFail_InEFCore)
            //{
            //    builder = builder.UseSqlServer(connectionString, options =>
            //        {
            //            options.EnableRetryOnFailure(3);
            //        });
            //}
            //else
            {
                builder = builder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            }
            builder = builder
                .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll)
                .EnableSensitiveDataLogging(true)
                .UseLoggerFactory(loggerFactory)
                .ConfigureWarnings(warnings =>
                {
                    warnings.Ignore(Microsoft.EntityFrameworkCore.Diagnostics.CoreEventId.SensitiveDataLoggingEnabledWarning);
                })
                //.UseLazyLoadingProxies(true)
                ;
            //if (DevelopmentMode.IsEnableInterceptor)
            //{
            //    builder.AddInterceptors(new EnjectWhereInCommandInterceptor(loggerFactory));
            //    //result = builder.ReplaceService<IQueryTranslationPreprocessorFactory, CustomQueryTranslationPreprocessorFactory>();
            //}
            return builder;
        }
    }
}
