﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace AndreaHair.EntityFramework.MainDb.Base
{
    public interface IDbContextFilterTypeCache
    {
        public int FilterType { get; }
    }

    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {
            if (context is IDbContextFilterTypeCache dynamicContext)
            {
                return (context.GetType(), dynamicContext.FilterType);
            }
            return context.GetType();
        }
    }
}
