﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class ColorMapping
    {
        public long ColorId { get; set; }
        public string ColorCode { get; set; }

        public virtual Color Color { get; set; }
    }
}
