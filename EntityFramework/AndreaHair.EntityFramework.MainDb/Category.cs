﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class Category
    {
        public Category()
        {
            ProductTypes = new HashSet<ProductType>();
        }

        public long CategoryId { get; set; }
        public string CategoryNameEn { get; set; }
        public string CategoryNameRu { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual ICollection<ProductType> ProductTypes { get; set; }
    }
}
