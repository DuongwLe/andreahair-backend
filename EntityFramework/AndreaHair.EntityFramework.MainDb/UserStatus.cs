﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class UserStatus
    {
        public short UserStatusId { get; set; }
        public string UserStatusName { get; set; }
    }
}
