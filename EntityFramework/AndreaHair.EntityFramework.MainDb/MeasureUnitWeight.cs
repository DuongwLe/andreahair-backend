﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class MeasureUnitWeight
    {
        public short MeasureUnitWeightId { get; set; }
        public string MeasureUnitWeightName { get; set; }
    }
}
