﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class ProductType
    {
        public ProductType()
        {
            ProductGroups = new HashSet<ProductGroup>();
            Products = new HashSet<Product>();
        }

        public long ProductTypeId { get; set; }
        public string ProductTypeNameEn { get; set; }
        public string ProductTypeNameRu { get; set; }
        public long CategoryId { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<ProductGroup> ProductGroups { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
