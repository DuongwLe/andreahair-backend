﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class UserType
    {
        public short UserTypeId { get; set; }
        public string UserTypeName { get; set; }
    }
}
