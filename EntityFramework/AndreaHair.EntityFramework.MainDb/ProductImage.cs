﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class ProductImage
    {
        public long ProductId { get; set; }
        public long ImageId { get; set; }
        public bool IsMainImage { get; set; }

        public virtual PhysicalFile Image { get; set; }
        public virtual Product Product { get; set; }
    }
}
