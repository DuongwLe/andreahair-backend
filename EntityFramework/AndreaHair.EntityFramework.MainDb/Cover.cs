﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class Cover
    {
        public long CoverId { get; set; }
        public string CoverName { get; set; }
        public long PhysicalFileId { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual PhysicalFile PhysicalFile { get; set; }
    }
}
