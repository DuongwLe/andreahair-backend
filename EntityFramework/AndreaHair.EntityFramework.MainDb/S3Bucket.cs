﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class S3Bucket
    {
        public S3Bucket()
        {
            PhysicalFiles = new HashSet<PhysicalFile>();
        }

        public short S3BucketId { get; set; }
        public string S3BucketName { get; set; }

        public virtual ICollection<PhysicalFile> PhysicalFiles { get; set; }
    }
}
