﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class HairStyle
    {
        public short HairStyleId { get; set; }
        public string HairStyleName { get; set; }
    }
}
