﻿using System;
using System.Linq.Expressions;
using AndreaHair.EntityFramework.MainDb.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Logging;

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class MainDbContext
    {
        public MainDbContext(string connectionString, ILoggerFactory loggerFactory)
             : this(new DbContextOptionsBuilder<MainDbContext>().StandardOptions(connectionString, loggerFactory).Options)
        {
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            ApplyFilter(modelBuilder);

            // Convert all datetime value to Utc
            var dateTimeConverter = new ValueConverter<DateTime, DateTime>(
                v => v.ToUniversalTime(),
                v => DateTime.SpecifyKind(v, DateTimeKind.Utc));
            var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(
                v => v.HasValue ? v.Value.ToUniversalTime() : v,
                v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v);
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                if (entityType.IsKeyless)
                {
                    continue;
                }

                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(DateTime))
                    {
                        property.SetValueConverter(dateTimeConverter);
                    }
                    else if (property.ClrType == typeof(DateTime?))
                    {
                        property.SetValueConverter(nullableDateTimeConverter);
                    }
                }
            }
        }

        protected virtual void ApplyFilter(ModelBuilder modelBuilder)
        {
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                var filterBuilder = new FilterExpressionBuilder(entityType.ClrType);

                var isDeletedProp = entityType.FindProperty("IsDeleted");
                if (isDeletedProp != null)
                {
                    filterBuilder.AddFilter("IsDeleted", Expression.Constant(false));
                }

                entityType.SetQueryFilter(filterBuilder.Build());
            }
        }
    }
}
