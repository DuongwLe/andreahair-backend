﻿using AndreaHair.Commons.Enums;
using AndreaHair.EntityFramework.MainDb.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;

namespace AndreaHair.EntityFramework.MainDb.Custom
{
    public class MainDbContextRestriction : MainDbContext, IDbContextFilterTypeCache
    {
        public int FilterType { get; private set; }

        public MainDbContextRestriction(
           string connectionString,
           ILoggerFactory loggerFactory,
           params DbContextFilterTypeEnum[] filterTypes
           )
           : base(connectionString, loggerFactory)
        {
            FilterType = 0;
            if (filterTypes != null)
            {
                foreach (var filterType in filterTypes)
                {
                    FilterType |= (int)filterType;
                }
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();

        protected override void ApplyFilter(ModelBuilder modelBuilder)
        {
            FiltersExtensions.GlobalFilters(this, modelBuilder);
        }
    }
}
