﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class UserConfirmationCode
    {
        public Guid ConfirmationId { get; set; }
        public Guid UserId { get; set; }
        public string ConfirmationCode { get; set; }
        public short ConfirmationTypeId { get; set; }
        public DateTime ExpiredTime { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual User User { get; set; }
    }
}
