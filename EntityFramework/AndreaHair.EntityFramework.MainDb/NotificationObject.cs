﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class NotificationObject
    {
        public NotificationObject()
        {
            Notifications = new HashSet<Notification>();
        }

        public long NotificationObjectId { get; set; }
        public string TitleEn { get; set; }
        public string BodyEn { get; set; }
        public string TitleVi { get; set; }
        public string BodyVi { get; set; }
        public string DataJson { get; set; }
        public Guid? SenderId { get; set; }
        public DateTime SendAtUtc { get; set; }
        public bool IsSent { get; set; }
        public DateTime? ActualSentAtUtc { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }
    }
}
