﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class Color
    {
        public Color()
        {
            ColorMappings = new HashSet<ColorMapping>();
            Products = new HashSet<Product>();
        }

        public long ColorId { get; set; }
        public string ColorNameEn { get; set; }
        public string ColorNameRu { get; set; }
        public bool? IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public DateTime UpdatedAtUtc { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual ICollection<ColorMapping> ColorMappings { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
