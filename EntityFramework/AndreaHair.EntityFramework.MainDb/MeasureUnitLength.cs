﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AndreaHair.EntityFramework.MainDb
{
    public partial class MeasureUnitLength
    {
        public short MeasureUnitLengthId { get; set; }
        public string MeasureUnitLengthName { get; set; }
    }
}
