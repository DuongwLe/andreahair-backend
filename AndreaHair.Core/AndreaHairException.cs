﻿using System;

namespace AndreaHair.Core
{
    public class AndreaHairException : Exception
    {
        public AndreaHairException()
        {
        }

        public AndreaHairException(string message) : base(message)
        {
        }

        public AndreaHairException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

