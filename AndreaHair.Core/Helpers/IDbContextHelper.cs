﻿using System;
using AndreaHair.EntityFramework.MainDb;

namespace AndreaHair.Core.Helpers
{
    public interface IDbContextHelper
    {
        MainDbContext GetCurrentMainDbContext(bool isIsolatedInstance = false);
    }
}

