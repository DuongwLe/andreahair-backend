﻿using System;
using AndreaHair.Commons.Enums;

namespace AndreaHair.Core.Helpers
{
    public interface ICurrentContext
    {
        bool IsReady { get; }
        Guid? UserId { get; }
        UserTypeEnum? UserTypeId { get; }
    }
}

