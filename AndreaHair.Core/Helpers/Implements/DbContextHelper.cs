﻿using System;
using AndreaHair.Commons;
using AndreaHair.EntityFramework.MainDb;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AndreaHair.Core.Helpers.Implements
{
    public class DbContextHelper : IDbContextHelper
    {
        private readonly AppSettings _appSettings;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ICurrentContext _currentContext;

        private MainDbContext _currentMainDbContext;

        public DbContextHelper(
            IOptionsSnapshot<AppSettings> appSettings,
            ILoggerFactory loggerFactory,
            ICurrentContext currentContext
        )
        {
            _appSettings = appSettings.Value;
            _loggerFactory = loggerFactory;
            _currentContext = currentContext;
        }

        public MainDbContext GetCurrentMainDbContext(bool isIsolatedInstance = false)
        {
            if (!_currentContext.IsReady)
                throw new AndreaHairException("CurrentContext not ready!");

            if (isIsolatedInstance)
            {
                return CreateMainDbContext();
            }

            if (_currentMainDbContext == null)
            {
                _currentMainDbContext = CreateMainDbContext();
            }

            return _currentMainDbContext;
        }

        private MainDbContext CreateMainDbContext()
        {
            var connectionString = _appSettings.ConnectionStrings.MainDbConnection;

            return new MainDbContext(connectionString, _loggerFactory);
        }

        public void Dispose()
        {
            try
            {
                if (_currentMainDbContext != null)
                    _currentMainDbContext.Dispose();

                GC.SuppressFinalize(this);
            }
            catch
            {
                // ignore because of disposing
            }
        }
    }
}

