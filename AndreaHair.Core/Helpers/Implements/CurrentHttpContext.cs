﻿using System;
using System.Linq;
using AndreaHair.Commons.Constants;
using AndreaHair.Commons.Enums;
using Microsoft.AspNetCore.Http;

namespace AndreaHair.Core.Helpers.Implements
{
    public class CurrentHttpContext : ICurrentContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IIdentityHelper _identityHelper;

        public CurrentHttpContext(
            IHttpContextAccessor httpContextAccessor,
            IIdentityHelper identityHelper
        )
        {
            _httpContextAccessor = httpContextAccessor;
            _identityHelper = identityHelper;

            _isReady = httpContextAccessor.HttpContext != null;
        }

        private bool _isReady;
        private Guid? _userId;
        private UserTypeEnum? _userTypeId = null;

        public bool IsReady => _isReady;

        public Guid? UserId
        {
            get
            {
                if (!_isReady)
                    throw new AndreaHairException("CurrentContext not ready!");

                if (_userId.HasValue)
                    return _userId;

                var userId = GetClaimValue(OAuthConstants.ClaimTypes.UserId);
                if (string.IsNullOrEmpty(userId))
                {
                    return null;
                }
                else
                {
                    _userId = Guid.Parse(userId);
                    return _userId;
                }
            }
        }

        public UserTypeEnum? UserTypeId
        {
            get
            {
                if (!_isReady)
                    throw new AndreaHairException("CurrentContext not ready!");

                if (_userTypeId.HasValue)
                    return _userTypeId;

                var userType = GetClaimValue(OAuthConstants.ClaimTypes.UserType);
                if (string.IsNullOrEmpty(userType) || !int.TryParse(userType, out int userTypeId))
                {
                    return null;
                }

                _userTypeId = (UserTypeEnum)userTypeId;
                return _userTypeId;
            }
        }

        private string GetClaimValue(params string[] claimTypes)
        {
            if (!_isReady)
                throw new NotSupportedException("Not a HTTP call!");

            foreach (var claimType in claimTypes)
            {
                var claim = _httpContextAccessor.HttpContext.User.FindFirst(claimType);

                if (claim != null)
                    return claim.Value ?? string.Empty;
            }

            return string.Empty;
        }

        private string[] GetClaimValues(params string[] claimTypes)
        {
            if (!_isReady)
                throw new NotSupportedException("Not a HTTP call!");

            foreach (var claimType in claimTypes)
            {
                var claims = _httpContextAccessor.HttpContext.User.FindAll(claimType);

                if (claims != null && claims.Any())
                    return claims.Select(c => c.Value).ToArray();
            }

            return Array.Empty<string>();
        }

        private string GetRouteValue(string key)
        {
            if (!_isReady)
                throw new NotSupportedException("Not a HTTP call!");

            var hasValue = _httpContextAccessor.HttpContext.Request.RouteValues.TryGetValue(key, out object value);
            if (hasValue)
            {
                return value.ToString();
            }

            return string.Empty;
        }
    }
}

