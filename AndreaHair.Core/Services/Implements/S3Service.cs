﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using AndreaHair.Commons;
using Microsoft.Extensions.Options;

namespace AndreaHair.Core.Services.Implements
{
    public class S3Service : IS3Service
    {
        private readonly S3Settings _settings;
        private readonly AmazonS3Client _s3Client;

        public S3Service(IOptions<S3Settings> settings)
        {
            _settings = settings.Value;
            _s3Client = new AmazonS3Client(settings.Value.AccessKey, settings.Value.SecretKey, RegionEndpoint.GetBySystemName(settings.Value.EndPoint));
        }

        public string GetPresignedUploadUrl(string bucketName, string fileKey)
        {
            var presignedUrl = _s3Client.GetPreSignedURL(new GetPreSignedUrlRequest
            {
                Verb = HttpVerb.PUT,
                BucketName = bucketName,
                Key = fileKey,
                Expires = DateTime.UtcNow.AddMinutes(60 * 60)
            });

            return presignedUrl;
        }

        public async Task<GetObjectResponse> GetStat(string bucketName, string fileKey, CancellationToken cancellationToken)
        {
            try
            {
                return await _s3Client.GetObjectAsync(bucketName, fileKey, cancellationToken);
            }
            catch (AmazonS3Exception)
            {
                return null;
            }
        }

        public string GetTempPublicUrl(string bucketName, string fileKey)
        {
            return _s3Client.GetPreSignedURL(new GetPreSignedUrlRequest
            {
                BucketName = bucketName,
                Key = fileKey,
                Expires = DateTime.UtcNow.AddMinutes(60 * 3)
            });
        }

        public async Task<DeleteObjectsResponse> DeleteObjects(string bucketName, string[] fileKeys,CancellationToken cancellationToken)
        {
            List < KeyVersion > keyVersions = new();
            foreach (var key in fileKeys)
            {
                keyVersions.Add(new KeyVersion
                {
                    Key = key,
                    VersionId = null
                });
            }

            var multiObjectDeleteRequest = new DeleteObjectsRequest
            {
                BucketName = bucketName,
                Objects = keyVersions // This includes the object keys and null version IDs.
            };
            return await _s3Client.DeleteObjectsAsync(multiObjectDeleteRequest, cancellationToken);
        }
    }
}

