﻿namespace AndreaHair.Commons.Enums
{
    public enum HairStyleEnum
    {
        Straight = 1,
        KinkyStraight = 2,
        Wave = 3,
        BodyWave = 4,
        LooseWay = 5,
        DeepWave = 6,
        Curly = 7,
        CurlyWave = 8,
        KinkyCurly = 9
    }
}