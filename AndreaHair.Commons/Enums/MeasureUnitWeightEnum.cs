﻿namespace AndreaHair.Commons.Enums
{
    public enum MeasureUnitWeightEnum
    {
        Kilogram = 1,
        Pound = 2
    }
}