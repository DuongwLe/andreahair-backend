﻿namespace AndreaHair.Commons.Enums
{
    public enum PhysicalFileStatusEnum
    {
        New = 0,
        OK = 1,
        WillBeDeleted = 2,
        Deleted = 3,
        Corrupted = 4,
    }
}

