﻿namespace AndreaHair.Commons.Enums
{
    public enum MeasureUnitLengthEnum
    {
        Inch = 1,
        Centimeter = 2
    }
}