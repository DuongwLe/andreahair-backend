﻿namespace AndreaHair.Commons.Enums
{
    public enum NotificationEventTypeEnum
    {
        NewContact = 1,
        NewSubscriber = 2
    }
}