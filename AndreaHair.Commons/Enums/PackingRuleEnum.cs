﻿namespace AndreaHair.Commons.Enums
{
    public enum PackingRuleEnum
    {
        ClearPlasticBagWithSmallTag = 1,
        ClientsPackingAndLogoAccepted = 2
    }
}