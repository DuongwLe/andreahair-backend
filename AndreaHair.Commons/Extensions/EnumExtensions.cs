﻿using System;
using System.ComponentModel;
using AndreaHair.Commons.Enums;

namespace AndreaHair.Commons.Extensions
{
    public static class EnumExtentions
    {
        public static string ToCultureString(this LanguageEnum value)
        {
            return value switch
            {
                LanguageEnum.EN => "en-US",
                LanguageEnum.RU => "ru-RU",
                LanguageEnum.VN => "vi-VN",
                _ => "en-US",
            };
        }

        public static string GetDescription<T>(this T src) where T : Enum
        {
            if (!typeof(T).IsEnum)
                return null;

            var description = src.ToString();
            var fieldInfo = src.GetType().GetField(src.ToString());

            if (fieldInfo != null)
            {
                var attrs = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    description = ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return description;
        }
    }
}

