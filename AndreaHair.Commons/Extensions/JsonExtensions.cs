﻿using System.Text.Json;

namespace AndreaHair.Commons.Extensions
{
    public static class JsonExtensions
    {
        private static JsonSerializerOptions _defaultOptions;

        private static JsonSerializerOptions DefaultOptions
        {
            get
            {
                if (_defaultOptions == null)
                {
                    _defaultOptions = CreateDefaultOptions();
                }
                return _defaultOptions;
            }
        }

        private static JsonSerializerOptions CreateDefaultOptions()
        {
            var setting = new JsonSerializerOptions
            {
                IgnoreNullValues = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            return setting;
        }

        public static string ToJson<T>(this T obj) where T : class
        {
            return JsonSerializer.Serialize(obj, DefaultOptions);
        }

        public static T ToObject<T>(this string json)
        {
            return JsonSerializer.Deserialize<T>(json, DefaultOptions);
        }
    }
}