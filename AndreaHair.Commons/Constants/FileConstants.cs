﻿using System;

namespace AndreaHair.Commons.Constants
{
    public static class FileConstants
    {
        public static readonly string[] AllowFileExtensions = new string[] { "jpeg", "jpg", "heic", "png", "mp3", "wav", "mpeg" };

        public static string GetFileKey(string type, string ext) => $"{type.ToLower()}/{ext.ToLower()}/{Guid.NewGuid()}.{ext.ToLower()}";
    }
}

