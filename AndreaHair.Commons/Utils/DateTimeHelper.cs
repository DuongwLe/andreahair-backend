﻿using System;
using TimeZoneConverter;

namespace AndreaHair.Commons.Utils
{
    public static class DateTimeHelper
    {
        private static readonly DateTime MIN_JAVASCRIPT_TIME = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ToUnixTime(DateTime utcTime)
        {
            return (long)(utcTime - MIN_JAVASCRIPT_TIME).TotalMilliseconds;
        }

        public static DateTime GetDateFromUnixTime(long time)
        {
            return MIN_JAVASCRIPT_TIME.AddMilliseconds(time);
        }

        public static DateTime GetDateFromUnixTime_Floor(long time)
        {
            return MIN_JAVASCRIPT_TIME.AddMilliseconds(time - 999);
        }

        public static DateTime GetDateFromUnixTime_Ceil(long time)
        {
            return MIN_JAVASCRIPT_TIME.AddMilliseconds(time + 999);
        }

        public static TimeZoneInfo GetTimeZoneInfo(string timeZoneId)
        {
            return TZConvert.GetTimeZoneInfo(timeZoneId);
        }

        public static DateTime ToDateTimeFromUtc(this DateTime utcTime, string timeZoneId = "Asia/Ho_Chi_Minh")
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcTime, TZConvert.GetTimeZoneInfo(timeZoneId));
        }

        public static int GetTimeZoneHourDiffFromUtc(string timeZoneId = "Asia/Ho_Chi_Minh")
        {
            return timeZoneId switch
            {
                "Asia/Ho_Chi_Minh" => 7,
                _ => 0
            };
        }
    }
}