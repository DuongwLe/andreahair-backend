﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace AndreaHair.Commons.Utils
{
    public static class ValidationHelper
    {
        public static bool IsEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool IsPhoneNumber(string phoneNumber, bool strict = false)
        {
            try
            {
                if (strict)
                {
                    // +84987654321
                    return Regex.IsMatch(phoneNumber,
                        @"^[\+][0-9]{11}$",
                        RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                }
                else
                {
                    return Regex.IsMatch(phoneNumber,
                        @"^(?=(?:\D*\d){10,15}\D*$)\+?[0-9]{1,3}[\s-]?(?:\(0?[0-9]{1,5}\)|[0-9]{1,5})[-\s]?[0-9][\d\s-]{5,7}\s?(?:x[\d-]{0,4})?$",
                        RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}