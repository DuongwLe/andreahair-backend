﻿using Sodium;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace AndreaHair.Commons.Utils
{
    public static class StringHelper
    {
        private static readonly Random random = new Random();
        private static readonly byte[] nonce = Encoding.UTF8.GetBytes("vw!$4C^Mh8&jRGG+5N8sFH8*"); //24 byte nonce
        private static readonly byte[] key = Encoding.UTF8.GetBytes("pdT@_XqUMKeA7M3b!XuMG3zC+*npGL?q"); //32 byte key

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomString(int length, string candidates)
        {
            return new string(Enumerable.Repeat(candidates, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string Encrypt(string message)
        {
            var data = Encoding.UTF8.GetBytes(message);
            var cipherText = SecretBox.Create(data, nonce, key);
            return Convert.ToBase64String(cipherText);
        }

        public static string Decrypt(string cipherTextBase64)
        {
            var cipherText = Convert.FromBase64String(cipherTextBase64);
            var data = SecretBox.Open(cipherText, nonce, key);
            return Encoding.UTF8.GetString(data);
        }

        public static string HashMD5(string message)
        {
            byte[] asciiBytes = Encoding.ASCII.GetBytes(message);
            byte[] hashedBytes = MD5.Create().ComputeHash(asciiBytes);
            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            return hashedString;
        }

        public static string BuildSearchString(string input)
        {
            // Replace non-word character with "+"
            var rawString = $"+{Regex.Replace(input, @"[\W]", "+")}";

            var stringElements = rawString.Split("+");
            var resultBuilder = new StringBuilder();

            foreach (var element in stringElements)
            {
                if (element.Length >= 2)
                {
                    resultBuilder.Append("+" + element);
                }
            }
            return resultBuilder.ToString().Trim();
        }

        public static string HmacSHA512(string key, string inputData)
        {
            var hash = new StringBuilder();
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
            using (var hmac = new HMACSHA512(keyBytes))
            {
                byte[] hashValue = hmac.ComputeHash(inputBytes);
                foreach (var theByte in hashValue)
                {
                    hash.Append(theByte.ToString("x2"));
                }
            }
            return hash.ToString();
        }

        public static string SwitchLanguage(string key, string inputEn, string inputRu)
        {
            string input = null;

            if (key.Contains("en-US"))
            {
                input = inputEn;
            }
            if (key.Contains("ru-RU"))
            {
                input = inputRu;
            }

            return input;
        }
    }
}