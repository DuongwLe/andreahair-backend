﻿namespace AndreaHair.Commons
{
    public class AppSettings
    {
        public ApplicationSettings ApplicationSettings { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }
        public IdentityServer IdentityServer { get; set; }
        public SigningCredential SigningCredential { get; set; }
        public S3Settings S3Settings { get; set; }
        public HangfireSettings HangfireSettings { get; set; }
    }

    public class ApplicationSettings
    {
        public string ConfigurationFileLocation { get; set; }
        public string ConfigurationFilePath { get; set; }
    }

    public class ConnectionStrings
    {
        public string MainDbConnection { get; set; }
        public string IdentityDbConnection { get; set; }
        public string HangfireDbConnection { get; set; }
    }

    public class IdentityServer
    {
        public string DefaultScheme { get; set; }
        public string Authority { get; set; }
        public string TokenEndpoint { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ApiName { get; set; }
        public string ApiSecret { get; set; }
    }

    public class SigningCredential
    {
        public string CertificateFileLocation { get; set; }
        public string CertificateFilePath { get; set; }
        public string CertificateFilePassword { get; set; }
    }

    public class S3Settings
    {
        public string EndPoint { get; set; }
        public short BucketId { get; set; }
        public string BucketName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }

    public class HangfireSettings
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}

