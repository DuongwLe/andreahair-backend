﻿using System;
using System.IO;
using System.Reflection;
using AndreaHair.Commons.Constants;
using Microsoft.Extensions.Configuration;

namespace AndreaHair.Commons
{
    public class AppSettingBuilder
    {
        public static IConfiguration Build()
        {
            DotNetEnv.Env.TraversePath().Load();
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development";
            Console.WriteLine("ASPNETCORE_ENVIRONMENT={0}", environmentName);

            var exeFolder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            exeFolder = exeFolder
                .Replace(@"file:\", "")
                .Replace(@"file:", "");

            var builder = new ConfigurationBuilder()
                    .SetBasePath(exeFolder)
                    .AddJsonFile($"appsettings.json", false, true)
                    .AddJsonFile($"appsettings.{environmentName}.json", true, true)
                    .AddJsonFile($"appconfig.json", false, true)
                    .AddJsonFile($"appconfig.{environmentName}.json", false, true);

            AddEnvironmentConfig(builder);

            return builder.Build();
        }

        public static void AddEnvironmentConfig(IConfigurationBuilder builder)
        {
            var appConfigTemp = builder.Build().Get<AppSettings>();

            switch (appConfigTemp.ApplicationSettings.ConfigurationFileLocation)
            {
                case AppConstants.ConfigurationLocation.LocalMachine:
                    var environmentFilePath = appConfigTemp.ApplicationSettings.ConfigurationFilePath;
                    if (!string.IsNullOrEmpty(environmentFilePath))
                    {
                        builder.AddJsonFile(environmentFilePath, false, true);
                    }
                    break;
                //case AppConstants.ConfigurationLocation.S3:
                //    var envAppConfigJson = ReadS3JsonAsync(appConfigTemp.ApplicationSettings.ConfigurationFilePath).Result;
                //    if (!string.IsNullOrEmpty(envAppConfigJson))
                //    {
                //        builder.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(envAppConfigJson)));
                //    }
                //    break;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}

