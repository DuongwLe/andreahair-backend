﻿using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiService
{
    public interface INotificationService
    {
        Task TriggerCreateNonUserMessage(long contactId, CancellationToken cancellationToken);
        Task TriggerSubscribe(long subscriberId, CancellationToken cancellationToken);
    }
}