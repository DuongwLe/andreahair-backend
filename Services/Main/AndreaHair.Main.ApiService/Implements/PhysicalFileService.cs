﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace AndreaHair.Main.ApiService.Implements
{
    public class PhysicalFileService : IPhysicalFileService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public PhysicalFileService(
            IServiceScopeFactory scopeFactory,
            IS3Service s3Service,
            IOptions<S3Settings> s3Settings)
        {
            _scopeFactory = scopeFactory;
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task DeletePhysicalFiles()
        {
            using var scope = _scopeFactory.CreateScope();
            {
                CancellationToken cancellationToken = new();
                var dbContext = scope.ServiceProvider.GetRequiredService<MainDbContext>();

                var files = await dbContext.PhysicalFiles
                    .Where(x => x.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.WillBeDeleted ||
                        x.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.Corrupted)
                    .ToArrayAsync();

                using var transaction = await dbContext.Database.BeginTransactionAsync();

                // Remove from s3 bucket
                await _s3Service.DeleteObjects(_s3Settings.BucketName, files.Select(x => x.S3FileKey).ToArray(), cancellationToken);

                // Set is_deleted
                foreach (var file in files)
                {
                    file.PhysicalFileStatusId = (short)PhysicalFileStatusEnum.Deleted;
                    file.IsDeleted = true;
                    file.UpdatedAtUtc = DateTime.UtcNow;
                    dbContext.PhysicalFiles.Update(file);
                }
                await dbContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
        }
    }
}

