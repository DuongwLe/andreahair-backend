﻿using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Localization;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Hubs;
using AndreaHair.EntityFramework.MainDb;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiService.Implements
{
    public class NotificationService : INotificationService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IHubContext<NotificationHub> _hubContext;

        public NotificationService(IServiceScopeFactory scopeFactory, IHubContext<NotificationHub> hubContext)
        {
            _scopeFactory = scopeFactory;
            _hubContext = hubContext;
        }

        public async Task TriggerCreateNonUserMessage(long contactId, CancellationToken cancellationToken)
        {
            // Send notification to admin
            using var scope = _scopeFactory.CreateScope();
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<MainDbContext>();

                var contact = await dbContext.Contacts
                    .Where(x => x.ContactId == contactId)
                    .FirstOrDefaultAsync(cancellationToken);

                var notifierIds = await dbContext.Users
                   .Where(u => u.UserTypeId == (short)UserTypeEnum.Admin)
                   .Select(u => u.UserId)
                   .ToArrayAsync(cancellationToken);

                if (notifierIds.Length > 0)
                {
                    var data = new
                    {
                        NotificationEventType = NotificationEventTypeEnum.NewContact,
                        contact.Email,
                        CreateAtUnix = DateTimeHelper.ToUnixTime(contact.CreatedAtUtc)
                    };

                    Resource.Culture = CultureInfo.GetCultureInfo("en-US");
                    string titleEn = Resource.CreateContactTitle;
                    string bodyEn = string.Format(Resource.CreateContactBody, contact.Email);
                    Resource.Culture = CultureInfo.GetCultureInfo("vi-VN");
                    string titleVi = Resource.CreateContactTitle;
                    string bodyVi = string.Format(Resource.CreateContactBody, contact.Email);

                    await Trigger
                    (
                        titleEn,
                        titleVi,
                        bodyEn,
                        bodyVi,
                        data.ToJson(),
                        Guid.Empty,
                        notifierIds,
                        DateTime.UtcNow,
                        cancellationToken
                    );

                    await _hubContext.Clients
                        .Groups(Array.ConvertAll(notifierIds, id => id.ToString()))
                        .SendAsync("ReceiveMessage", data.ToJson());
                }
            }
        }

        public async Task TriggerSubscribe(long subscriberId, CancellationToken cancellationToken)
        {
            // Send notification to admin
            using var scope = _scopeFactory.CreateScope();
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<MainDbContext>();

                var subscriber = await dbContext.Subscribers
                    .Where(x => x.SubscriberId == subscriberId)
                    .FirstOrDefaultAsync(cancellationToken);

                var notifierIds = await dbContext.Users
                   .Where(u => u.UserTypeId == (short)UserTypeEnum.Admin)
                   .Select(u => u.UserId)
                   .ToArrayAsync(cancellationToken);

                if (notifierIds.Length > 0)
                {
                    var data = new
                    {
                        NotificationEventType = NotificationEventTypeEnum.NewSubscriber,
                        subscriber.Email,
                        CreateAtUnix = DateTimeHelper.ToUnixTime(subscriber.CreatedAtUtc)
                    };

                    Resource.Culture = CultureInfo.GetCultureInfo("en-US");
                    string titleEn = Resource.SubscribeTitle;
                    string bodyEn = string.Format(Resource.SubscribeBody, subscriber.Email);
                    Resource.Culture = CultureInfo.GetCultureInfo("vi-VN");
                    string titleVi = Resource.SubscribeTitle;
                    string bodyVi = string.Format(Resource.SubscribeBody, subscriber.Email);

                    await Trigger
                    (
                        titleEn,
                        titleVi,
                        bodyEn,
                        bodyVi,
                        data.ToJson(),
                        Guid.Empty,
                        notifierIds,
                        DateTime.UtcNow,
                        cancellationToken
                    );

                    await _hubContext.Clients
                        .Groups(Array.ConvertAll(notifierIds, id => id.ToString()))
                        .SendAsync("ReceiveMessage", data.ToJson());
                }
            }
        }

        private async Task<bool> Trigger(string titleEN, string titleVN, string bodyEN, string bodyVN, string data, Guid senderId, Guid[] notifierIds, DateTime sentAtUtc, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<MainDbContext>();
                    var notificaitonObject = new NotificationObject
                    {
                        TitleEn = titleEN,
                        TitleVi = titleVN,
                        BodyVi = bodyVN,
                        BodyEn = bodyEN,
                        DataJson = data,
                        IsSent = true,
                        SenderId = senderId,
                        SendAtUtc = sentAtUtc,
                        ActualSentAtUtc = sentAtUtc,
                        CreatedByUserId = senderId,
                        CreatedAtUtc = DateTime.UtcNow
                    };
                    dbContext.NotificationObjects.Add(notificaitonObject);
                    await dbContext.SaveChangesAsync(cancellationToken);

                    var notifications = notifierIds.Select(userId => new Notification
                    {
                        NotificationObjectId = notificaitonObject.NotificationObjectId,
                        NotifierId = userId,
                        IsRead = false,
                        IsDeleted = false,
                        CreatedByUserId = senderId,
                        CreatedAtUtc = DateTime.UtcNow
                    });
                    await dbContext.BulkInsertAsync(notifications, cancellationToken);
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"[NotificationService] Trigger error", e);
                return false;
            }
        }
    }
}