﻿using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiService
{
    public interface IPhysicalFileService
    {
        Task DeletePhysicalFiles();
    }
}

