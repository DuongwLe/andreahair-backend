namespace AndreaHair.Main.ApiModel.ApiErrorMessages
{
    /// <summary>
    /// Prefix must be: PINT_
    /// </summary>
    public static class ApiInternalErrorMessages
    {
        public static ApiErrorMessage LoginFailed => new ApiErrorMessage
        {
            Code = "PINT_1001",
            Value = "Login failed"
        };

        public static ApiErrorMessage UserNotVerified => new ApiErrorMessage
        {
            Code = "PINT_1002",
            Value = "The user is not verified"
        };

        public static ApiErrorMessage EmailAlreadyUsed => new ApiErrorMessage
        {
            Code = "PINT_1003",
            Value = "The email is already used"
        };

        public static ApiErrorMessage TokenError => new ApiErrorMessage
        {
            Code = "PINT_1004",
            Value = "Token error"
        };

        public static ApiErrorMessage DuplicatedCategoryNameRu => new ApiErrorMessage
        {
            Code = "PINT_1005",
            Value = "Duplicated category name ru"
        };

        public static ApiErrorMessage DuplicatedCategoryNameEn => new ApiErrorMessage
        {
            Code = "PINT_1006",
            Value = "Duplicated category name en"
        };

        public static ApiErrorMessage DuplicatedProductTypeNameRu => new ApiErrorMessage
        {
            Code = "PINT_1007",
            Value = "Duplicated product type name ru"
        };

        public static ApiErrorMessage DuplicatedProductTypeNameEn => new ApiErrorMessage
        {
            Code = "PINT_1008",
            Value = "Duplicated product type name en"
        };

        public static ApiErrorMessage ProductTypeIsInUse => new ApiErrorMessage
        {
            Code = "PINT_1009",
            Value = "Product type is in use"
        };

        public static ApiErrorMessage CategoryIsInUse => new ApiErrorMessage
        {
            Code = "PINT_1010",
            Value = "Category is in use"
        };

        public static ApiErrorMessage DuplicatedColorNameRu => new ApiErrorMessage
        {
            Code = "PINT_1011",
            Value = "Duplicated color name ru"
        };

        public static ApiErrorMessage DuplicatedColorNameEn => new ApiErrorMessage
        {
            Code = "PINT_1012",
            Value = "Duplicated color name en"
        };

        public static ApiErrorMessage ColorIsInUse => new ApiErrorMessage
        {
            Code = "PINT_1013",
            Value = "Color is in use"
        };

        public static ApiErrorMessage FileCorrupted => new ApiErrorMessage
        {
            Code = "PINT_1014",
            Value = "File Corrupted"
        };

        public static ApiErrorMessage DuplicatedProductNameRu => new ApiErrorMessage
        {
            Code = "PINT_1015",
            Value = "Duplicated product name ru"
        };

        public static ApiErrorMessage DuplicatedProductNameEn => new ApiErrorMessage
        {
            Code = "PINT_1016",
            Value = "Duplicated product name en"
        };

        public static ApiErrorMessage DuplicatedSupporterName => new ApiErrorMessage
        {
            Code = "PINT_1017",
            Value = "Duplicated supporter name"
        };

        public static ApiErrorMessage DuplicatedSupporterPhoneNumber => new ApiErrorMessage
        {
            Code = "PINT_1018",
            Value = "Duplicated supporter phone number"
        };

        public static ApiErrorMessage DuplicatedSupporterEmail => new ApiErrorMessage
        {
            Code = "PINT_1019",
            Value = "Duplicated supporter email"
        };

        public static ApiErrorMessage CategoryHasBeenDisabled => new ApiErrorMessage
        {
            Code = "PINT_1020",
            Value = "Category has been disabled"
        };

        public static ApiErrorMessage ProductTypeHasBeenDisabled => new ApiErrorMessage
        {
            Code = "PINT_1021",
            Value = "Product type has been disabled"
        };

        public static ApiErrorMessage DuplicateCoverName => new ApiErrorMessage
        {
            Code = "PINT_1022",
            Value = "Duplicate cover name"
        };
        #region NOT FOUND ERROR
        public static ApiErrorMessage CategoryNotFound => new ApiErrorMessage
        {
            Code = "PINT_4001",
            Value = "Category not found"
        };

        public static ApiErrorMessage ProductTypeNotFound => new ApiErrorMessage
        {
            Code = "PINT_4002",
            Value = "Product type not found"
        };

        public static ApiErrorMessage PhysicalFileNotFound => new ApiErrorMessage
        {
            Code = "PINT_4003",
            Value = "Physical file not found"
        };

        public static ApiErrorMessage ColorNotFound => new ApiErrorMessage
        {
            Code = "PINT_4004",
            Value = "Color not found"
        };

        public static ApiErrorMessage UserNotFound => new ApiErrorMessage
        {
            Code = "PINT_4005",
            Value = "User not found"
        };

        public static ApiErrorMessage ProductNotFound => new ApiErrorMessage
        {
            Code = "PINT_4006",
            Value = "Product not found"
        };
        public static ApiErrorMessage SupporterNotFound => new ApiErrorMessage
        {
            Code = "PINT_4007",
            Value = "Supporter not found"
        };

        public static ApiErrorMessage ContactNotFound => new ApiErrorMessage
        {
            Code = "PINT_4008",
            Value = "Contact not found"
        };

        public static ApiErrorMessage NotificationNotFound => new ApiErrorMessage
        {
            Code = "PINT_4009",
            Value = "Notification not found"
        };

        public static ApiErrorMessage ImageNotFound => new ApiErrorMessage
        {
            Code = "PINT_4010",
            Value = "Image not found"
        };

        public static ApiErrorMessage CoverNotFound => new ApiErrorMessage
        {
            Code = "PINT_4011",
            Value = "Cover not found"
        };
        #endregion

        #region INVALID ERROR
        public static ApiErrorMessage InvalidUsernameOrPassword => new ApiErrorMessage
        {
            Code = "PINT_5001",
            Value = "Invalid username or password"
        };

        public static ApiErrorMessage InvalidFileExtension => new ApiErrorMessage
        {
            Code = "PINT_5002",
            Value = "Invalid file extension"
        };

        public static ApiErrorMessage InvalidEmail => new ApiErrorMessage
        {
            Code = "PINT_5003",
            Value = "Invalid email"
        };
        #endregion
    }
}