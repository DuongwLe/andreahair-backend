﻿using System;
using System.ComponentModel.DataAnnotations;
using AndreaHair.Commons.Enums;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductGetAllEnabledInputModel : IApiInput
    {
        [Range(1, 50)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }

        [StringLength(128)]
        public string Keyword { get; set; }

        [StringLength(128)]
        public string OrderBy { get; set; }

        public OrderByTypeEnum OrderByTypeId { get; set; }

        public ProductGetAllEnabledInput Details { get; set; }
    }

    public class ProductGetAllEnabledInput
    {
        public long? CategoryId { get; set; }

        public long? ProductTypeId { get; set; }

        public long? ColorId { get; set; }

        public bool? IsBestSelling { get; set; }
    }
}

