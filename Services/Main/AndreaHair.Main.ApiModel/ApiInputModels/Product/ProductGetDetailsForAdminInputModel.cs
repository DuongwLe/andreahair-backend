﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductGetDetailsForAdminInputModel : IApiInput
    {
        [Required]
        public long ProductGroupId { get; set; }
    }
}