﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductGetDetailsInputModel : IApiInput
    {
        [Required]
        public long ProductId { get; set; }
    }
}

