﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductSearchInputModel : IApiInput
    {
        [Range(1, 50)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }

        [StringLength(128)]
        public string Keyword { get; set; }
    }
}
