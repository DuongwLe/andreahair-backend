﻿using System.ComponentModel.DataAnnotations;
using AndreaHair.Commons.Enums;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductCreateInputModel : IApiInput
    {
        [Required]
        [StringLength(128)]
        public string ProductGroupNameEn { get; set; }

        [Required]
        [StringLength(128)]
        public string ProductGroupNameRu { get; set; }

        [Required]
        public long ProductTypeId { get; set; }

        [Required]
        public MaterialTypeEnum MaterialTypeId { get; set; }

        [Required]
        public HairStyleEnum HairStyleId { get; set; }

        [Required]
        public MeasureUnitLengthEnum MeasureUnitLengthId { get; set; }

        [Required]
        public decimal FromLength { get; set; }

        [Required]
        public decimal ToLength { get; set; }

        [Required]
        public MeasureUnitWeightEnum MeasureUnitWeightId { get; set; }

        [Required]
        public decimal Weight { get; set; }

        [Required]
        public string Origin { get; set; }

        [Required]
        public PackingRuleEnum PackingRuleId { get; set; }

        [Required]
        public string VideoUrl { get; set; }

        [Required]
        public string DescriptionEn { get; set; }

        [Required]
        public string DescriptionRu { get; set; }

        [Required]
        [MinLength(1)]
        public ColorInputModel[] Colors { get; set; }
    }

    public class ColorInputModel
    {
        [Required]
        public long ColorId { get; set; }

        [Required]
        [MinLength(1)]
        public ImageInputModel[] Images { get; set; }

        [Required]
        public bool IsBestSelling { get; set; }

        [Required]
        public bool IsEnabled { get; set; }
    }

    public class ImageInputModel
    {
        [Required]
        public long ImageId { get; set; }

        public bool IsMainImage { get; set; }
    }
}