﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductSetEnabledInputModel : IApiInput
    {
        [Required]
        public long ProductGroupId { get; set; }

        [Required]
        public ProductSetEnabledInput Details { get; set; }
    }

    public class ProductSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}