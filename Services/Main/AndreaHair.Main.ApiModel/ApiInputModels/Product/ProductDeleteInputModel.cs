﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductDeleteInputModel : IApiInput
    {
        [Required]
        public long ProductGroupId { get; set; }
    }
}