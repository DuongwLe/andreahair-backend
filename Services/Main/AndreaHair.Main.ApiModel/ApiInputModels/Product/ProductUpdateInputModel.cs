﻿using AndreaHair.Commons.Enums;
using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Product
{
    public class ProductUpdateInputModel : IApiInput
    {
        [Required]
        public long ProductGroupId { get; set; }

        [Required]
        public ProductUpdateInput Details { get; set; }
    }

    public class ProductUpdateInput
    {
        [Required]
        [StringLength(128)]
        public string ProductGroupNameEn { get; set; }

        [Required]
        [StringLength(128)]
        public string ProductGroupNameRu { get; set; }

        [Required]
        public long ProductTypeId { get; set; }

        [Required]
        public MaterialTypeEnum MaterialTypeId { get; set; }

        [Required]
        public HairStyleEnum HairStyleId { get; set; }

        [Required]
        public MeasureUnitLengthEnum MeasureUnitLengthId { get; set; }

        [Required]
        public decimal FromLength { get; set; }

        [Required]
        public decimal ToLength { get; set; }

        [Required]
        public MeasureUnitWeightEnum MeasureUnitWeightId { get; set; }

        [Required]
        public decimal Weight { get; set; }

        [Required]
        public string Origin { get; set; }

        [Required]
        public PackingRuleEnum PackingRuleId { get; set; }

        [Required]
        public string VideoUrl { get; set; }

        [Required]
        public string DescriptionEn { get; set; }

        [Required]
        public string DescriptionRu { get; set; }

        [Required]
        [MinLength(1)]
        public ColorUpdateInputModel[] Colors { get; set; }
    }

    public class ColorUpdateInputModel : ColorInputModel
    {
        public long? ProductId { get; set; }
    }
}