﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Auth
{
    public class AuthLoginInputModel : IApiInput
    {
        [Required]
        [EmailAddress]
        [StringLength(128)]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}