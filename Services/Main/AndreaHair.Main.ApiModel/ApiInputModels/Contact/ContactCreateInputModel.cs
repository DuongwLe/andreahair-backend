﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Contact
{
    public class ContactCreateInputModel : IApiInput
    {
        [Required]
        [StringLength(128)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(128)]
        public string LastName { get; set; }

        [Required]
        [StringLength(128)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(128)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(65535)]
        public string Message { get; set; }

        public long? ProductId { get; set; }
    }
}

