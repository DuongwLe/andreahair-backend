﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Contact
{
    public class ContactGetDetailsInputModel : IApiInput
    {
        [Required]
        public long ContactId { get; set; }
    }
}