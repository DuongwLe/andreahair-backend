﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Subcriber
{
    public class SubscriberGetAllInputModel : IApiInput
    {
        [Range(1, 50)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }
    }
}