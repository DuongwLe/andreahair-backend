﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Subcriber
{
    public class SubscriberCreateInputModel : IApiInput
    {
        [Required]
        [EmailAddress]
        [StringLength(128)]
        public string Email { get; set; }
    }
}

