using System;
using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Notification
{
    public class NotificationGetAllInputModel : IApiInput
    {
        [Range(1, 100)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }
    }
}