using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Notification
{
    public class NotificationMarkAsReadInputModel : IApiInput
    {
        [Required]
        public long NotificationId { get; set; }
    }
}