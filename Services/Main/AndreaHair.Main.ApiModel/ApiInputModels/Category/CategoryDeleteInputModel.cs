﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Category
{
    public class CategoryDeleteInputModel : IApiInput
    {
        [Required]
        public long CategoryId { get; set; }
    }
}