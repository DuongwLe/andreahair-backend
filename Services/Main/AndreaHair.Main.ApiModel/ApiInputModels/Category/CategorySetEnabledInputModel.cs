﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Category
{
    public class CategorySetEnabledInputModel : IApiInput
    {
        [Required]
        public long CategoryId { get; set; }

        [Required]
        public CategorySetEnabledInput Details { get; set; }
    }

    public class CategorySetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}