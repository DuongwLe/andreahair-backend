﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Category
{
    public class CategoryCreateInputModel : IApiInput
    {
        [Required]
        public string CategoryNameEn { get; set; }
        
        [Required]
        public string CategoryNameRu { get; set; }
    }
}