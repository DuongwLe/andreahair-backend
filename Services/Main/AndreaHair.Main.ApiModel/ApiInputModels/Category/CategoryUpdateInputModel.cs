﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Category
{
    public class CategoryUpdateInputModel : IApiInput
    {
        [Required]
        public long CategoryId { get; set; }

        [Required]
        public CategoryUpdateInput Details { get; set; }
    }

    public class CategoryUpdateInput
    {
        [Required]
        public string CategoryNameEn { get; set; }

        [Required]
        public string CategoryNameRu { get; set; }
    }
}