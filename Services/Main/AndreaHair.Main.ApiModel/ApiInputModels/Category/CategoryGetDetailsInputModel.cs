﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Category
{
    public class CategoryGetDetailsInputModel : IApiInput
    {
        [Required]
        public long CategoryId { get; set; }
    }
}