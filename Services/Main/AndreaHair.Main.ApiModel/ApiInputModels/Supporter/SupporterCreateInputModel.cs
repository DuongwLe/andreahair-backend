﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Supporter
{
    public class SupporterCreateInputModel : IApiInput
    {
        [Required]
        public string SupporterName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }

        public string FacebookUrl { get; set; }

        public string InstagramUrl { get; set; }

        public string WhatsappPhoneNumber { get; set; }

        [Required]
        public long AvatarId { get; set; }
    }
}