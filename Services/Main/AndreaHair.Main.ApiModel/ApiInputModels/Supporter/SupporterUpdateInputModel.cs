﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Supporter
{
    public class SupporterUpdateInputModel : IApiInput
    {
        [Required]
        public long SupporterId { get; set; }

        [Required]
        public SupporterUpdateInput Details { get; set; }
    }

    public class SupporterUpdateInput
    {
        [Required]
        public string SupporterName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }

        public string FacebookUrl { get; set; }

        public string InstagramUrl { get; set; }

        public string WhatsappPhoneNumber { get; set; }

        [Required]
        public long AvatarId { get; set; }
    }
}