﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Supporter
{
    public class SupporterDeleteInputModel : IApiInput
    {
        [Required]
        public long SupporterId { get; set; }
    }
}