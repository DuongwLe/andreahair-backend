﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Supporter
{
    public class SupporterSetEnabledInputModel : IApiInput
    {
        [Required]
        public long SupporterId { get; set; }

        [Required]
        public SupporterSetEnabledInput Details { get; set; }
    }

    public class SupporterSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}

