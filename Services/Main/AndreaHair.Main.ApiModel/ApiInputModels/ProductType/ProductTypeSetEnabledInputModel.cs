﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.ProductType
{
    public class ProductTypeSetEnabledInputModel : IApiInput
    {
        [Required]
        public long ProductTypeId { get; set; }

        [Required]
        public ProductTypeSetEnabledInput Details { get; set; }
    }

    public class ProductTypeSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}