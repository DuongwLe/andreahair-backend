﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.ProductType
{
    public class ProductTypeSearchInputModel : IApiInput
    {
        [Range(1, 50)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }

        [StringLength(128)]
        public string Keyword { get; set; }

        public ProductTypeSearchInput Details { get; set; }
    }

    public class ProductTypeSearchInput
    {
        public long? CategoryId { get; set; }
    }
}