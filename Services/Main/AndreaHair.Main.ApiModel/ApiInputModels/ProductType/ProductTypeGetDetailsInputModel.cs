﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.ProductType
{
    public class ProductTypeGetDetailsInputModel : IApiInput
    {
        [Required]
        public long ProductTypeId { get; set; }
    }
}