﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.ProductType
{
    public class ProductTypeUpdateInputModel : IApiInput
    {
        [Required]
        public long ProductTypeId { get; set; }

        [Required]
        public ProductTypeUpdateInput Details { get; set; }
    }

    public class ProductTypeUpdateInput
    {
        [Required]
        public string ProductTypeNameEn { get; set; }

        [Required]
        public string ProductTypeNameRu { get; set; }

        [Required]
        public long CategoryId { get; set; }
    }
}