﻿namespace AndreaHair.Main.ApiModel.ApiInputModels.ProductType
{
    public class ProductTypeGetAllEnabledInputModel : IApiInput
    {
        public long? CategoryId { get; set; }
    }
}

