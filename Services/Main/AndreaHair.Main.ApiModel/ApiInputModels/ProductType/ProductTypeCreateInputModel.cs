﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.ProductType
{
    public class ProductTypeCreateInputModel : IApiInput
    {
        [Required]
        public string ProductTypeNameEn { get; set; }

        [Required]
        public string ProductTypeNameRu { get; set; }

        [Required]
        public long CategoryId { get; set; }
    }
}