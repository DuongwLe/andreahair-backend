﻿using System;
using System.ComponentModel.DataAnnotations;
using AndreaHair.Commons.Enums;

namespace AndreaHair.Main.ApiModel.ApiInputModels.PhysicalFile
{
    public class PhysicalFileRequestUploadInputModel : IApiInput
    {
        [Required]
        public PhysicalFileTypeEnum PhysicalFileTypeId { get; set; }

        /// <summary>
        /// Include extension
        /// </summary>
        [Required]
        public string FileName { get; set; }

        /// <summary>
        /// Max file size: no limit
        /// </summary>
        [Required]
        public long FileLength { get; set; }
    }
}

