﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.PhysicalFile
{
    public class PhysicalFileMarkUploadDoneInputModel : IApiInput
    {
        [Required]
        public long[] PhysicalFileIds { get; set; }
    }
}

