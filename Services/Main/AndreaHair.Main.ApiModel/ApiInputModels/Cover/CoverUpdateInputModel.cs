using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Cover
{
    public class CoverUpdateInputModel : IApiInput
    {
        [Required]
        public long CoverId { get; set; }

        [Required]
        public CoverUpdateInput Details { get; set; }
    }

    public class CoverUpdateInput
    {
        [Required]
        [StringLength(128)]
        public string CoverName { get; set; }

        [Required]
        public long PhysicalFileId { get; set; }
    }
}