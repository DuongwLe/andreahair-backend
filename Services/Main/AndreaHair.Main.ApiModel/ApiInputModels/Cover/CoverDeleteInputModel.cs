using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Cover
{
    public class CoverDeleteInputModel : IApiInput
    {
        [Required]
        public long CoverId { get; set; }
    }
}