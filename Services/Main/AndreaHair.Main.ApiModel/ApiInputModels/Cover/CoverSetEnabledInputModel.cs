using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Cover
{
    public class CoverSetEnabledInputModel : IApiInput
    {
        [Required]
        public long CoverId { get; set; }

        [Required]
        public CoverSetEnabledInput Details { get; set; }
    }

    public class CoverSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}