using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Cover
{
    public class CoverCreateInputModel : IApiInput
    {
        [Required]
        [StringLength(128)]
        public string CoverName { get; set; }

        [Required]
        public long PhysicalFileId { get; set; }

        [Required]
        public bool IsEnabled { get; set; }
    }
}