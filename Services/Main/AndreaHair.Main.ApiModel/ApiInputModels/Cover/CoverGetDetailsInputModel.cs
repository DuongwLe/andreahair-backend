using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Cover
{
    public class CoverGetDetailsInputModel : IApiInput
    {
        [Required]
        public long CoverId { get; set; }
    }
}