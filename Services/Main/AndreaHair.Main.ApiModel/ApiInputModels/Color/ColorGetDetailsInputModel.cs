﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Color
{
    public class ColorGetDetailsInputModel : IApiInput
    {
        [Required]
        public long ColorId { get; set; }
    }
}