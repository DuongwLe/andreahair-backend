﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Color
{
    public class ColorCreateInputModel : IApiInput
    {
        [Required]
        public string ColorNameEn { get; set; }

        [Required]
        public string ColorNameRu { get; set; }

        [Required]
        public string[] ColorCodes { get; set; }
    }
}