﻿using System.ComponentModel.DataAnnotations;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Color
{
    public class ColorSetEnabledInputModel : IApiInput
    {
        [Required]
        public long ColorId { get; set; }

        [Required]
        public ColorSetEnabledInput Details { get; set; }
    }

    public class ColorSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}