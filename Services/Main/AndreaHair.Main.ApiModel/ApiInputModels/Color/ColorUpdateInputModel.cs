﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiModel.ApiInputModels.Color
{
    public class ColorUpdateInputModel : IApiInput
    {
        [Required]
        public long ColorId { get; set; }

        [Required]
        public ColorUpdateInput Details { get; set; }
    }

    public class ColorUpdateInput
    {
        [Required]
        public string ColorNameEn { get; set; }

        [Required]
        public string ColorNameRu { get; set; }

        [Required]
        public string[] ColorCodes { get; set; }
    }
}
