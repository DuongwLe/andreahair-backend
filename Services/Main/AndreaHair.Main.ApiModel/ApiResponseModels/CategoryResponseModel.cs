﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class CategoryResponseModel : IApiResponseData
    {
        public long CategoryId { get; set; }

        public string CategoryNameEn { get; set; }

        public string CategoryNameRu { get; set; }

        public bool IsEnabled { get; set; }
    }
}