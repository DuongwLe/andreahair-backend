﻿using AndreaHair.Commons.Enums;
using System;

namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class UserProfileResponseModel : IApiResponseData
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public UserTypeEnum UserTypeId { get; set; }
    }
}