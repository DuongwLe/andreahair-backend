namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class CoverAllEnabledResponseModel : IApiResponseData
    {
        public string[] Urls { get; set; }
    }
}