﻿using System;

namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class UserResponseModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int StatusId { get; set; }
    }
}

