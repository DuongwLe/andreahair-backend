﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ContactResponseModel : IApiResponseData
    {
        public long ContactId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Message { get; set; }

        public string ProductName { get; set; }

        public long CreateAtUnix { get; set; }
    }
}