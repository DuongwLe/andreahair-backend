﻿using System;

namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class AuthLoginResponseModel : IApiResponseData
    {
        public AuthTokenResponseModel Token { get; set; }
        public UserResponseModel User { get; set; }
    }
}

