﻿using AndreaHair.Commons.Enums;

namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ProductDetailsForAdminResponseModel : IApiResponseData
    {
        public long ProductGroupId { get; set; }

        public string ProductNameEn { get; set; }

        public string ProductNameRu { get; set; }

        public long CategoryId { get; set; }

        public long ProductTypeId { get; set; }

        public string VideoUrl { get; set; }

        public MaterialTypeEnum MaterialTypeId { get; set; }

        public HairStyleEnum HairStyleId { get; set; }

        public MeasureUnitLengthEnum MeasureUnitLengthId { get; set; }

        public decimal FromLength { get; set; }

        public decimal ToLength { get; set; }

        public MeasureUnitWeightEnum MeasureUnitWeightId { get; set; }

        public decimal Weight { get; set; }

        public string Origin { get; set; }

        public PackingRuleEnum PackingRuleId { get; set; }

        public string DescriptionEn { get; set; }

        public string DescriptionRu { get; set; }

        public ProductColorResponseModel[] Colors { get; set; }
    }

    public class ProductColorResponseModel : ColorResponseModel
    {
        public long ProductId { get; set; }

        public bool IsBestSelling { get; set; }

        public ImageResponseModel[] Images { get; set; }
    }

    public class ImageResponseModel
    {
        public long ImageId { get; set; }

        public string ImageFileUrl { get; set; }

        public bool IsMainImage { get; set; }
    }
}
