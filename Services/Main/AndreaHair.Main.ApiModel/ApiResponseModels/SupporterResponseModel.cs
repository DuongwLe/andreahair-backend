﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class SupporterResponseModel : IApiResponseData
    {
        public long SupporterId { get; set; }

        public string SupporterName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string FacebookUrl { get; set; }

        public string InstagramUrl { get; set; }

        public string WhatsappPhoneNumber { get; set; }

        public long AvatarId { get; set; }

        public string AvatarUrl { get; set; }

        public bool IsEnabled { get; set; }
    }
}