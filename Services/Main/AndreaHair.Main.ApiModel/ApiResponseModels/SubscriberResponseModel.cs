﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class SubscriberResponseModel : IApiResponseData
    {
        public long SubcriberId { get; set; }

        public string Email { get; set; }

        public long CreateAtUnix { get; set; }
    }
}