﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ProductTypeResponseModel : IApiResponseData
    {
        public long ProductTypeId { get; set; }

        public string ProductTypeNameEn { get; set; }

        public string ProductTypeNameRu { get; set; }

        public long CategoryId { get; set; }

        public string CategoryName { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsBestSelling { get; set; }
    }
}
