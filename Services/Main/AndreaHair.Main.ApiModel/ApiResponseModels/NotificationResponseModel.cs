namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class NotificationResponseModel : IApiResponseData
    {
        public long NotificationId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string DataJson { get; set; }
        public long? ActualSentAtUnix { get; set; }
        public bool IsRead { get; set; }
    }
}