namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class NotificationUnreadCountResponseModel : IApiResponseData
    {
        public int Count { get; set; }
    }
}