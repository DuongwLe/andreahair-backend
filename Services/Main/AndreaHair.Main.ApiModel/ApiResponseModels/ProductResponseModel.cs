﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ProductResponseModel : IApiResponseData
    {
        public long ProductGroupId { get; set; }

        public string ProductNameEn { get; set; }

        public string ProductNameRu { get; set; }

        public string ProductTypeName { get; set; }

        public int NumberOfColor { get; set; }

        public bool IsEnabled { get; set; }
    }
}