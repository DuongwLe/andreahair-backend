﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ColorAllEnabledResponseModel : IApiResponseData
    {
        public long ColorId { get; set; }
        public string ColorName { get; set; }
        public string[] ColorCode { get; set; }
    }
}

