﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ProducTypeAllEnabledResponseModel : IApiResponseData
    {
        public long ProductTypeId { get; set; }

        public string ProductTypeName { get; set; }
    }
}

