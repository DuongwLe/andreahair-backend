﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ProductAllEnabledResponseModel : IApiResponseData
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductTypeName { get; set; }
        public string MainImageUrl { get; set; }
        public string[] ImageUrls { get; set; }
        public bool IsBestSelling { get; set; }
    }
}

