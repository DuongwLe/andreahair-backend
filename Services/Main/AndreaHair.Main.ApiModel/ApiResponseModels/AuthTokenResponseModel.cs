﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class AuthTokenResponseModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string TokenType { get; set; }
        public int ExpiresIn { get; set; }
    }
}

