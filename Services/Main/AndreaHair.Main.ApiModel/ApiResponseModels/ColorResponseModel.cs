﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ColorResponseModel : IApiResponseData
    {
        public long ColorId { get; set; }

        public string ColorNameEn { get; set; }

        public string ColorNameRu { get; set; }

        public string[] ColorCodes { get; set; }

        public bool IsEnabled { get; set; }
    }
}
