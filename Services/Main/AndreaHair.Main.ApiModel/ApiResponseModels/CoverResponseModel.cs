namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class CoverResponseModel : IApiResponseData
    {
        public long CoverId { get; set; }
        public string CoverName { get; set; }
        public long FileId { get; set; }
        public string FileUrl { get; set; }
        public bool IsEnabled { get; set; }
    }
}