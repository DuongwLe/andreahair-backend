﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class CategoryAllEnabledResponseModel : IApiResponseData
    {
        public long CategoryId { get; set; }

        public string CategoryName { get; set; }

        public ProducTypeAllEnabledResponseModel[] ProductTypes { get; set; }
    }
}
