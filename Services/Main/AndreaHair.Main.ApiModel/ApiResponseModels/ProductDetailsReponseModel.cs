﻿using AndreaHair.Commons.Enums;

namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class ProductDetailsResponseModel : IApiResponseData
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ColorName { get; set; }
        public string[] ColorCode { get; set; }
        public ReferenceProductModel[] ReferenceProducts { get; set; }
        public string[] ImageUrls { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long ProductTypeId { get; set; }
        public string ProductTypeName { get; set; }
        public MaterialTypeEnum MaterialTypeId { get; set; }
        public HairStyleEnum HairStyleId { get; set; }
        public MeasureUnitLengthEnum MeasureUnitLengthId { get; set; }
        public decimal FromLength { get; set; }
        public decimal ToLength { get; set; }
        public MeasureUnitWeightEnum MeasureUnitWeightId { get; set; }
        public decimal Weight { get; set; }
        public string Origin { get; set; }
        public PackingRuleEnum PackingRuleId { get; set; }
        public string VideoUrl { get; set; }
        public string Description { get; set; }
        public bool IsBestSelling { get; set; }
    }

    public class ReferenceProductModel
    {
        public long ProductId { get; set; }
        public string[] ColorCode { get; set; }
    }
}

