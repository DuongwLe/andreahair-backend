﻿namespace AndreaHair.Main.ApiModel.ApiResponseModels
{
    public class PhysicalFileRequestUploadResponseModel : IApiResponseData
    {
        public long PhysicalFileId { get; set; }

        public string PhysicalFileName { get; set; }

        public string PresignedUploadUrl { get; set; }
    }
}

