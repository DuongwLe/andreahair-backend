﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class SearchHandler : IRequestHandler<ApiActionAuthenticatedRequest<SupporterSearchInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public SearchHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SupporterSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = from s in _dbContext.Supporters
                        select new
                        {
                            s.SupporterId,
                            s.SupporterName,
                            s.PhoneNumber,
                            s.Email,
                            s.FacebookUrl,
                            s.InstagramUrl,
                            s.WhatsappPhoneNumber,
                            s.AvatarId,
                            s.Avatar.S3FileKey,
                            s.IsEnabled
                        };

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Match(item.SupporterName, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                         EF.Functions.Like(item.SupporterName, $"%{request.Input.Keyword}%")
                    select item;
            }

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order by
            query = query.OrderBy(x => x.SupporterName);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new SupporterResponseModel
                {
                    SupporterId = x.SupporterId,
                    SupporterName = x.SupporterName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    FacebookUrl = x.FacebookUrl,
                    InstagramUrl = x.InstagramUrl,
                    WhatsappPhoneNumber = x.WhatsappPhoneNumber,
                    AvatarId = x.AvatarId,
                    AvatarUrl = x.S3FileKey != null ? _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x.S3FileKey) : null,
                    IsEnabled = x.IsEnabled.Value
                }).ToList(),
                requestPaging);
        }
    }
}
