﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<SupporterCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SupporterCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var supporterName = await _dbContext.Supporters.AnyAsync(x => x.SupporterName == request.Input.SupporterName, cancellationToken);

            if (supporterName)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedSupporterName);
            }

            var phoneNumber = await _dbContext.Supporters.AnyAsync(x => x.PhoneNumber == request.Input.PhoneNumber, cancellationToken);

            if (phoneNumber)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedSupporterPhoneNumber);
            }

            var email = await _dbContext.Supporters.AnyAsync(x => x.Email == request.Input.Email, cancellationToken);

            if (email)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedSupporterEmail);
            }
            if (!ValidationHelper.IsEmail(request.Input.Email))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidEmail);
            }
            #endregion

            var newSupporter = new Supporter
            {
                SupporterName = request.Input.SupporterName,
                PhoneNumber = request.Input.PhoneNumber,
                Email = request.Input.Email,
                FacebookUrl = request.Input.FacebookUrl,
                InstagramUrl = request.Input.InstagramUrl,
                WhatsappPhoneNumber = request.Input.WhatsappPhoneNumber,
                AvatarId = request.Input.AvatarId,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.Supporters.Add(newSupporter);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}