﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionAuthenticatedRequest<SupporterSetEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SetEnabledHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SupporterSetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var supporter = await (from x in _dbContext.Supporters
                                   where x.SupporterId == request.Input.SupporterId
                                   select x).FirstOrDefaultAsync(cancellationToken);

            if (supporter == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SupporterNotFound);
            }

            supporter.IsEnabled = request.Input.Details.IsEnabled;
            supporter.UpdatedAtUtc = DateTime.UtcNow;
            supporter.UpdatedByUserId = request.UserId;
            _dbContext.Supporters.Update(supporter);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}

