﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<SupporterDeleteInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public DeleteHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SupporterDeleteInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var supporter = await _dbContext.Supporters
                .FirstOrDefaultAsync(x => x.SupporterId == request.Input.SupporterId, cancellationToken);

            if (supporter == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SupporterNotFound);
            }
            #endregion

            // Delete supporter
            await _dbContext.Supporters
                .Where(x => x.SupporterId == request.Input.SupporterId)
                .UpdateFromQueryAsync(pt => new Supporter
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            // Delete avatar
            await _dbContext.PhysicalFiles
                .Where(x => x.PhysicalFileId == supporter.AvatarId)
                .UpdateFromQueryAsync(pt => new PhysicalFile
                {
                    PhysicalFileStatusId = (short)PhysicalFileStatusEnum.WillBeDeleted,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
