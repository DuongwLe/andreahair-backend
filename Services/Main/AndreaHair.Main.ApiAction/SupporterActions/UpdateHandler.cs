﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<SupporterUpdateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public UpdateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }
        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SupporterUpdateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var supporter = await _dbContext.Supporters
                .Where(x => x.SupporterId == request.Input.SupporterId)
                .FirstOrDefaultAsync(cancellationToken);

            if (supporter == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SupporterNotFound);
            }

            var supporterName = await _dbContext.Supporters
                .Where(x => x.SupporterId != request.Input.SupporterId &&
                    x.SupporterName == request.Input.Details.SupporterName)
                .AnyAsync(cancellationToken);

            if (supporterName)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedSupporterName);
            }

            var phoneNumber = await _dbContext.Supporters
                .AnyAsync(x => x.SupporterId != request.Input.SupporterId &&
                    x.PhoneNumber == request.Input.Details.PhoneNumber, cancellationToken);

            if (phoneNumber)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedSupporterPhoneNumber);
            }

            var email = await _dbContext.Supporters
                .AnyAsync(x => x.SupporterId != request.Input.SupporterId &&
                    x.Email == request.Input.Details.Email, cancellationToken);

            if (email)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedSupporterEmail);
            }
            if (!ValidationHelper.IsEmail(request.Input.Details.Email))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidEmail);
            }
            #endregion

            if (supporter.AvatarId != request.Input.Details.AvatarId)
            {
                // Delete old avatar
                await _dbContext.PhysicalFiles
                    .Where(x => x.PhysicalFileId == supporter.AvatarId)
                    .UpdateFromQueryAsync(pt => new PhysicalFile
                    {
                        PhysicalFileStatusId = (short)PhysicalFileStatusEnum.WillBeDeleted,
                        UpdatedByUserId = request.UserId,
                        UpdatedAtUtc = DateTime.UtcNow
                    }, cancellationToken);
            }

            supporter.SupporterName = request.Input.Details.SupporterName;
            supporter.PhoneNumber = request.Input.Details.PhoneNumber;
            supporter.Email = request.Input.Details.Email;
            supporter.FacebookUrl = request.Input.Details.FacebookUrl;
            supporter.InstagramUrl = request.Input.Details.InstagramUrl;
            supporter.WhatsappPhoneNumber = request.Input.Details.WhatsappPhoneNumber;
            supporter.AvatarId = request.Input.Details.AvatarId;
            supporter.UpdatedAtUtc = DateTime.UtcNow;
            supporter.UpdatedByUserId = request.UserId;
            _dbContext.Supporters.Update(supporter);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}