﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class GetAllEnabledHandler : IRequestHandler<ApiActionAnonymousRequest<SupporterGetAllEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetAllEnabledHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<SupporterGetAllEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var query = from s in _dbContext.Supporters
                        where s.IsEnabled == true
                        select new
                        {
                            s.SupporterId,
                            s.SupporterName,
                            s.PhoneNumber,
                            s.Email,
                            s.FacebookUrl,
                            s.InstagramUrl,
                            s.WhatsappPhoneNumber,
                            s.Avatar.S3FileKey
                        };

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new SupporterResponseModel
                {
                    SupporterId = x.SupporterId,
                    SupporterName = x.SupporterName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    FacebookUrl = x.FacebookUrl,
                    InstagramUrl = x.InstagramUrl,
                    WhatsappPhoneNumber = x.WhatsappPhoneNumber,
                    AvatarUrl = x.S3FileKey != null ? _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x.S3FileKey) : null
                }).ToList(),
                requestPaging);
        }
    }
}