﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace AndreaHair.Main.ApiAction.SupporterActions
{
    public class GetDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<SupporterGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetDetailsHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SupporterGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var supporterId = await _dbContext.Supporters
               .AnyAsync(x => x.SupporterId == request.Input.SupporterId, cancellationToken);

            if (!supporterId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SupporterNotFound);
            }
            #endregion

            var supporter = await (from s in _dbContext.Supporters
                                   where s.SupporterId == request.Input.SupporterId
                                   select new
                                   {
                                       s.SupporterId,
                                       s.SupporterName,
                                       s.PhoneNumber,
                                       s.Email,
                                       s.FacebookUrl,
                                       s.InstagramUrl,
                                       s.WhatsappPhoneNumber,
                                       s.AvatarId,
                                       s.Avatar.S3FileKey
                                   }).FirstOrDefaultAsync(cancellationToken);

            return ApiResponse.CreateModel(new SupporterResponseModel
            {
                SupporterId = supporter.SupporterId,
                SupporterName = supporter.SupporterName,
                PhoneNumber = supporter.PhoneNumber,
                Email = supporter.Email,
                FacebookUrl = supporter.FacebookUrl,
                InstagramUrl = supporter.InstagramUrl,
                WhatsappPhoneNumber = supporter.WhatsappPhoneNumber,
                AvatarId = supporter.AvatarId,
                AvatarUrl = supporter.S3FileKey != null ? _s3Service.GetTempPublicUrl(_s3Settings.BucketName, supporter.S3FileKey) : null
            });
        }
    }
}
