﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Subcriber;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.SubscriberActions
{
    public class GetAllHandler : IRequestHandler<ApiActionAuthenticatedRequest<SubscriberGetAllInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetAllHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<SubscriberGetAllInputModel> request, CancellationToken cancellationToken)
        {
            var query = from sb in _dbContext.Subscribers
                        select new
                        {
                            sb.SubscriberId,
                            sb.Email,
                            sb.CreatedAtUtc
                        };

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order items
            query = query.OrderByDescending(x => x.CreatedAtUtc);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new SubscriberResponseModel
                {
                    SubcriberId = x.SubscriberId,
                    Email = x.Email,
                    CreateAtUnix = DateTimeHelper.ToUnixTime(x.CreatedAtUtc)
                }).ToList(),
                requestPaging);
        }
    }
}