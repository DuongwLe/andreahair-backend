﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Subcriber;
using AndreaHair.Main.ApiService;
using Hangfire;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.SubscriberActions
{
    public class SubscribeHandler : IRequestHandler<ApiActionAnonymousRequest<SubscriberCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SubscribeHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<SubscriberCreateInputModel> request, CancellationToken cancellationToken)
        {
            if (!ValidationHelper.IsEmail(request.Input.Email))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidEmail);
            }

            // Check if email existed
            var subcriber = await (from x in _dbContext.Subscribers
                                   where x.Email == request.Input.Email
                                   select x).AnyAsync(cancellationToken);

            if (subcriber)
            {
                return ApiResponse.CreateModel(HttpStatusCode.OK);
            }

            var newSubscriber = new Subscriber
            {
                Email = request.Input.Email,
                CreatedAtUtc = DateTime.UtcNow
            };
            _dbContext.Subscribers.Add(newSubscriber);
            await _dbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<INotificationService>(service =>
                service.TriggerSubscribe(newSubscriber.SubscriberId, CancellationToken.None));

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}