﻿using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CoverActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<CoverCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CoverCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var coverName = await _dbContext.Covers.AnyAsync(x => x.CoverName == request.Input.CoverName, cancellationToken);

            if (coverName)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateCoverName);
            }

            var physicalFile = await _dbContext.PhysicalFiles
                .Where(x => x.PhysicalFileId == request.Input.PhysicalFileId &&
                    x.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                .AnyAsync(cancellationToken);

            if (!physicalFile)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ImageNotFound);
            }
            #endregion

            var newCover = new Cover
            {
                CoverName = request.Input.CoverName,
                PhysicalFileId = request.Input.PhysicalFileId,
                IsEnabled = request.Input.IsEnabled,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.Covers.Add(newCover);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(System.Net.HttpStatusCode.OK);
        }
    }
}
