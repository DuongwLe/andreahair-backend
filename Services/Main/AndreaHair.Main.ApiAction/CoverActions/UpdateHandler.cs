﻿using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CoverActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<CoverUpdateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public UpdateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CoverUpdateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var cover = await _dbContext.Covers
                    .Where(x => x.CoverId == request.Input.CoverId)
                    .FirstOrDefaultAsync(cancellationToken);

            if (cover == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CoverNotFound);
            }

            var coverName = await _dbContext.Covers
                .Where(x => x.CoverId != request.Input.CoverId && x.CoverName == request.Input.Details.CoverName)
                .AnyAsync(cancellationToken);

            if (coverName)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateCoverName);
            }

            var physicalFile = await _dbContext.PhysicalFiles
                .Where(x => x.PhysicalFileId == request.Input.Details.PhysicalFileId &&
                    x.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                .AnyAsync(cancellationToken);

            if (!physicalFile)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ImageNotFound);
            }
            #endregion

            cover.CoverName = request.Input.Details.CoverName;
            cover.PhysicalFileId = request.Input.Details.PhysicalFileId;
            cover.UpdatedAtUtc = DateTime.UtcNow;
            cover.UpdatedByUserId = request.UserId;
            _dbContext.Covers.Update(cover);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
