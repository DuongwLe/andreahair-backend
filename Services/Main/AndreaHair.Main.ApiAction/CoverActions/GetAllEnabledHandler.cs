﻿using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CoverActions
{
    public class GetAllEnabledHandler : IRequestHandler<ApiActionAnonymousRequest<CoverGetAllEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetAllEnabledHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<CoverGetAllEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var cover = await (from c in _dbContext.Covers
                               where
                               c.IsEnabled.Value &&
                               c.PhysicalFile.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK
                               select new
                               {
                                   Urls = _s3Service.GetTempPublicUrl(_s3Settings.BucketName, c.PhysicalFile.S3FileKey)
                               }).ToListAsync(cancellationToken);

            return ApiResponse.CreateModel(new CoverAllEnabledResponseModel
            {
                Urls = cover.Select(x => x.Urls).ToArray()
            });
        }
    }
}
