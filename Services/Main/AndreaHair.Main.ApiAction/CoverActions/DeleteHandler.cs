﻿using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CoverActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<CoverDeleteInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public DeleteHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CoverDeleteInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var cover = await _dbContext.Covers
                .Where(x => x.CoverId == request.Input.CoverId)
                .FirstOrDefaultAsync(cancellationToken);

            if (cover == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CoverNotFound);
            }
            #endregion

            // Delete cover
            await _dbContext.Covers
                .Where(x => x.CoverId == request.Input.CoverId)
                .UpdateFromQueryAsync(pt => new Cover
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            // Delete image
            await _dbContext.PhysicalFiles
                .Where(x => x.PhysicalFileId == cover.PhysicalFileId)
                .UpdateFromQueryAsync(pt => new PhysicalFile
                {
                    PhysicalFileStatusId = (short)PhysicalFileStatusEnum.WillBeDeleted,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
