﻿using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CoverActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionAuthenticatedRequest<CoverSetEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SetEnabledHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CoverSetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var cover = await _dbContext.Covers
                .Where(x => x.CoverId == request.Input.CoverId)
                .FirstOrDefaultAsync(cancellationToken);

            if (cover == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CoverNotFound);
            }
            #endregion

            cover.IsEnabled = request.Input.Details.IsEnabled;
            cover.UpdatedAtUtc = DateTime.UtcNow;
            cover.UpdatedByUserId = request.UserId;
            _dbContext.Covers.Update(cover);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
