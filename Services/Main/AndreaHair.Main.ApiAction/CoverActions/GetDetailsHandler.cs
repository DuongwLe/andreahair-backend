﻿using AndreaHair.Commons;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CoverActions
{
    public class GetDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<CoverGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetDetailsHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CoverGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            var cover = await (from c in _dbContext.Covers
                               where c.CoverId == request.Input.CoverId
                               select new CoverResponseModel
                               {
                                   CoverId = c.CoverId,
                                   CoverName = c.CoverName,
                                   FileId = c.PhysicalFileId,
                                   FileUrl = _s3Service.GetTempPublicUrl(_s3Settings.BucketName, c.PhysicalFile.S3FileKey),
                                   IsEnabled = c.IsEnabled.Value
                               }).FirstOrDefaultAsync(cancellationToken);

            if (cover == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CoverNotFound);
            }

            return ApiResponse.CreateModel(cover);
        }
    }
}
