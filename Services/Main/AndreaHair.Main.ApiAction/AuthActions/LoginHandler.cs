﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Constants;
using AndreaHair.Commons.Enums;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Auth;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using IdentityModel.Client;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.AuthActions
{
    public class LoginHandler : IRequestHandler<ApiActionAnonymousRequest<AuthLoginInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly TokenClient _tokenClient;

        public LoginHandler(MainDbContext dbContext, TokenClient tokenClient)
        {
            _dbContext = dbContext;
            _tokenClient = tokenClient;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<AuthLoginInputModel> request, CancellationToken cancellationToken)
        {
            var token = await _tokenClient.RequestPasswordTokenAsync(request.Input.UserName, request.Input.Password, "main.read_write offline_access", cancellationToken: cancellationToken);

            if (token.IsError)
            {
                if (token.ErrorDescription == OAuthConstants.ErrorMessage.CannotFindUser || token.ErrorDescription == OAuthConstants.ErrorMessage.PasswordIncorrect)
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidUsernameOrPassword);
                }
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.LoginFailed);
            }
            if (!token.Json.TryGetProperty("userId", out var userIdValue))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.LoginFailed);
            }
            if (string.IsNullOrEmpty(userIdValue.ToString()))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.LoginFailed);
            }

            var users = await (
                from u in _dbContext.Users
                where
                u.UserId == Guid.Parse(userIdValue.ToString())
                select new
                {
                    u.UserId,
                    u.UserName,
                    u.StatusId
                })
                .ToArrayAsync(cancellationToken);
            if (users == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.LoginFailed);
            }

            var userGroup = users
               .GroupBy(u => new { u.UserId, u.UserName, u.StatusId })
               .Select(g => new
               {
                   User = new UserResponseModel
                   {
                       UserId = g.Key.UserId,
                       UserName = g.Key.UserName,
                       StatusId = g.Key.StatusId
                   }
               })
               .FirstOrDefault();

            if (userGroup.User.StatusId == (int)UserStatusEnum.WaitForVerify)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.UserNotVerified);
            }

            return ApiResponse.CreateModel(new AuthLoginResponseModel
            {
                Token = new AuthTokenResponseModel
                {
                    AccessToken = token.AccessToken,
                    ExpiresIn = token.ExpiresIn,
                    RefreshToken = token.RefreshToken,
                    TokenType = token.TokenType
                },
                User = userGroup.User
            });
        }
    }
}