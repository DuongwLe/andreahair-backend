﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Auth;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Sodium;

namespace AndreaHair.Main.ApiAction.AuthActions
{
    public class RegisterHandler : IRequestHandler<ApiActionAnonymousRequest<AuthRegisterInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public RegisterHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<AuthRegisterInputModel> request, CancellationToken cancellationToken)
        {
            // Check if email existed
            var user = await (from u in _dbContext.Users
                              where u.UserName == request.Input.Email
                              select u).FirstOrDefaultAsync(cancellationToken);
            if (user != null && user.StatusId >= (int)UserStatusEnum.Normal)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.EmailAlreadyUsed);
            }

            // Add new user
            // If user is waiting for verify then we don't need to create new user
            if (user == null)
            {
                // New user
                var userId = Guid.NewGuid();
                var salt = Convert.ToBase64String(PasswordHash.ScryptGenerateSalt());
                user = new User
                {
                    UserId = userId,
                    UserName = request.Input.Email,
                    PasswordHashed = PasswordHash.ScryptHashString(request.Input.Password + salt),
                    Salt = salt,
                    UserTypeId = (int)UserTypeEnum.Admin,
                    StatusId = (int)UserStatusEnum.Normal,
                    CreatedAtUtc = DateTime.UtcNow,
                    UpdatedAtUtc = DateTime.UtcNow
                };
                _dbContext.Users.Add(user);

                await _dbContext.SaveChangesAsync(cancellationToken);
            }

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}