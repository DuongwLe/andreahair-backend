﻿using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Auth;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using IdentityModel.Client;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.AuthenticationActions
{
    public class RefreshTokenHandler : IRequestHandler<ApiActionAnonymousRequest<AuthRefreshTokenInputModel>, IApiResponse>
    {
        private readonly TokenClient _tokenClient;

        public RefreshTokenHandler(TokenClient tokenClient)
        {
            _tokenClient = tokenClient;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<AuthRefreshTokenInputModel> request, CancellationToken cancellationToken)
        {
            var token = await _tokenClient.RequestRefreshTokenAsync(request.Input.RefreshToken, "main.read_write offline_access", cancellationToken: cancellationToken);

            if (token.IsError)
            {
                return ApiResponse.CreateErrorModel(System.Net.HttpStatusCode.BadRequest, ApiInternalErrorMessages.TokenError);
            }

            return ApiResponse.CreateModel(new UserTokenResponseModel()
            {
                AccessToken = token.AccessToken,
                ExpiresIn = token.ExpiresIn,
                RefreshToken = token.RefreshToken,
                TokenType = token.TokenType
            });
        }
    }
}