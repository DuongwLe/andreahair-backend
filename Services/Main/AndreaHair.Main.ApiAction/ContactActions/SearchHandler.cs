﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Contact;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ContactActions
{
    public class SearchHandler : IRequestHandler<ApiActionAuthenticatedRequest<ContactSearchInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SearchHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ContactSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = from x in _dbContext.Contacts
                        select new
                        {
                            x.ContactId,
                            x.Email,
                            x.Message,
                            x.Product.ProductGroup.ProductGroupNameEn,
                            x.CreatedAtUtc
                        };

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order by
            query = query.OrderByDescending(x => x.CreatedAtUtc);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new ContactResponseModel
                {
                    ContactId = x.ContactId,
                    Email = x.Email,
                    Message = x.Message,
                    ProductName = x.ProductGroupNameEn,
                    CreateAtUnix = DateTimeHelper.ToUnixTime(x.CreatedAtUtc)
                }).ToList(),
                requestPaging);
        }
    }
}