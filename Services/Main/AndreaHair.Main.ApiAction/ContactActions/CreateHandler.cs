﻿using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Contact;
using AndreaHair.Main.ApiService;
using Hangfire;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ContactActions
{
    public class CreateHandler : IRequestHandler<ApiActionAnonymousRequest<ContactCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ContactCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            if (!ValidationHelper.IsEmail(request.Input.Email))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidEmail);
            }

            if (request.Input.ProductId.HasValue)
            {
                var productId = await _dbContext.Products
                    .AnyAsync(x => x.ProductId == request.Input.ProductId, cancellationToken);

                if (!productId)
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
                }
            }
            #endregion

            var newContact = new Contact
            {
                FirstName = request.Input.FirstName,
                LastName = request.Input.LastName,
                Email = request.Input.Email,
                PhoneNumber = request.Input.PhoneNumber,
                Message = request.Input.Message,
                ProductId = request.Input.ProductId.HasValue ? request.Input.ProductId.Value : null,
                CreatedAtUtc = DateTime.UtcNow
            };
            _dbContext.Contacts.Add(newContact);
            await _dbContext.SaveChangesAsync(cancellationToken);

            BackgroundJob.Enqueue<INotificationService>(service =>
                service.TriggerCreateNonUserMessage(newContact.ContactId, CancellationToken.None));

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
