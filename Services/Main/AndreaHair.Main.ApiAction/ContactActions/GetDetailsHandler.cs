﻿using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Contact;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ContactActions
{
    public class GetDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<ContactGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetDetailsHandler(IDbContextHelper _dbContextHelper)
        {
            _dbContext = _dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ContactGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            var contact = await (from x in _dbContext.Contacts
                                 where x.ContactId == request.Input.ContactId
                                 select new ContactResponseModel
                                 {
                                     ContactId = x.ContactId,
                                     FirstName = x.FirstName,
                                     LastName = x.LastName,
                                     Email = x.Email,
                                     PhoneNumber = x.PhoneNumber,
                                     Message = x.Message,
                                     ProductName = x.ProductId.HasValue ? x.Product.ProductGroup.ProductGroupNameEn : null
                                 }).FirstOrDefaultAsync(cancellationToken);

            if (contact == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ContactNotFound);
            }

            return ApiResponse.CreateModel(contact);
        }
    }
}