﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class GetAllProductTypesHandler : IRequestHandler<ApiActionAnonymousRequest<CategoryGetAllProductTypesInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetAllProductTypesHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<CategoryGetAllProductTypesInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();

            var allProducts = await (from c in _dbContext.Categories
                                     where c.IsEnabled.Value
                                     select new CategoryAllEnabledResponseModel
                                     {
                                         CategoryId = c.CategoryId,
                                         CategoryName = StringHelper.SwitchLanguage(localization, c.CategoryNameEn, c.CategoryNameRu),
                                         ProductTypes = c.ProductTypes.Select(x => new ProducTypeAllEnabledResponseModel
                                         {
                                             ProductTypeId = x.ProductTypeId,
                                             ProductTypeName = StringHelper.SwitchLanguage(localization, x.ProductTypeNameEn, x.ProductTypeNameRu),
                                         }).ToArray()
                                     }).ToArrayAsync(cancellationToken);

            return ApiResponse.CreateModel(allProducts);
        }
    }
}