﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class GetAllEnabledHandler : IRequestHandler<ApiActionAnonymousRequest<CategoryGetAllEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetAllEnabledHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<CategoryGetAllEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();
            var data = await _dbContext.Categories
                .Where(x => x.IsEnabled.Value)
                .Select(x => new CategoryAllEnabledResponseModel
                {
                    CategoryId = x.CategoryId,
                    CategoryName = StringHelper.SwitchLanguage(localization, x.CategoryNameEn, x.CategoryNameRu)
                })
                .ToArrayAsync(cancellationToken);

            return ApiResponse.CreateModel(data);
        }
    }
}

