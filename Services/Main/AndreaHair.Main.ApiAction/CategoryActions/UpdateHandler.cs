﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<CategoryUpdateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public UpdateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CategoryUpdateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var category = await (from c in _dbContext.Categories
                                  where c.CategoryId == request.Input.CategoryId
                                  select c).FirstOrDefaultAsync(cancellationToken);

            if (category == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryNotFound);
            }

            var categoryNameEn = await (from c in _dbContext.Categories
                                        where
                                        c.CategoryId != request.Input.CategoryId &&
                                        c.CategoryNameEn == request.Input.Details.CategoryNameEn
                                        select c).AnyAsync(cancellationToken);

            if (categoryNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedCategoryNameEn);
            }

            var categoryNameRu = await (from c in _dbContext.Categories
                                        where
                                        c.CategoryId != request.Input.CategoryId &&
                                        c.CategoryNameRu == request.Input.Details.CategoryNameRu
                                        select c).AnyAsync(cancellationToken);

            if (categoryNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedCategoryNameRu);
            }
            #endregion

            category.CategoryNameEn = request.Input.Details.CategoryNameEn;
            category.CategoryNameRu = request.Input.Details.CategoryNameRu;
            category.UpdatedAtUtc = DateTime.UtcNow;
            category.UpdatedByUserId = request.UserId;
            _dbContext.Categories.Update(category);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
