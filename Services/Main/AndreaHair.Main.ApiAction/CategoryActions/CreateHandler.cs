﻿using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<CategoryCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }
        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CategoryCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var categoryNameEn = await _dbContext.Categories.AnyAsync(x => x.CategoryNameEn == request.Input.CategoryNameEn, cancellationToken);

            if (categoryNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedCategoryNameEn);
            }

            var categoryNameRu = await _dbContext.Categories.AnyAsync(x => x.CategoryNameRu == request.Input.CategoryNameRu, cancellationToken);

            if (categoryNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedCategoryNameRu);
            }
            #endregion

            var newCategory = new Category
            {
                CategoryNameEn = request.Input.CategoryNameEn,
                CategoryNameRu = request.Input.CategoryNameRu,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.Categories.Add(newCategory);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}