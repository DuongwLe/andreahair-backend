﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionAuthenticatedRequest<CategorySetEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SetEnabledHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CategorySetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var category = await _dbContext.Categories
                .Where(x => x.CategoryId == request.Input.CategoryId)
                .FirstOrDefaultAsync(cancellationToken);

            if (category == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryNotFound);
            }

            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            category.IsEnabled = request.Input.Details.IsEnabled;
            category.UpdatedAtUtc = DateTime.UtcNow;
            category.UpdatedByUserId = request.UserId;
            _dbContext.Categories.Update(category);

            // Set product types
            var productTypes = await _dbContext.ProductTypes
                .Where(x => x.CategoryId == request.Input.CategoryId)
                .ToArrayAsync(cancellationToken);

            foreach (var productType in productTypes)
            {
                productType.IsEnabled = request.Input.Details.IsEnabled;
                productType.UpdatedAtUtc = DateTime.UtcNow;
                productType.UpdatedByUserId = request.UserId;
                _dbContext.ProductTypes.Update(productType);
            }

            // Set product groups
            var productGroups = await _dbContext.ProductGroups
                 .Where(x => productTypes.Select(y => y.ProductTypeId).Contains(x.ProductTypeId))
                 .ToArrayAsync(cancellationToken);

            foreach (var productGroup in productGroups)
            {
                productGroup.IsEnabled = request.Input.Details.IsEnabled;
                productGroup.UpdatedAtUtc = DateTime.UtcNow;
                productGroup.UpdatedByUserId = request.UserId;
                _dbContext.ProductGroups.Update(productGroup);
            }

            // Set products
            var products = await _dbContext.Products
                .Where(x => productGroups.Select(y => y.ProductGroupId).Contains(x.ProductGroupId))
                 .ToArrayAsync(cancellationToken);

            foreach (var product in products)
            {
                product.IsEnabled = request.Input.Details.IsEnabled;
                product.UpdatedAtUtc = DateTime.UtcNow;
                product.UpdatedByUserId = request.UserId;
                _dbContext.Products.Update(product);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}