﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class SearchHandler : IRequestHandler<ApiActionAnonymousRequest<CategorySearchInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SearchHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<CategorySearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = from c in _dbContext.Categories
                        select new
                        {
                            c.CategoryId,
                            c.CategoryNameEn,
                            c.CategoryNameRu,
                            c.IsEnabled
                        };

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Match(item.CategoryNameEn, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                         EF.Functions.Like(item.CategoryNameEn, $"%{request.Input.Keyword}%")
                    select item;
            }

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order by
            query = query.OrderBy(x => x.CategoryNameEn);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new CategoryResponseModel
                {
                    CategoryId = x.CategoryId,
                    CategoryNameEn = x.CategoryNameEn,
                    CategoryNameRu = x.CategoryNameRu,
                    IsEnabled = x.IsEnabled.Value
                }).ToList(),
                requestPaging);
        }
    }
}