﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class GetDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<CategoryGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetDetailsHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CategoryGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            var category = await (from c in _dbContext.Categories
                                  where c.CategoryId == request.Input.CategoryId
                                  select new
                                  {
                                      c.CategoryId,
                                      c.CategoryNameEn,
                                      c.CategoryNameRu
                                  }).FirstOrDefaultAsync(cancellationToken);

            if (category == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryNotFound);
            }

            return ApiResponse.CreateModel(new CategoryResponseModel
            {
                CategoryId = category.CategoryId,
                CategoryNameEn = category.CategoryNameEn,
                CategoryNameRu = category.CategoryNameRu
            });
        }
    }
}
