﻿using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.CategoryActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<CategoryDeleteInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public DeleteHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<CategoryDeleteInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var categoryId = await _dbContext.Categories
                .AnyAsync(x => x.CategoryId == request.Input.CategoryId, cancellationToken);

            if (!categoryId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryNotFound);
            }

            var checkCategoryInProductType = await _dbContext.ProductTypes
                .AnyAsync(x => x.CategoryId == request.Input.CategoryId, cancellationToken);

            if (checkCategoryInProductType)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryIsInUse);
            }
            #endregion

            // Delete category
            await _dbContext.Categories
                .Where(x => x.CategoryId == request.Input.CategoryId)
                .UpdateFromQueryAsync(pt => new Category
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}