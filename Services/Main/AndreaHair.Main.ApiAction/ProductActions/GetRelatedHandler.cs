﻿using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class GetRelatedHandler : IRequestHandler<ApiActionAnonymousRequest<ProductGetRelatedInputModel>, ApiModel.IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetRelatedHandler(
            IDbContextHelper contextHelper,
            IS3Service s3Service,
            IOptions<S3Settings> s3Settings)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ProductGetRelatedInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();

            var product = await (from p in _dbContext.Products
                                 where p.ProductId == request.Input.ProductId
                                 select new
                                 {
                                     p.ProductGroupId,
                                     p.ProductGroup.ProductTypeId
                                 }).FirstOrDefaultAsync(cancellationToken);

            if (product == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
            }

            var query = from p in _dbContext.Products
                        where
                        p.IsEnabled.Value &&
                        p.ProductGroup.IsEnabled.Value &&
                        p.ProductGroupId != product.ProductGroupId &&
                        p.ProductGroup.ProductTypeId == product.ProductTypeId
                        select new
                        {
                            p.ProductId,
                            p.ProductNameEn,
                            p.ProductNameRu,
                            p.ProductType.ProductTypeNameEn,
                            p.ProductType.ProductTypeNameRu,
                            p.IsBestSelling,
                            MainImageKey = p.ProductImages
                                .Where(x => x.IsMainImage && x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                .Select(x => x.Image.S3FileKey).FirstOrDefault(),
                            ImageKeys = p.ProductImages
                                .Where(x => !x.IsMainImage && x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                .Select(x => x.Image.S3FileKey).ToArray()
                        };

            // Total
            var totalItems = await query.CountAsync(cancellationToken);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
               result.Select(x => new ProductAllEnabledResponseModel
               {
                   ProductId = x.ProductId,
                   ProductName = StringHelper.SwitchLanguage(localization, x.ProductNameEn, x.ProductNameRu),
                   ProductTypeName = StringHelper.SwitchLanguage(localization, x.ProductTypeNameEn, x.ProductTypeNameRu),
                   IsBestSelling = x.IsBestSelling,
                   MainImageUrl = x.MainImageKey != null ? _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x.MainImageKey) : null,
                   ImageUrls = x.ImageKeys != null ? x.ImageKeys.Select(x => _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x)).ToArray() : null
               }), requestPaging);
        }
    }
}