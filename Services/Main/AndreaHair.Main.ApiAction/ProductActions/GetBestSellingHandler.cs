﻿using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class GetBestSellingHandler : IRequestHandler<ApiActionAnonymousRequest<ProductGetBestSellingInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetBestSellingHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ProductGetBestSellingInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();

            var productBestSellings = from p in _dbContext.Products
                                      where p.IsEnabled.Value && p.IsBestSelling == true
                                      select new
                                      {
                                          p.ProductId,
                                          p.ProductNameEn,
                                          p.ProductNameRu,
                                          ProductTypeEn = p.ProductType.ProductTypeNameEn,
                                          ProductTypeRu = p.ProductType.ProductTypeNameRu,
                                          p.IsBestSelling,
                                          MainImageKey = p.ProductImages
                                            .Where(x => x.IsMainImage && x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                            .Select(x => x.Image.S3FileKey).FirstOrDefault(),
                                          ImageKeys = p.ProductImages
                                            .Where(x => !x.IsMainImage && x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                            .Select(x => x.Image.S3FileKey).ToArray()
                                      };

            // Total items
            var totalItems = await productBestSellings.CountAsync(cancellationToken);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await productBestSellings
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new ProductBestSellingResponseModel
                {
                    ProductId = x.ProductId,
                    ProductName = StringHelper.SwitchLanguage(localization, x.ProductNameEn, x.ProductNameRu),
                    ProductTypeName = StringHelper.SwitchLanguage(localization, x.ProductTypeEn, x.ProductTypeRu),
                    IsBestSelling = x.IsBestSelling,
                    MainImageUrl = x.MainImageKey != null ? _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x.MainImageKey) : null,
                    ImageUrls = x.ImageKeys != null ? x.ImageKeys.Select(x => _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x)).ToArray() : null
                }).ToList(),
                requestPaging);
        }
    }
}