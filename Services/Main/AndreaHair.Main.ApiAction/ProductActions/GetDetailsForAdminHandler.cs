﻿using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class GetDetailsForAdminHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductGetDetailsForAdminInputModel>, IApiResponse>
    {
        public readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetDetailsForAdminHandler(IDbContextHelper dbContextHelper, IS3Service s3Service, IOptions<S3Settings> s3Settings)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductGetDetailsForAdminInputModel> request, CancellationToken cancellationToken)
        {
            var product = await (from p in _dbContext.Products
                                 where p.ProductGroupId == request.Input.ProductGroupId
                                 select new
                                 {
                                     p.ProductGroupId,
                                     p.ProductGroup.ProductGroupNameEn,
                                     p.ProductGroup.ProductGroupNameRu,
                                     p.ProductType.CategoryId,
                                     p.VideoUrl,
                                     p.ProductTypeId,
                                     p.MaterialTypeId,
                                     p.HairStyleId,
                                     p.MeasureUnitLengthId,
                                     p.FromLength,
                                     p.ToLength,
                                     p.MeasureUnitWeightId,
                                     p.Weight,
                                     p.Origin,
                                     p.PackingRuleId,
                                     p.DescriptionEn,
                                     p.DescriptionRu,
                                 }).FirstOrDefaultAsync(cancellationToken);

            if (product == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
            }

            var productColors = await (from p in _dbContext.Products
                                       where p.ProductGroupId == request.Input.ProductGroupId
                                       select new ProductColorResponseModel
                                       {
                                           ProductId = p.ProductId,
                                           IsBestSelling = p.IsBestSelling,
                                           IsEnabled = p.IsEnabled.Value,
                                           ColorId = p.ColorId,
                                           ColorNameEn = p.Color.ColorNameEn,
                                           ColorNameRu = p.Color.ColorNameRu,
                                           ColorCodes = p.Color.ColorMappings.Select(x => x.ColorCode).ToArray(),
                                           Images = p.ProductImages
                                                .Where(x => x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                                .Select(x => new ImageResponseModel
                                                {
                                                    ImageId = x.ImageId,
                                                    ImageFileUrl = _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x.Image.S3FileKey),
                                                    IsMainImage = x.IsMainImage
                                                }).OrderByDescending(p => p.IsMainImage).ToArray()
                                       }).ToArrayAsync(cancellationToken);

            return ApiResponse.CreateModel(new ProductDetailsForAdminResponseModel
            {
                ProductGroupId = product.ProductGroupId,
                ProductNameEn = product.ProductGroupNameEn,
                ProductNameRu = product.ProductGroupNameRu,
                CategoryId = product.CategoryId,
                ProductTypeId = product.ProductTypeId,
                VideoUrl = product.VideoUrl,
                MaterialTypeId = (MaterialTypeEnum)product.MaterialTypeId,
                HairStyleId = (HairStyleEnum)product.HairStyleId,
                MeasureUnitLengthId = (MeasureUnitLengthEnum)product.MeasureUnitLengthId,
                FromLength = product.FromLength,
                ToLength = product.ToLength,
                MeasureUnitWeightId = (MeasureUnitWeightEnum)product.MeasureUnitWeightId,
                Weight = product.Weight,
                Origin = product.Origin,
                PackingRuleId = (PackingRuleEnum)product.PackingRuleId,
                DescriptionEn = product.DescriptionEn,
                DescriptionRu = product.DescriptionRu,
                Colors = productColors
            });
        }
    }
}