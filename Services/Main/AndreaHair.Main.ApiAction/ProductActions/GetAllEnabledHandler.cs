﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class GetAllEnabledHandler : IRequestHandler<ApiActionAnonymousRequest<ProductGetAllEnabledInputModel>, ApiModel.IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetAllEnabledHandler(
            IDbContextHelper contextHelper,
            IS3Service s3Service,
            IOptions<S3Settings> s3Settings)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ProductGetAllEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();

            var query = from p in _dbContext.Products
                        where p.IsEnabled.Value &&
                        (request.Input.Details.CategoryId == null || !request.Input.Details.CategoryId.HasValue ||
                           p.ProductType.CategoryId == request.Input.Details.CategoryId) &&
                        (request.Input.Details.ProductTypeId == null || !request.Input.Details.ProductTypeId.HasValue ||
                           p.ProductTypeId == request.Input.Details.ProductTypeId) &&
                        (request.Input.Details.ColorId == null || !request.Input.Details.ColorId.HasValue ||
                           p.ColorId == request.Input.Details.ColorId) &&
                        (request.Input.Details.IsBestSelling == null || !request.Input.Details.IsBestSelling.HasValue ||
                            p.IsBestSelling == request.Input.Details.IsBestSelling)
                        select new
                        {
                            p.ProductId,
                            p.ProductNameEn,
                            p.ProductNameRu,
                            ProductTypeEn = p.ProductType.ProductTypeNameEn,
                            ProductTypeRu = p.ProductType.ProductTypeNameRu,
                            p.IsBestSelling,
                            MainImageKey = p.ProductImages
                                .Where(x => x.IsMainImage && x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                .Select(x => x.Image.S3FileKey).FirstOrDefault(),
                            ImageKeys = p.ProductImages
                                .Where(x => !x.IsMainImage && x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                                .Select(x => x.Image.S3FileKey).ToArray()
                        };

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                switch (localization)
                {
                    case "en-US":
                        query =
                         from item in query
                         where EF.Functions.Match(item.ProductNameEn, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                              EF.Functions.Like(item.ProductNameEn, $"%{request.Input.Keyword}%")
                         select item;
                        break;
                    case "ru-RU":
                        query =
                         from item in query
                         where EF.Functions.Match(item.ProductNameRu, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                              EF.Functions.Like(item.ProductNameRu, $"%{request.Input.Keyword}%")
                         select item;
                        break;

                }
            }
            // Total
            var totalItems = await query.CountAsync(cancellationToken);

            // Order
            var orderBy = "";
            if (request.Input.OrderByTypeId == OrderByTypeEnum.Asc)
            {
                switch (request.Input.OrderBy)
                {
                    case "productName":
                        switch (localization)
                        {
                            case "en-US":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenBy(c => c.ProductNameEn);
                                orderBy = request.Input.OrderBy;
                                break;
                            case "ru-RU":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenBy(c => c.ProductNameRu);
                                orderBy = request.Input.OrderBy;
                                break;
                        }
                        break;
                    default:
                        switch (localization)
                        {
                            case "en-US":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenBy(c => c.ProductNameEn);
                                orderBy = request.Input.OrderBy;
                                break;
                            case "ru-RU":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenBy(c => c.ProductNameRu);
                                orderBy = request.Input.OrderBy;
                                break;
                        }
                        break;
                }
            }
            else if (request.Input.OrderByTypeId == OrderByTypeEnum.Desc)
            {
                switch (request.Input.OrderBy)
                {
                    case "productName":
                        switch (localization)
                        {
                            case "en-US":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenByDescending(c => c.ProductNameEn);
                                orderBy = request.Input.OrderBy;
                                break;
                            case "ru-RU":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenByDescending(c => c.ProductNameRu);
                                orderBy = request.Input.OrderBy;
                                break;
                        }
                        break;
                    default:
                        switch (localization)
                        {
                            case "en-US":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenByDescending(c => c.ProductNameEn);
                                orderBy = request.Input.OrderBy;
                                break;
                            case "ru-RU":
                                query = query.OrderByDescending(x => x.IsBestSelling).ThenByDescending(c => c.ProductNameRu);
                                orderBy = request.Input.OrderBy;
                                break;
                        }
                        break;
                }
            }

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems, orderBy, (int)request.Input.OrderByTypeId,
                new List<string>() { "productName" });

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
               result.Select(x => new ProductAllEnabledResponseModel
               {
                   ProductId = x.ProductId,
                   ProductName = StringHelper.SwitchLanguage(localization, x.ProductNameEn, x.ProductNameRu),
                   ProductTypeName = StringHelper.SwitchLanguage(localization, x.ProductTypeEn, x.ProductTypeRu),
                   IsBestSelling = x.IsBestSelling,
                   MainImageUrl = x.MainImageKey != null ? _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x.MainImageKey) : null,
                   ImageUrls = x.ImageKeys != null ? x.ImageKeys.Select(x => _s3Service.GetTempPublicUrl(_s3Settings.BucketName, x)).ToArray() : null
               }), requestPaging);
        }
    }
}