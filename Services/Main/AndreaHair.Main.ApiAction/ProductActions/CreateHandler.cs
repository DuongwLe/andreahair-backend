using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }
        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productNameEn = await _dbContext.ProductGroups
                .Where(x => x.ProductTypeId == request.Input.ProductTypeId &&
                    x.ProductGroupNameEn == request.Input.ProductGroupNameEn)
                .AnyAsync(cancellationToken);

            if (productNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductNameEn);
            }

            var productNameRu = await _dbContext.ProductGroups
                .Where(x => x.ProductTypeId == request.Input.ProductTypeId &&
                    x.ProductGroupNameRu == request.Input.ProductGroupNameRu)
                .AnyAsync(cancellationToken);

            if (productNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductNameRu);
            }

            var colors = await _dbContext.Colors
                .Where(x => request.Input.Colors.Select(y => y.ColorId).Contains(x.ColorId))
                .ToListAsync(cancellationToken);

            if (colors.Count != request.Input.Colors.Length)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorNotFound);
            }

            foreach (var item in request.Input.Colors)
            {
                var physicalFileIds = await _dbContext.PhysicalFiles
                    .Where(x => item.Images.Select(y => y.ImageId).ToArray().Contains(x.PhysicalFileId) &&
                        x.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                    .CountAsync(cancellationToken);

                if (physicalFileIds != item.Images.Length)
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ImageNotFound);
                }
            }
            #endregion

            var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            var newProductGroup = new ProductGroup
            {
                ProductGroupNameEn = request.Input.ProductGroupNameEn,
                ProductGroupNameRu = request.Input.ProductGroupNameRu,
                ProductTypeId = request.Input.ProductTypeId,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.ProductGroups.Add(newProductGroup);
            await _dbContext.SaveChangesAsync(cancellationToken);

            var productImages = new List<ProductImage>();

            foreach (var item in request.Input.Colors)
            {
                var colorNameEn = colors.Where(x => x.ColorId == item.ColorId).Select(x => x.ColorNameEn).FirstOrDefault();
                var colorNameRu = colors.Where(x => x.ColorId == item.ColorId).Select(x => x.ColorNameRu).FirstOrDefault();

                var newProduct = new Product
                {
                    ProductNameEn = $"{request.Input.ProductGroupNameEn} - {colorNameEn}",
                    ProductNameRu = $"{request.Input.ProductGroupNameRu} - {colorNameRu}",
                    ProductGroupId = newProductGroup.ProductGroupId,
                    ColorId = item.ColorId,
                    ProductTypeId = request.Input.ProductTypeId,
                    MaterialTypeId = (short)request.Input.MaterialTypeId,
                    HairStyleId = (short)request.Input.HairStyleId,
                    MeasureUnitLengthId = (short)request.Input.MeasureUnitLengthId,
                    FromLength = request.Input.FromLength,
                    ToLength = request.Input.ToLength,
                    MeasureUnitWeightId = (short)request.Input.MeasureUnitWeightId,
                    Weight = request.Input.Weight,
                    Origin = request.Input.Origin,
                    PackingRuleId = (short)request.Input.PackingRuleId,
                    VideoUrl = request.Input.VideoUrl,
                    DescriptionEn = request.Input.DescriptionEn,
                    DescriptionRu = request.Input.DescriptionRu,
                    IsBestSelling = item.IsBestSelling,
                    IsEnabled = item.IsEnabled,
                };
                _dbContext.Products.Add(newProduct);
                await _dbContext.SaveChangesAsync(cancellationToken);

                foreach (var image in item.Images)
                {
                    if (!productImages.Select(x => x.ImageId).Contains(image.ImageId))
                    {
                        productImages.Add(new ProductImage
                        {
                            ProductId = newProduct.ProductId,
                            ImageId = image.ImageId,
                            IsMainImage = image.IsMainImage,
                        });
                    }
                }
            }

            if (productImages.Count > 0)
            {
                _dbContext.ProductImages.AddRange(productImages);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}