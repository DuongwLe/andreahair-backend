﻿using AndreaHair.Commons;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class GetDetailsHander : IRequestHandler<ApiActionAnonymousRequest<ProductGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public GetDetailsHander(
            IDbContextHelper contextHelper,
            IS3Service s3Service,
            IOptions<S3Settings> s3Settings)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ProductGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();

            var product = await _dbContext.Products
                .Where(x => x.ProductId == request.Input.ProductId && x.IsEnabled.Value)
                .Select(x => new
                {
                    ProductId = x.ProductId,
                    ProductName = StringHelper.SwitchLanguage(localization, x.ProductNameEn, x.ProductNameRu),
                    ColorName = StringHelper.SwitchLanguage(localization, x.Color.ColorNameEn, x.Color.ColorNameRu),
                    ColorCode = x.Color.ColorMappings.Select(c => c.ColorCode).ToArray(),
                    ReferenceProducts = _dbContext.Products
                        .Where(y => y.ProductGroup == x.ProductGroup &&
                            y.ProductId != request.Input.ProductId)
                        .Select(y => new ReferenceProductModel
                        {
                            ProductId = y.ProductId,
                            ColorCode = y.Color.ColorMappings.Select(c => c.ColorCode).ToArray()
                        }).ToArray(),
                    ImageUrls = x.ProductImages
                        .Where(x => x.Image.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                        .Select(p => new
                        {
                            ImageUrl = _s3Service.GetTempPublicUrl(_s3Settings.BucketName, p.Image.S3FileKey),
                            p.IsMainImage
                        }).OrderByDescending(p => p.IsMainImage).ToArray(),
                    CategoryId = x.ProductType.CategoryId,
                    CategoryName = StringHelper.SwitchLanguage(localization, x.ProductType.Category.CategoryNameEn, x.ProductType.Category.CategoryNameRu),
                    ProductTypeId = x.ProductTypeId,
                    ProductTypeName = StringHelper.SwitchLanguage(localization, x.ProductType.ProductTypeNameEn, x.ProductType.ProductTypeNameRu),
                    MaterialTypeId = (MaterialTypeEnum)x.MaterialTypeId,
                    HairStyleId = (HairStyleEnum)x.HairStyleId,
                    MeasureUnitLengthId = (MeasureUnitLengthEnum)x.MeasureUnitLengthId,
                    FromLength = x.FromLength,
                    ToLength = x.ToLength,
                    MeasureUnitWeightId = (MeasureUnitWeightEnum)x.MeasureUnitWeightId,
                    Weight = x.Weight,
                    Origin = x.Origin,
                    PackingRuleId = (PackingRuleEnum)x.PackingRuleId,
                    VideoUrl = x.VideoUrl,
                    Description = StringHelper.SwitchLanguage(localization, x.DescriptionEn, x.DescriptionRu),
                    IsBestSelling = x.IsBestSelling
                })
                .FirstOrDefaultAsync(cancellationToken);

            if (product == null)
            {
                return ApiResponse.CreateErrorModel(System.Net.HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
            }

            return ApiResponse.CreateModel(new ProductDetailsResponseModel
            {
                ProductId = product.ProductId,
                ProductName = product.ProductName,
                ColorName = product.ColorName,
                ColorCode = product.ColorCode,
                ReferenceProducts = product.ReferenceProducts,
                ImageUrls = product.ImageUrls.Select(x => x.ImageUrl).ToArray(),
                CategoryId = product.CategoryId,
                CategoryName = product.CategoryName,
                ProductTypeId = product.ProductTypeId,
                ProductTypeName = product.ProductTypeName,
                MaterialTypeId = product.MaterialTypeId,
                HairStyleId = product.HairStyleId,
                MeasureUnitLengthId = product.MeasureUnitLengthId,
                FromLength = product.FromLength,
                ToLength = product.ToLength,
                MeasureUnitWeightId = product.MeasureUnitWeightId,
                Weight = product.Weight,
                Origin = product.Origin,
                PackingRuleId = product.PackingRuleId,
                VideoUrl = product.VideoUrl,
                Description = product.Description,
                IsBestSelling = product.IsBestSelling
            });
        }
    }
}

