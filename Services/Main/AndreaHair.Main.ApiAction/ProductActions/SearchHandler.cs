﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class SearchHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductSearchInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SearchHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = from p in _dbContext.ProductGroups
                        select new
                        {
                            p.ProductGroupId,
                            p.ProductGroupNameEn,
                            p.ProductGroupNameRu,
                            p.ProductType.ProductTypeNameEn,
                            p.IsEnabled,
                            NumberOfColor = p.Products.Count
                        };

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Match(item.ProductGroupNameEn, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                         EF.Functions.Like(item.ProductGroupNameEn, $"%{request.Input.Keyword}%")
                    select item;
            }

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order by
            query = query.OrderBy(x => x.ProductGroupNameEn);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new ProductResponseModel
                {
                    ProductGroupId = x.ProductGroupId,
                    ProductNameEn = x.ProductGroupNameEn,
                    ProductNameRu = x.ProductGroupNameRu,
                    ProductTypeName = x.ProductTypeNameEn,
                    NumberOfColor = x.NumberOfColor,
                    IsEnabled = x.IsEnabled.Value
                }).ToList(),
                requestPaging);
        }
    }
}
