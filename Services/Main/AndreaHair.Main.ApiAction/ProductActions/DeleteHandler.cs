﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductDeleteInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public DeleteHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductDeleteInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productGroupId = await _dbContext.ProductGroups
                .AnyAsync(x => x.ProductGroupId == request.Input.ProductGroupId, cancellationToken);

            if (!productGroupId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
            }
            #endregion

            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            // Set is deleted product groups
            await _dbContext.ProductGroups
                .Where(x => x.ProductGroupId == request.Input.ProductGroupId)
                .UpdateFromQueryAsync(x => new ProductGroup
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);


            var products = await _dbContext.Products
                .Where(x => x.ProductGroupId == request.Input.ProductGroupId)
                .Select(x => new
                {
                    x.ProductId,
                    ImageIds = x.ProductImages.Select(y => y.ImageId).ToArray()
                })
                .ToListAsync(cancellationToken);

            // Set is deleted products
            await _dbContext.Products
                .Where(x => products.Select(p => p.ProductId).Contains(x.ProductId))
                .UpdateFromQueryAsync(x => new Product
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                });

            // Set will be deleted physical files
            foreach (var product in products)
            {
                await _dbContext.PhysicalFiles
                       .Where(x => product.ImageIds.Contains(x.PhysicalFileId))
                       .UpdateFromQueryAsync(p => new PhysicalFile
                       {
                           PhysicalFileStatusId = (short)PhysicalFileStatusEnum.WillBeDeleted,
                           UpdatedAtUtc = DateTime.UtcNow,
                           UpdatedByUserId = request.UserId
                       }, cancellationToken);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}