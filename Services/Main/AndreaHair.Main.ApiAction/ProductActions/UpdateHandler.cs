﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductUpdateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public UpdateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductUpdateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productGroup = await _dbContext.ProductGroups
                .Where(x => x.ProductGroupId == request.Input.ProductGroupId)
                .FirstOrDefaultAsync(cancellationToken);

            if (productGroup == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
            }

            var productGroupNameEn = await _dbContext.ProductGroups
                .AnyAsync(x => x.ProductGroupId != request.Input.ProductGroupId &&
                    x.ProductGroupNameEn == request.Input.Details.ProductGroupNameEn,
                    cancellationToken);

            if (productGroupNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductNameEn);
            }

            var productGroupNameRu = await _dbContext.ProductGroups
                .AnyAsync(x => x.ProductGroupId != request.Input.ProductGroupId &&
                    x.ProductGroupNameRu == request.Input.Details.ProductGroupNameRu,
                    cancellationToken);

            if (productGroupNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductNameRu);
            }

            var colors = await _dbContext.Colors
                .Where(x => request.Input.Details.Colors.Select(y => y.ColorId).Contains(x.ColorId))
                .ToArrayAsync(cancellationToken);

            if (colors.Length != request.Input.Details.Colors.Length)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorNotFound);
            }

            foreach (var item in request.Input.Details.Colors)
            {
                var physicalFileIds = await _dbContext.PhysicalFiles
                    .Where(x => item.Images.Select(y => y.ImageId).ToArray().Contains(x.PhysicalFileId) &&
                        x.PhysicalFileStatusId == (short)PhysicalFileStatusEnum.OK)
                    .CountAsync(cancellationToken);

                if (physicalFileIds != item.Images.Length)
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ImageNotFound);
                }
            }
            #endregion
            var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            // Update product groups
            productGroup.ProductGroupNameEn = request.Input.Details.ProductGroupNameEn;
            productGroup.ProductGroupNameRu = request.Input.Details.ProductGroupNameRu;
            productGroup.ProductTypeId = request.Input.Details.ProductTypeId;
            productGroup.UpdatedAtUtc = DateTime.UtcNow;
            productGroup.UpdatedByUserId = request.UserId;
            _dbContext.ProductGroups.Update(productGroup);

            // HARD CODE: Update products
            // Get current product
            var currentProducts = await _dbContext.Products
                .Where(x => x.ProductGroupId == request.Input.ProductGroupId)
                .Select(x => new
                {
                    x.ProductId,
                    ImageIds = x.ProductImages.Select(y => y.ImageId).ToArray()
                })
                .ToArrayAsync(cancellationToken);

            var newImages = new List<ProductImage>();
            var deletePhysicalFileIds = new List<long>();

            // Delete products which not in input product ids
            foreach (var item in currentProducts)
            {
                if (!request.Input.Details.Colors.Where(x => x.ProductId.HasValue).Select(x => x.ProductId.Value).Contains(item.ProductId))
                {
                    // Set is deleted product
                    await _dbContext.Products
                        .Where(x => x.ProductId == item.ProductId)
                        .UpdateFromQueryAsync(x => new Product
                        {
                            IsDeleted = true,
                            UpdatedAtUtc = DateTime.UtcNow,
                            UpdatedByUserId = request.UserId
                        }, cancellationToken);

                    // Set all images belong to delete products
                    deletePhysicalFileIds.AddRange(item.ImageIds);
                }
            }

            foreach (var item in request.Input.Details.Colors)
            {
                var colorNameEn = colors.Where(x => x.ColorId == item.ColorId).Select(x => x.ColorNameEn).FirstOrDefault();
                var colorNameRu = colors.Where(x => x.ColorId == item.ColorId).Select(x => x.ColorNameRu).FirstOrDefault();

                // Update
                if (item.ProductId.HasValue)
                {
                    await _dbContext.Products
                        .Where(x => x.ProductId == item.ProductId.Value)
                        .UpdateFromQueryAsync(x => new Product
                        {
                            ProductNameEn = $"{request.Input.Details.ProductGroupNameEn} - {colorNameEn}",
                            ProductNameRu = $"{request.Input.Details.ProductGroupNameRu} - {colorNameRu}",
                            ColorId = item.ColorId,
                            ProductTypeId = request.Input.Details.ProductTypeId,
                            MaterialTypeId = (short)request.Input.Details.MaterialTypeId,
                            HairStyleId = (short)request.Input.Details.HairStyleId,
                            MeasureUnitLengthId = (short)request.Input.Details.MeasureUnitLengthId,
                            FromLength = request.Input.Details.FromLength,
                            ToLength = request.Input.Details.ToLength,
                            MeasureUnitWeightId = (short)request.Input.Details.MeasureUnitWeightId,
                            Weight = request.Input.Details.Weight,
                            Origin = request.Input.Details.Origin,
                            PackingRuleId = (short)request.Input.Details.PackingRuleId,
                            VideoUrl = request.Input.Details.VideoUrl,
                            DescriptionEn = request.Input.Details.DescriptionEn,
                            DescriptionRu = request.Input.Details.DescriptionRu,
                            IsBestSelling = item.IsBestSelling,
                            IsEnabled = productGroup.IsEnabled.Value ? item.IsEnabled : false,
                        }, cancellationToken);

                    // Update physical files
                    var currentImageIds = currentProducts
                        .Where(x => x.ProductId == item.ProductId)
                        .Select(x => x.ImageIds)
                        .FirstOrDefault();

                    // Add image id to delete image ids
                    foreach (var imageId in currentImageIds)
                    {
                        if (!item.Images.Select(x => x.ImageId).ToArray().Contains(imageId))
                        {
                            deletePhysicalFileIds.Add(imageId);
                        }
                    }

                    // Insert new image
                    foreach (var image in item.Images)
                    {
                        if (!currentImageIds.Contains(image.ImageId) && !newImages.Select(x => x.ImageId).Contains(image.ImageId))
                        {
                            newImages.Add(new ProductImage
                            {
                                ProductId = item.ProductId.Value,
                                ImageId = image.ImageId,
                                IsMainImage = image.IsMainImage,
                            });
                        }
                    }
                }

                // Create new
                else
                {
                    var newProduct = new Product
                    {
                        ProductNameEn = $"{request.Input.Details.ProductGroupNameEn} - {colorNameEn}",
                        ProductNameRu = $"{request.Input.Details.ProductGroupNameRu} - {colorNameRu}",
                        ProductGroupId = request.Input.ProductGroupId,
                        ColorId = item.ColorId,
                        ProductTypeId = request.Input.Details.ProductTypeId,
                        MaterialTypeId = (short)request.Input.Details.MaterialTypeId,
                        HairStyleId = (short)request.Input.Details.HairStyleId,
                        MeasureUnitLengthId = (short)request.Input.Details.MeasureUnitLengthId,
                        FromLength = request.Input.Details.FromLength,
                        ToLength = request.Input.Details.ToLength,
                        MeasureUnitWeightId = (short)request.Input.Details.MeasureUnitWeightId,
                        Weight = request.Input.Details.Weight,
                        Origin = request.Input.Details.Origin,
                        PackingRuleId = (short)request.Input.Details.PackingRuleId,
                        VideoUrl = request.Input.Details.VideoUrl,
                        DescriptionEn = request.Input.Details.DescriptionEn,
                        DescriptionRu = request.Input.Details.DescriptionRu,
                        IsBestSelling = item.IsBestSelling,
                        IsEnabled = productGroup.IsEnabled.Value ? item.IsEnabled : false,
                    };
                    _dbContext.Products.Add(newProduct);
                    await _dbContext.SaveChangesAsync(cancellationToken);

                    // Insert new images
                    foreach (var image in item.Images)
                    {
                        if (!newImages.Select(x => x.ImageId).Contains(image.ImageId))
                        {
                            newImages.Add(new ProductImage
                            {
                                ProductId = newProduct.ProductId,
                                ImageId = image.ImageId,
                                IsMainImage = image.IsMainImage,
                            });
                        }
                    }
                }
            }

            if (deletePhysicalFileIds.Count > 0)
            {
                await _dbContext.ProductImages
                    .Where(x => deletePhysicalFileIds.Contains(x.ImageId))
                    .DeleteFromQueryAsync(cancellationToken);

                await _dbContext.PhysicalFiles
                    .Where(x => deletePhysicalFileIds.Contains(x.PhysicalFileId))
                    .UpdateFromQueryAsync(x => new PhysicalFile
                    {
                        PhysicalFileStatusId = (short)PhysicalFileStatusEnum.WillBeDeleted,
                        UpdatedAtUtc = DateTime.UtcNow,
                        UpdatedByUserId = request.UserId,
                    }, cancellationToken);
            }

            if (newImages.Count > 0)
            {
                _dbContext.ProductImages.AddRange(newImages);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}