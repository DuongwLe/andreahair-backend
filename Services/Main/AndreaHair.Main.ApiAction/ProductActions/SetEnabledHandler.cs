﻿using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ProductActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductSetEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SetEnabledHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductSetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            #region  Validate input
            var productGroup = await _dbContext.ProductGroups
                .Where(p => p.ProductGroupId == request.Input.ProductGroupId)
                .FirstOrDefaultAsync(cancellationToken);

            if (productGroup == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductNotFound);
            }

            var productTypeIsEnabled = await (from p in _dbContext.ProductTypes
                                              where p.ProductTypeId == productGroup.ProductTypeId
                                              select p.IsEnabled).FirstOrDefaultAsync(cancellationToken);

            if (!productTypeIsEnabled.Value)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductTypeHasBeenDisabled);
            }
            #endregion
            var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            productGroup.IsEnabled = request.Input.Details.IsEnabled;
            productGroup.UpdatedAtUtc = DateTime.UtcNow;
            productGroup.UpdatedByUserId = request.UserId;
            _dbContext.ProductGroups.Update(productGroup);

            // Set products
            await _dbContext.Products
                .Where(x => x.ProductGroupId == request.Input.ProductGroupId)
                .UpdateFromQueryAsync(x => new Product
                {
                    IsEnabled = request.Input.Details.IsEnabled,
                    UpdatedAtUtc = DateTime.UtcNow,
                    UpdatedByUserId = request.UserId,
                }, cancellationToken);

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}