﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons;
using AndreaHair.Commons.Constants;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.PhysicalFile;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.Extensions.Options;

namespace AndreaHair.Main.ApiAction.PhysicalFileActions
{
    public class RequestUploadHandler : IRequestHandler<ApiActionAuthenticatedRequest<PhysicalFileRequestUploadInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;
        private readonly S3Settings _s3Settings;

        public RequestUploadHandler(
            IDbContextHelper contextHelper,
            IS3Service s3Service,
            IOptions<S3Settings> s3Settings)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
            _s3Settings = s3Settings.Value;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<PhysicalFileRequestUploadInputModel> request, CancellationToken cancellationToken)
        {
            var ext = request.Input.FileName.Split('.').Last().ToLower();
            var fileKey = FileConstants.GetFileKey(request.Input.PhysicalFileTypeId.ToString(), ext);

            // Check ext
            if (!FileConstants.AllowFileExtensions.Contains(ext))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidFileExtension);
            }

            var physicalFile = new PhysicalFile
            {
                PhysicalFileTypeId = (short)request.Input.PhysicalFileTypeId,
                PhysicalFileStatusId = (short)PhysicalFileStatusEnum.New,
                FileLengthInBytes = request.Input.FileLength,
                S3BucketId = _s3Settings.BucketId,
                S3FileKey = fileKey,
                PhysicalFileName = request.Input.FileName,
                PhysicalFileExtension = ext,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.PhysicalFiles.Add(physicalFile);
            await _dbContext.SaveChangesAsync(cancellationToken);

            var presignedUrl = _s3Service.GetPresignedUploadUrl(_s3Settings.BucketName, fileKey);

            return ApiResponse.CreateModel(new PhysicalFileRequestUploadResponseModel
            {
                PhysicalFileId = physicalFile.PhysicalFileId,
                PhysicalFileName = physicalFile.PhysicalFileName,
                PresignedUploadUrl = presignedUrl
            });
        }
    }
}

