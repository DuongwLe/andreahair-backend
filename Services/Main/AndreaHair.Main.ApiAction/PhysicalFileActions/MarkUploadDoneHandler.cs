﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Services;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.PhysicalFile;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.PhysicalFileActions
{
    public class MarkUploadDoneHandler : IRequestHandler<ApiActionAuthenticatedRequest<PhysicalFileMarkUploadDoneInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;
        private readonly IS3Service _s3Service;

        public MarkUploadDoneHandler(IDbContextHelper contextHelper, IS3Service s3Service)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
            _s3Service = s3Service;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<PhysicalFileMarkUploadDoneInputModel> request, CancellationToken cancellationToken)
        {
            var files = await _dbContext.PhysicalFiles
                .Where(pf => request.Input.PhysicalFileIds.Contains(pf.PhysicalFileId))
                .Select(pf => new { pf, pf.S3Bucket })
                .ToArrayAsync(cancellationToken);

            if (files.Length != request.Input.PhysicalFileIds.Length)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.PhysicalFileNotFound);
            }

            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);
            // Check file length
            foreach (var file in files)
            {
                var fileStat = await _s3Service.GetStat(file.S3Bucket.S3BucketName, file.pf.S3FileKey, cancellationToken);
                if (fileStat == null || fileStat.ContentLength != file.pf.FileLengthInBytes)
                {
                    file.pf.PhysicalFileStatusId = (short)PhysicalFileStatusEnum.Corrupted;
                    _dbContext.PhysicalFiles.Update(file.pf);
                    await _dbContext.SaveChangesAsync(cancellationToken);

                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.FileCorrupted);
                }

                file.pf.PhysicalFileStatusId = (short)PhysicalFileStatusEnum.OK;
                _dbContext.PhysicalFiles.Update(file.pf);
            }
            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}

