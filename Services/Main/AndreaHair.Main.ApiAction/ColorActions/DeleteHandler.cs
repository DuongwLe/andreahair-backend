﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<ColorDeleteInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public DeleteHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ColorDeleteInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var colorId = await _dbContext.Colors
                .AnyAsync(x => x.ColorId == request.Input.ColorId, cancellationToken);

            if (!colorId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorNotFound);
            }

            // Check color in Products
            var checkColor = await _dbContext.Products
                .AnyAsync(x => x.ColorId == request.Input.ColorId, cancellationToken);

            if (checkColor)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorIsInUse);
            }
            #endregion

            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            // Delete color
            await _dbContext.Colors
                .Where(x => x.ColorId == request.Input.ColorId)
                .UpdateFromQueryAsync(pt => new Color
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            // Delete color_mapping
            await _dbContext.ColorMappings
                .Where(x => x.ColorId == request.Input.ColorId)
                .DeleteFromQueryAsync(cancellationToken);

            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}