﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class GetDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<ColorGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetDetailsHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ColorGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            var color = await (from c in _dbContext.Colors
                               where c.ColorId == request.Input.ColorId
                               select new
                               {
                                   c.ColorId,
                                   c.ColorNameEn,
                                   c.ColorNameRu
                               }).FirstOrDefaultAsync(cancellationToken);

            if (color == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorNotFound);
            }

            var colorCodes = await (from c in _dbContext.ColorMappings
                                    where c.ColorId == request.Input.ColorId
                                    select c.ColorCode).ToArrayAsync(cancellationToken);

            return ApiResponse.CreateModel(new ColorResponseModel
            {
                ColorId = color.ColorId,
                ColorNameEn = color.ColorNameEn,
                ColorNameRu = color.ColorNameRu,
                ColorCodes = colorCodes.ToArray()
            });
        }
    }
}