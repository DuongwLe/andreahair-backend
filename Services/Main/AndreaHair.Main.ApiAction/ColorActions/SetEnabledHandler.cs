﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionAuthenticatedRequest<ColorSetEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SetEnabledHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ColorSetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var color = await (from p in _dbContext.Colors
                               where p.ColorId == request.Input.ColorId
                               select p).FirstOrDefaultAsync(cancellationToken);

            if (color == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorNotFound);
            }

            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            color.IsEnabled = request.Input.Details.IsEnabled;
            color.UpdatedAtUtc = DateTime.UtcNow;
            color.UpdatedByUserId = request.UserId;
            _dbContext.Colors.Update(color);

            // Set products
            await _dbContext.Products
                .Where(x => x.ColorId == request.Input.ColorId)
                .UpdateFromQueryAsync(x => new Product
                {
                    IsEnabled = request.Input.Details.IsEnabled,
                    UpdatedAtUtc = DateTime.UtcNow,
                    UpdatedByUserId = request.UserId
                }, cancellationToken);

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}