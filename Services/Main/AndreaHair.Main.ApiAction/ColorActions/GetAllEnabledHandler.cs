﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class GetAllEnabledHandler : IRequestHandler<ApiActionAnonymousRequest<ColorGetAllEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetAllEnabledHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ColorGetAllEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();

            var colors = await _dbContext.Colors
                .Where(x => x.IsEnabled.Value)
                .Select(x => new ColorAllEnabledResponseModel
                {
                    ColorId = x.ColorId,
                    ColorName = StringHelper.SwitchLanguage(localization, x.ColorNameEn, x.ColorNameRu),
                    ColorCode = x.ColorMappings.Select(x => x.ColorCode).ToArray()
                }).ToArrayAsync(cancellationToken);

            return ApiResponse.CreateModel(colors);
        }
    }
}

