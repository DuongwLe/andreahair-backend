﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<ColorUpdateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public UpdateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ColorUpdateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var color = await (from c in _dbContext.Colors
                               where c.ColorId == request.Input.ColorId
                               select c).FirstOrDefaultAsync(cancellationToken);

            if (color == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ColorNotFound);
            }

            var colorNameEn = await (from c in _dbContext.Colors
                                     where
                                     c.ColorId != request.Input.ColorId &&
                                     c.ColorNameEn == request.Input.Details.ColorNameEn
                                     select c).AnyAsync(cancellationToken);

            if (colorNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedColorNameEn);
            }

            var colorNameRu = await (from c in _dbContext.Colors
                                     where
                                     c.ColorId != request.Input.ColorId &&
                                     c.ColorNameRu == request.Input.Details.ColorNameRu
                                     select c).AnyAsync(cancellationToken);

            if (colorNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedColorNameRu);
            }
            #endregion
            var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            // Update color
            color.ColorNameEn = request.Input.Details.ColorNameEn;
            color.ColorNameRu = request.Input.Details.ColorNameRu;
            color.UpdatedAtUtc = DateTime.UtcNow;
            color.UpdatedByUserId = request.UserId;
            _dbContext.Colors.Update(color);
            await _dbContext.SaveChangesAsync(cancellationToken);

            // Update color mapping
            await _dbContext.ColorMappings
                .Where(x => x.ColorId == request.Input.ColorId)
                .DeleteFromQueryAsync(cancellationToken);

            var newColorMapping = request.Input.Details.ColorCodes
                .Select((colorCode) => new ColorMapping
                {
                    ColorId = color.ColorId,
                    ColorCode = colorCode,
                });
            await _dbContext.ColorMappings.BulkInsertAsync(newColorMapping, cancellationToken);

            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}