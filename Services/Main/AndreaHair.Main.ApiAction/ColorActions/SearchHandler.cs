﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class SearchHandler : IRequestHandler<ApiActionAuthenticatedRequest<ColorSearchInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SearchHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }
        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ColorSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = from c in _dbContext.Colors
                        select new
                        {
                            c.ColorId,
                            c.ColorNameEn,
                            c.ColorNameRu,
                            c.IsEnabled
                        };

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Match(item.ColorNameEn, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                         EF.Functions.Like(item.ColorNameEn, $"%{request.Input.Keyword}%")
                    select item;
            }

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order by
            query = query.OrderBy(x => x.ColorNameEn);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new ColorResponseModel
                {
                    ColorId = x.ColorId,
                    ColorNameEn = x.ColorNameEn,
                    ColorNameRu = x.ColorNameRu,
                    IsEnabled = x.IsEnabled.Value
                }).ToList(),
                requestPaging);
        }
    }
}