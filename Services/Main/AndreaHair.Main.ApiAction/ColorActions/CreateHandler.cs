﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ColorActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<ColorCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }
        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ColorCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var colorNameEn = await _dbContext.Colors.AnyAsync(x => x.ColorNameEn == request.Input.ColorNameEn, cancellationToken);

            if (colorNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedColorNameEn);
            }

            var colorNameRu = await _dbContext.Colors.AnyAsync(x => x.ColorNameRu == request.Input.ColorNameRu, cancellationToken);

            if (colorNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedColorNameRu);
            }
            #endregion
            var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            // Create new color
            var newColor = new Color
            {
                ColorNameEn = request.Input.ColorNameEn,
                ColorNameRu = request.Input.ColorNameRu,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.Colors.Add(newColor);
            await _dbContext.SaveChangesAsync(cancellationToken);

            // Create color_mappings
            var colorMapping = request.Input.ColorCodes
                .Select((colorCode) => new ColorMapping
                {
                    ColorId = newColor.ColorId,
                    ColorCode = colorCode,
                });
            await _dbContext.ColorMappings.BulkInsertAsync(colorMapping, cancellationToken);

            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}