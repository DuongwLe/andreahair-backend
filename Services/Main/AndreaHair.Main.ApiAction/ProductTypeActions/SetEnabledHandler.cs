﻿using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductTypeSetEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SetEnabledHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductTypeSetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productType = await (from p in _dbContext.ProductTypes
                                     where p.ProductTypeId == request.Input.ProductTypeId
                                     select p).FirstOrDefaultAsync(cancellationToken);

            if (productType == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductTypeNotFound);
            }

            var categoryIsEnabled = await (from c in _dbContext.Categories
                                           where c.CategoryId == productType.CategoryId
                                           select c.IsEnabled).FirstOrDefaultAsync(cancellationToken);

            if (!categoryIsEnabled.Value)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryHasBeenDisabled);
            }
            #endregion
            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            productType.IsEnabled = request.Input.Details.IsEnabled;
            productType.UpdatedAtUtc = DateTime.UtcNow;
            productType.UpdatedByUserId = request.UserId;
            _dbContext.ProductTypes.Update(productType);

            // Set product groups
            var productGroups = await _dbContext.ProductGroups
                 .Where(x => x.ProductTypeId == request.Input.ProductTypeId)
                 .ToArrayAsync(cancellationToken);

            foreach (var productGroup in productGroups)
            {
                productGroup.IsEnabled = request.Input.Details.IsEnabled;
                productGroup.UpdatedAtUtc = DateTime.UtcNow;
                productGroup.UpdatedByUserId = request.UserId;
                _dbContext.ProductGroups.Update(productGroup);
            }

            // Set products
            var products = await _dbContext.Products
                .Where(x => productGroups.Select(y => y.ProductGroupId).Contains(x.ProductGroupId))
                 .ToArrayAsync(cancellationToken);

            foreach (var product in products)
            {
                product.IsEnabled = request.Input.Details.IsEnabled;
                product.UpdatedAtUtc = DateTime.UtcNow;
                product.UpdatedByUserId = request.UserId;
                _dbContext.Products.Update(product);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}