﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductTypeCreateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CreateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }
        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductTypeCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var categoryId = await _dbContext.Categories
                .AnyAsync(x => x.CategoryId == request.Input.CategoryId, cancellationToken);

            if (!categoryId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryNotFound);
            }

            var productTypeNameEn = await _dbContext.ProductTypes
                .Where(x => x.ProductTypeNameEn == request.Input.ProductTypeNameEn &&
                    x.CategoryId == request.Input.CategoryId)
                .AnyAsync(cancellationToken);

            if (productTypeNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductTypeNameEn);
            }

            var productTypeNameRu = await _dbContext.ProductTypes
                .Where(x => x.ProductTypeNameRu == request.Input.ProductTypeNameRu && 
                    x.CategoryId == request.Input.CategoryId)
                .AnyAsync(cancellationToken);

            if (productTypeNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductTypeNameRu);
            }
            #endregion

            var newProductType = new ProductType
            {
                ProductTypeNameEn = request.Input.ProductTypeNameEn,
                ProductTypeNameRu = request.Input.ProductTypeNameRu,
                CategoryId = request.Input.CategoryId,
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };
            _dbContext.ProductTypes.Add(newProductType);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}