﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Extensions;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class GetAllEnabledHandler : IRequestHandler<ApiActionAnonymousRequest<ProductTypeGetAllEnabledInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetAllEnabledHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAnonymousRequest<ProductTypeGetAllEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var localization = CultureInfo.GetCultureInfo(request.Lang.ToCultureString()).ToString();
            var data = await _dbContext.ProductTypes
                .Where(x => x.IsEnabled.Value &&
                    (request.Input.CategoryId == null || !request.Input.CategoryId.HasValue ||
                        x.CategoryId == request.Input.CategoryId))
                .Select(x => new ProducTypeAllEnabledResponseModel
                {
                    ProductTypeId = x.ProductTypeId,
                    ProductTypeName = StringHelper.SwitchLanguage(localization, x.ProductTypeNameEn, x.ProductTypeNameRu)
                })
                .ToArrayAsync(cancellationToken);

            return ApiResponse.CreateModel(data);
        }
    }
}

