﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class GetDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductTypeGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetDetailsHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductTypeGetDetailsInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productType = await (from pt in _dbContext.ProductTypes
                                     where pt.ProductTypeId == request.Input.ProductTypeId
                                     select new
                                     {
                                         pt.ProductTypeId,
                                         pt.ProductTypeNameEn,
                                         pt.ProductTypeNameRu,
                                         pt.CategoryId
                                     }).FirstOrDefaultAsync(cancellationToken);

            if (productType == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductTypeNotFound);
            }
            #endregion

            return ApiResponse.CreateModel(new ProductTypeResponseModel
            {
                ProductTypeId = productType.ProductTypeId,
                ProductTypeNameEn = productType.ProductTypeNameEn,
                ProductTypeNameRu = productType.ProductTypeNameRu,
                CategoryId = productType.CategoryId
            });
        }
    }
}