﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductTypeUpdateInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public UpdateHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductTypeUpdateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productTypeId = await _dbContext.ProductTypes
                .AnyAsync(x => x.ProductTypeId == request.Input.ProductTypeId, cancellationToken);

            if (!productTypeId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductTypeNotFound);
            }

            var categoryId = await _dbContext.Categories
                .AnyAsync(x => x.CategoryId == request.Input.Details.CategoryId, cancellationToken);

            if (!categoryId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.CategoryNotFound);
            }

            var productTypeNameEn = await (from pt in _dbContext.ProductTypes
                                           where
                                           pt.ProductTypeId != request.Input.ProductTypeId &&
                                           pt.CategoryId == request.Input.Details.CategoryId &&
                                           pt.ProductTypeNameEn == request.Input.Details.ProductTypeNameEn
                                           select pt).AnyAsync(cancellationToken);

            if (productTypeNameEn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductTypeNameEn);
            }

            var productTypeNameRu = await (from pt in _dbContext.ProductTypes
                                           where
                                           pt.ProductTypeId != request.Input.ProductTypeId &&
                                           pt.CategoryId == request.Input.Details.CategoryId &&
                                           pt.ProductTypeNameRu == request.Input.Details.ProductTypeNameRu
                                           select pt).AnyAsync(cancellationToken);

            if (productTypeNameRu)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicatedProductTypeNameRu);
            }
            #endregion

            await _dbContext.ProductTypes
               .Where(x => x.ProductTypeId == request.Input.ProductTypeId)
               .UpdateFromQueryAsync(x => new ProductType
               {
                   ProductTypeNameEn = request.Input.Details.ProductTypeNameEn,
                   ProductTypeNameRu = request.Input.Details.ProductTypeNameRu,
                   CategoryId = request.Input.Details.CategoryId,
                   UpdatedAtUtc = DateTime.UtcNow,
                   UpdatedByUserId = request.UserId
               }, cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}