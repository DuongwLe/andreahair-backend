﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class SearchHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductTypeSearchInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public SearchHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductTypeSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = from pt in _dbContext.ProductTypes
                        where (!request.Input.Details.CategoryId.HasValue ||
                            pt.CategoryId == request.Input.Details.CategoryId.Value)
                        select new
                        {
                            pt.ProductTypeId,
                            pt.ProductTypeNameEn,
                            pt.ProductTypeNameRu,
                            pt.Category.CategoryNameEn,
                            pt.IsEnabled
                        };

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Match(item.ProductTypeNameEn, $"{StringHelper.BuildSearchString(request.Input.Keyword)}*", MySqlMatchSearchMode.Boolean) &&
                         EF.Functions.Like(item.ProductTypeNameEn, $"%{request.Input.Keyword}%")
                    select item;
            }

            // Total items
            var totalItems = await query.CountAsync(cancellationToken);

            // Order by
            query = query.OrderBy(x => x.ProductTypeNameEn);

            // Page info
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Page data
            var result = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                result.Select(x => new ProductTypeResponseModel
                {
                    ProductTypeId = x.ProductTypeId,
                    ProductTypeNameEn = x.ProductTypeNameEn,
                    ProductTypeNameRu = x.ProductTypeNameRu,
                    CategoryName = x.CategoryNameEn,
                    IsEnabled = x.IsEnabled.Value
                }).ToList(),
                requestPaging);
        }
    }
}