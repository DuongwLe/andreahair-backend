﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.ProductTypeActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<ProductTypeDeleteInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public DeleteHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<ProductTypeDeleteInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var productTypeId = await _dbContext.ProductTypes
               .AnyAsync(x => x.ProductTypeId == request.Input.ProductTypeId, cancellationToken);

            if (!productTypeId)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductTypeNotFound);
            }

            // Check product types exist in product groups
            var checkProductType = await _dbContext.ProductGroups
                .AnyAsync(x => x.ProductTypeId == request.Input.ProductTypeId, cancellationToken);

            if (checkProductType)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ProductTypeIsInUse);
            }
            #endregion

            // Delete product types
            await _dbContext.ProductTypes
                .Where(x => x.ProductTypeId == request.Input.ProductTypeId)
                .UpdateFromQueryAsync(pt => new ProductType
                {
                    IsDeleted = true,
                    UpdatedByUserId = request.UserId,
                    UpdatedAtUtc = DateTime.UtcNow
                }, cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}