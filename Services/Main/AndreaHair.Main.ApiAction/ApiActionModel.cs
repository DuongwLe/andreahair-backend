﻿using System;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Extensions;
using AndreaHair.Main.ApiModel;
using MediatR;

namespace AndreaHair.Main.ApiAction
{
    public class ApiActionModel
    {
        public static ApiActionAnonymousRequest<T> CreateRequest<T>(T input, string lang = "en-US")
              where T : IApiInput, new()
        {
            return new ApiActionAnonymousRequest<T>()
            {
                Input = input,
                Lang = lang.ToLanguage()
            };
        }

        public static ApiActionAuthenticatedRequest<T> CreateRequest<T>(Guid userId, T input, string lang = "en-US")
            where T : IApiInput, new()
        {
            return new ApiActionAuthenticatedRequest<T>()
            {
                UserId = userId,
                Input = input,
                Lang = lang.ToLanguage()
            };
        }
    }

    public class ApiActionAnonymousRequest<T> : IRequest<IApiResponse>
    {
        internal ApiActionAnonymousRequest()
        { }

        public T Input { get; set; }
        public LanguageEnum Lang { get; set; }
    }

    public class ApiActionAuthenticatedRequest<T> : IRequest<IApiResponse>
        where T : IApiInput, new()
    {
        internal ApiActionAuthenticatedRequest()
        { }

        public Guid UserId { get; set; }
        public T Input { get; set; }
        public LanguageEnum Lang { get; set; }
    }
}

