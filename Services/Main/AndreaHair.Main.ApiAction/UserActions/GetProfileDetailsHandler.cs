﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.User;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.UserActions
{
    public class GetProfileDetailsHandler : IRequestHandler<ApiActionAuthenticatedRequest<UserProfileGetDetailsInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetProfileDetailsHandler(IDbContextHelper dbContextHelper)
        {
            _dbContext = dbContextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<UserProfileGetDetailsInputModel> request, CancellationToken cancellationToken)
        {

            var user = await (from u in _dbContext.Users
                              where u.UserId == request.UserId
                              select new
                              {
                                  UserId = u.UserId,
                                  UserName = u.UserName,
                                  UserTypeId = (UserTypeEnum)u.UserTypeId
                              }).FirstOrDefaultAsync(cancellationToken);

            if (user == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.UserNotFound);
            }

            return ApiResponse.CreateModel(new UserProfileResponseModel
            {
                UserId = user.UserId,
                UserName = user.UserName,
                UserTypeId = user.UserTypeId
            });
        }
    }
}