using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Commons.Utils;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Notification;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Lagom.Main.ApiAction.NotificationActions
{
    public class GetAllHandler : IRequestHandler<ApiActionAuthenticatedRequest<NotificationGetAllInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public GetAllHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<NotificationGetAllInputModel> request, CancellationToken cancellationToken)
        {
            var query = (from n in _dbContext.Notifications
                         where
                         n.NotifierId == request.UserId &&
                         n.NotificationObject.IsSent
                         select new
                         {
                             n.NotificationId,
                             n.NotificationObject.TitleEn,
                             n.NotificationObject.TitleVi,
                             n.NotificationObject.BodyEn,
                             n.NotificationObject.BodyVi,
                             n.NotificationObject.DataJson,
                             n.NotificationObject.ActualSentAtUtc,
                             n.IsRead
                         })
                         .Distinct();
            // Page info
            var totalItems = query.Count();
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            // Order
            query = query
                .OrderByDescending(a => a.ActualSentAtUtc);

            // Page data
            var notifications = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                notifications.Select(a => new NotificationResponseModel
                {
                    NotificationId = a.NotificationId,
                    Title = request.Lang == LanguageEnum.VN ? a.TitleVi : a.TitleEn,
                    Body = request.Lang == LanguageEnum.VN ? a.BodyVi : a.BodyEn,
                    DataJson = a.DataJson,
                    ActualSentAtUnix = a.ActualSentAtUtc.HasValue ? DateTimeHelper.ToUnixTime(a.ActualSentAtUtc.Value) : null,
                    IsRead = a.IsRead
                }).ToList(),
                requestPaging);
        }
    }
}