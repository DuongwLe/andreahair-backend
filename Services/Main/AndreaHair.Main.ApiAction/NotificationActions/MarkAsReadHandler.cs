using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiErrorMessages;
using AndreaHair.Main.ApiModel.ApiInputModels.Notification;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Lagom.Main.ApiAction.NotificationActions
{
    public class MarkAsReadHandler : IRequestHandler<ApiActionAuthenticatedRequest<NotificationMarkAsReadInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public MarkAsReadHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<NotificationMarkAsReadInputModel> request, CancellationToken cancellationToken)
        {
            var notification = await (from n in _dbContext.Notifications
                                      where
                                      n.NotificationId == request.Input.NotificationId &&
                                      n.NotifierId == request.UserId &&
                                      n.NotificationObject.IsSent
                                      select n)
                                      .FirstOrDefaultAsync(cancellationToken);
            if (notification == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.NotificationNotFound);
            }

            if (!notification.IsRead)
            {
                notification.IsRead = true;
                notification.UpdatedAtUtc = DateTime.UtcNow;
                notification.UpdatedByUserId = request.UserId;

                _dbContext.Update(notification);
                await _dbContext.SaveChangesAsync(cancellationToken);
            }

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}