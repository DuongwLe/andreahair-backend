using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Notification;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.Main.ApiAction.NotificationActions
{
    public class CountUnreadHandler : IRequestHandler<ApiActionAuthenticatedRequest<NotificationUnreadCountInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public CountUnreadHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<NotificationUnreadCountInputModel> request, CancellationToken cancellationToken)
        {
            var count = await _dbContext.Notifications.AsNoTracking()
                .Where(n => n.NotifierId == request.UserId &&
                        n.NotificationObject.IsSent && !n.IsRead)
                .Distinct()
                .CountAsync(cancellationToken);
            return ApiResponse.CreateModel(new NotificationUnreadCountResponseModel
            {
                Count = count
            });
        }
    }
}