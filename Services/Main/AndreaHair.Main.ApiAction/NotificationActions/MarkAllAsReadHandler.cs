using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Notification;
using MediatR;

namespace Lagom.Main.ApiAction.NotificationActions
{
    public class MarkAllAsReadHandler : IRequestHandler<ApiActionAuthenticatedRequest<NotificationMarkAllAsReadInputModel>, IApiResponse>
    {
        private readonly MainDbContext _dbContext;

        public MarkAllAsReadHandler(IDbContextHelper contextHelper)
        {
            _dbContext = contextHelper.GetCurrentMainDbContext();
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<NotificationMarkAllAsReadInputModel> request, CancellationToken cancellationToken)
        {
            await _dbContext.Notifications
                .Where(n => n.NotifierId == request.UserId &&
                        n.NotificationObject.IsSent && !n.IsRead)
                .UpdateFromQueryAsync(n => new Notification
                {
                    IsRead = true,
                    UpdatedAtUtc = DateTime.UtcNow,
                    UpdatedByUserId = null
                }, cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}