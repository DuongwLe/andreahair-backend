﻿using System;
using System.IO;
using AndreaHair.Commons;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace AndreaHair.Main.API
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var configuration = AppSettingBuilder.Build();

            var logFolder = Environment.GetEnvironmentVariable("ANDREAHAIR_LOG_FOLDER");
            if (string.IsNullOrEmpty(logFolder))
            {
                logFolder = AppContext.BaseDirectory;
            }
            logFolder = Path.Combine(logFolder, "api-logs");
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.Async(a => a.File(
                    Path.Combine(logFolder, "main.api..log"),
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff}] [{Level}] {Message}{NewLine}{Exception}"
                ))
                .CreateLogger();

            try
            {
                var host = new WebHostBuilder()
                    .UseConfiguration(configuration)
                    .UseKestrel()
                    .UseUrls($"http://0.0.0.0:{configuration["Port"]}")
                    .UseStartup<Startup>()
                    .Build();

                Log.Information("Starting host...");
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly.");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}

