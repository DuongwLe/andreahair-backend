﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AndreaHair.Main.API.Binders
{
    public class JsonInputBinder<T> : IModelBinder
    {
        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request;
            string body;
            using (StreamReader sr = new(request.Body, Encoding.UTF8))
            {
                body = await sr.ReadToEndAsync();
            }

            var deserializeOptions = new JsonSerializerOptions()
            {
                IgnoreNullValues = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };
            var dataModel = JsonSerializer.Deserialize<T>(body, deserializeOptions);

            TrimAllStringsInObject(dataModel, typeof(T));

            bindingContext.Result = ModelBindingResult.Success(dataModel);
        }

        private static void TrimAllStringsInObject(object obj, Type objType)
        {
            if (obj == null)
            {
                return;
            }
            foreach (var property in objType.GetProperties())
            {
                var propertyType = property.PropertyType;
                if (propertyType == typeof(string))
                {
                    string currentValue = property.GetValue(obj) as string;
                    if (!string.IsNullOrEmpty(currentValue))
                    {
                        property.SetValue(obj, currentValue.Trim());
                    }
                    continue;
                }
                if (propertyType.IsClass)
                {
                    TrimAllStringsInObject(property.GetValue(obj), propertyType);
                }
            }
        }
    }
}