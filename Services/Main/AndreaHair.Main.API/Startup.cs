﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using AndreaHair.Commons;
using AndreaHair.Commons.Constants;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.Core.Helpers.Implements;
using AndreaHair.Core.Hubs;
using AndreaHair.Core.Services;
using AndreaHair.Core.Services.Implements;
using AndreaHair.EntityFramework.MainDb;
using AndreaHair.Main.API.Binders;
using AndreaHair.Main.API.Filters;
using AndreaHair.Main.API.Middlewares;
using AndreaHair.Main.ApiService;
using AndreaHair.Main.ApiService.Implements;
using Hangfire;
using Hangfire.MySql;
using HangfireBasicAuthenticationFilter;
using IdentityModel.AspNetCore.OAuth2Introspection;
using IdentityModel.Client;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Serilog;

namespace AndreaHair.Main.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppSettings = new AppSettings();
            Configuration.Bind(AppSettings);
        }

        public IConfiguration Configuration { get; }
        public AppSettings AppSettings { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration);

            services
                .Configure<RouteOptions>(options =>
                {
                    options.LowercaseUrls = true;
                    options.AppendTrailingSlash = true;
                })
                .Configure((Action<ApiBehaviorOptions>)(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                }));

            services
                .AddMvc(options =>
                {
                    options.EnableEndpointRouting = false;
                    options.MaxModelValidationErrors = 10;
                    options.Filters.Add<ValidateModelFilter>();
                    options.Filters.Add<CustomExceptionResponseFilter>();
                    options.ModelBinderProviders.Insert(0, new ApiInputBinderProvider());
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });

            services
                .AddAuthentication(AppSettings.IdentityServer.DefaultScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = AppSettings.IdentityServer.Authority;
                    options.RequireHttpsMetadata = false;
                    options.ApiName = AppSettings.IdentityServer.ApiName;
                    options.ApiSecret = AppSettings.IdentityServer.ApiSecret;
                    options.EnableCaching = true;
                    options.CacheDuration = TimeSpan.FromMinutes(20);
                    options.TokenRetriever = new Func<HttpRequest, string>(req =>
                    {
                        var fromHeader = TokenRetrieval.FromAuthorizationHeader();
                        var fromQuery = TokenRetrieval.FromQueryString();
                        return fromHeader(req) ?? fromQuery(req);
                    });
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminOnly", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim(OAuthConstants.ClaimTypes.UserType,
                        UserTypeEnum.Admin.ToString("d"),
                        UserTypeEnum.SuperAdmin.ToString("d"));
                });
            });

            services
                .Configure<TokenClientOptions>(options =>
                {
                    options.Address = AppSettings.IdentityServer.TokenEndpoint;
                    options.ClientId = AppSettings.IdentityServer.ClientId;
                    options.ClientSecret = AppSettings.IdentityServer.ClientSecret;
                })
                .AddTransient(sp => sp.GetRequiredService<IOptions<TokenClientOptions>>().Value)
                .AddHttpClient<TokenClient>();

            services
                .AddCors(options =>
                {
                    options.AddPolicy("allowAll", builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            ;
                    });
                })
                .AddResponseCompression();

            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "AndreaHair.Main.API", Version = "v1" });
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "AndreaHair.Main.API.xml"));
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "AndreaHair.Main.ApiModel.xml"));
                    c.OperationFilter<Swashbuckle.AspNetCore.Filters.AppendAuthorizeToSummaryOperationFilter>();
                    c.OperationFilter<Swashbuckle.AspNetCore.Filters.SecurityRequirementsOperationFilter>();
                    c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                    {
                        Description = "Standard Authorization header using the Bearer scheme. Example: \"Bearer {your_access_token}\"",
                        In = ParameterLocation.Header,
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"
                    });
                    c.DescribeAllParametersInCamelCase();
                });

            services
                .AddLogging()
                .AddOptions()
                .AddHttpContextAccessor();

            services
                .AddDbContext<MainDbContext>(options =>
                {
                    options
                        .UseMySql(
                            AppSettings.ConnectionStrings.MainDbConnection,
                            ServerVersion.AutoDetect(AppSettings.ConnectionStrings.MainDbConnection)
                        )
                        .EnableDetailedErrors();
                },
                ServiceLifetime.Scoped);

            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies().First(t => t.GetName().Name == "AndreaHair.Main.ApiAction"));

            // Singleton services
            services
                .Configure<S3Settings>(Configuration.GetSection("S3Settings"))
                .AddSingleton<IS3Service, S3Service>();

            // Scoped services
            services
                .AddScoped<NotificationHub>()
                .AddScoped<ICurrentContext, CurrentHttpContext>()
                .AddScoped<IIdentityHelper, IdentityHelper>()
                .AddScoped<IDbContextHelper, DbContextHelper>()
                .AddScoped<INotificationService, NotificationService>()
                .AddScoped<IPhysicalFileService, PhysicalFileService>();

            services.AddSignalR();

            services
               .AddHangfire(configuration => configuration
                   .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                   .UseSimpleAssemblyNameTypeSerializer()
                   .UseRecommendedSerializerSettings()
                   .UseStorage(
                       new MySqlStorage(
                           AppSettings.ConnectionStrings.HangfireDbConnection,
                           new MySqlStorageOptions
                           {
                               TablesPrefix = "Hangfire"
                           })
               ));

            services.AddHangfireServer();

            // Recurring hangfire jobs
            JobStorage.Current = new MySqlStorage(
                AppSettings.ConnectionStrings.HangfireDbConnection,
                new MySqlStorageOptions { TablesPrefix = "Hangfire" });

            RecurringJob.AddOrUpdate<IPhysicalFileService>(services => services.DeletePhysicalFiles(), Cron.Yearly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (!env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }

            app
                .UseRouting()
                .UseCors("allowAll")
                .UseResponseCompression();

            app.UseAuthentication();
            app.UseAuthorization();

            if (!env.IsProduction())
            {
                app
                    .UseSwagger()
                    .UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AndreaHair.Main.API v1"));

            }

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notifyhub");
            });

            app.UseMiddleware<RequestInputLoggingMiddleware>();

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[]
                {
                     new HangfireCustomBasicAuthenticationFilter
                     {
                         User = AppSettings.HangfireSettings.User,
                         Pass = AppSettings.HangfireSettings.Password
                     }
                 }
            });

            app.UseMvc();
        }
    }
}

