﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Color;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("colors")]
    public class ColorController : BaseController
    {
        public ColorController(IMediator mediator,
          ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] ColorCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="colorId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{colorId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long colorId, [FromBody] ColorUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ColorUpdateInputModel
            {
                ColorId = colorId,
                Details = details
            }));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="colorId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpDelete]
        [Route("{colorId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long colorId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ColorDeleteInputModel
            {
                ColorId = colorId
            }));
        }

        /// <summary>
        /// Get details
        /// </summary>
        /// <param name="colorId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{colorId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ColorResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long colorId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ColorGetDetailsInputModel
            {
                ColorId = colorId
            }));
        }

        /// <summary>
        /// Set enabled
        /// </summary>
        /// <param name="colorId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{colorId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] long colorId, [FromBody] ColorSetEnabledInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ColorSetEnabledInputModel
            {
                ColorId = colorId,
                Details = details
            }));
        }

        /// <summary>
        /// Search paging
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<ColorResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ColorSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }

        /// <summary>
        /// Get all enabled
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("all-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiArrayResponseModel<ColorAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEnabled([FromQuery] string lang)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new ColorGetAllEnabledInputModel
            {
            }, lang));
        }
    }
}