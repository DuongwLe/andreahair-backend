using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Cover;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("covers")]
    public class CoverController : BaseController
    {
        public CoverController(IMediator mediator,
            ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] CoverCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Get details 
        /// </summary>
        /// <param name="coverId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{coverId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<CoverResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long coverId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CoverGetDetailsInputModel
            {
                CoverId = coverId,
            }));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="coverId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{coverId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<CoverResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long coverId, [FromBody] CoverUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CoverUpdateInputModel
            {
                CoverId = coverId,
                Details = details,
            }));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="coverId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpDelete]
        [Route("{coverId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long coverId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CoverDeleteInputModel
            {
                CoverId = coverId,
            }));
        }

        /// <summary>
        /// Set enabled
        /// </summary>
        /// <param name="coverId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{coverId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<CoverResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] long coverId, [FromBody] CoverSetEnabledInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CoverSetEnabledInputModel
            {
                CoverId = coverId,
                Details = details,
            }));
        }

        /// <summary>
        /// Get all enabled
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("all-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiArrayResponseModel<CoverAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEnabled()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new CoverGetAllEnabledInputModel
            {
            }));
        }

        /// <summary>
        /// Search paging 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<CoverResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = ""
        )
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CoverSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }
    }
}
