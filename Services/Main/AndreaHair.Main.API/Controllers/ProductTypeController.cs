﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.ProductType;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("product-types")]
    public class ProductTypeController : BaseController
    {
        public ProductTypeController(IMediator mediator,
          ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] ProductTypeCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{productTypeId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long productTypeId, [FromBody] ProductTypeUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductTypeUpdateInputModel
            {
                ProductTypeId = productTypeId,
                Details = details
            }));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpDelete]
        [Route("{productTypeId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long productTypeId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductTypeDeleteInputModel
            {
                ProductTypeId = productTypeId
            }));
        }

        /// <summary>
        /// Get details
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{productTypeId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ProductTypeResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long productTypeId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductTypeGetDetailsInputModel
            {
                ProductTypeId = productTypeId
            }));
        }

        /// <summary>
        /// Set enabled
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{productTypeId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] long productTypeId, [FromBody] ProductTypeSetEnabledInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductTypeSetEnabledInputModel
            {
                ProductTypeId = productTypeId,
                Details = details
            }));
        }

        /// <summary>
        /// Search paging
        /// </summary>
        /// <param name="details"></param>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<ProductTypeResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromBody] ProductTypeSearchInput details,
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductTypeSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword,
                Details = details
            }));
        }

        /// <summary>
        /// Get all enabled
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("all-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiArrayResponseModel<ProducTypeAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEnabled([FromQuery] long categoryId, [FromQuery] string lang)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new ProductTypeGetAllEnabledInputModel
            {
                CategoryId = categoryId
            }, lang));
        }
    }
}
