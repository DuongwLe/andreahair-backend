﻿using AndreaHair.Core.Helpers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("home")]
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        public HomeController(IMediator mediator,
            ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }
    }
}