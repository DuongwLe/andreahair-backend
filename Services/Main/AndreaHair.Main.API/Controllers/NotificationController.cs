﻿using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Notification;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("notifications")]
    public class NotificationController : BaseController
    {
        public NotificationController(IMediator mediator,
           ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Get all notifications of an user
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<NotificationResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAll(
            string lang,
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new NotificationGetAllInputModel
            {
                PageNumber = pageNumber,
                PageSize = pageSize
            }, lang));
        }

        /// <summary>
        /// Mark a notification as read
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{notificationId}/read")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> MarkAsRead([FromRoute] long notificationId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new NotificationMarkAsReadInputModel
            {
                NotificationId = notificationId
            }));
        }

        /// <summary>
        /// Mark all notification as read
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("read-all")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> MarkAllAsRead()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new NotificationMarkAllAsReadInputModel()));
        }

        /// <summary>
        /// Count all unread notifications of an user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("unread-count")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<NotificationUnreadCountResponseModel>), (int)HttpStatusCode.OK)]
        [ProducesDefaultResponseType(typeof(ApiResponseModel))]
        public async Task<IActionResult> CountAllUnread()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new NotificationUnreadCountInputModel()));
        }
    }
}
