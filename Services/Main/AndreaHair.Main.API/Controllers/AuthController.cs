﻿using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Auth;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [AllowAnonymous]
    [Route("auth")]
    [ApiController]
    public class AuthController : BaseController
    {
        public AuthController(IMediator mediator,
            ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        ///// <summary>
        ///// Create new account
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route("register")]
        //[Produces("application/json")]
        //[ProducesResponseType((int)HttpStatusCode.OK)]
        //public async Task<IActionResult> Create([FromBody] AuthRegisterInputModel input)
        //{
        //    return await _mediator.Send(ApiActionModel.CreateRequest(input));
        //}

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<AuthLoginResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Login([FromBody] AuthLoginInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(input));
        }

        /// <summary>
        /// Refresh new token
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("refresh-token")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<UserTokenResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> RefreshToken([FromBody] AuthRefreshTokenInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(input));
        }
    }
}

