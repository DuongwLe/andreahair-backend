﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Supporter;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("supporters")]
    public class SupporterController : BaseController
    {
        public SupporterController(IMediator mediator,
          ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Get all enabled
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("all-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<SupporterResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEnabled(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new SupporterGetAllEnabledInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber
            }));
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] SupporterCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="supporterId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{supporterId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long supporterId, [FromBody] SupporterUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new SupporterUpdateInputModel
            {
                SupporterId = supporterId,
                Details = details
            }));
        }

        /// <summary>
        /// Get details
        /// </summary>
        /// <param name="supporterId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{supporterId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<SupporterResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long supporterId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new SupporterGetDetailsInputModel
            {
                SupporterId = supporterId
            }));
        }

        /// <summary>
        /// Search paging
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<SupporterResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new SupporterSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="supporterId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpDelete]
        [Route("{supporterId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long supporterId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new SupporterDeleteInputModel
            {
                SupporterId = supporterId
            }));
        }

        /// <summary>
        /// Set enabled
        /// </summary>
        /// <param name="supporterId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{supporterId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] long supporterId, [FromBody] SupporterSetEnabledInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new SupporterSetEnabledInputModel
            {
                SupporterId = supporterId,
                Details = details
            }));
        }
    }
}
