﻿using System;
using AndreaHair.Core.Helpers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [Authorize]
    public class BaseController : ControllerBase
    {
        protected readonly IMediator _mediator;
        protected ICurrentContext _currentContext;

        public BaseController(IMediator mediator, ICurrentContext currentContext)
        {
            _mediator = mediator;
            _currentContext = currentContext;
        }

        protected Guid UserId => _currentContext.UserId.Value;
    }
}

