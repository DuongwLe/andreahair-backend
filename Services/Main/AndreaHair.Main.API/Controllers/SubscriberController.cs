﻿using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Subcriber;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("subscribers")]
    public class SubscriberController : BaseController
    {
        public SubscriberController(IMediator mediator,
            ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Subcribe([FromBody] SubscriberCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(input));
        }

        /// <summary>
        /// Get all subscriber
        /// </summary>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<SubscriberResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new SubscriberGetAllInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber
            }));
        }
    }
}