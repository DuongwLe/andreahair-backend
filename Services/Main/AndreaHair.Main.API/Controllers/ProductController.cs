﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AndreaHair.Commons.Enums;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Product;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("products")]
    public class ProductController : BaseController
    {
        public ProductController(IMediator mediator,
          ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] ProductCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Get best selling products
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("best-selling")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<ProductBestSellingResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetBestSelling(
            string lang,
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new ProductGetBestSellingInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber
            }, lang));
        }

        /// <summary>
        /// Get details
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("{productId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ProductDetailsResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long productId, [FromQuery] string lang)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new ProductGetDetailsInputModel
            {
                ProductId = productId
            }, lang));
        }

        /// <summary>
        /// Get all enabled with paging
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderByTypeId"></param>
        /// <param name="details"></param>
        /// <param name="lang"></param>
        ///<returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("all-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<ProductAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEnabled(
            [FromBody] ProductGetAllEnabledInput details,
            [FromQuery][Range(1, 100)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "",
            [FromQuery] string lang = "",
            OrderByTypeEnum orderByTypeId = 0,
            [StringLength(128)] string orderBy = ""
            )
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new ProductGetAllEnabledInputModel
            {
                Keyword = keyword,
                PageSize = pageSize,
                PageNumber = pageNumber,
                OrderBy = orderBy,
                OrderByTypeId = orderByTypeId,
                Details = details
            }, lang));
        }

        /// <summary>
        /// Get details for admin
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{productGroupId}/admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ProductDetailsForAdminResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetailsAdmin([FromRoute] long productGroupId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductGetDetailsForAdminInputModel
            {
                ProductGroupId = productGroupId
            }));
        }

        /// <summary>
        /// Search paging
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<ProductResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }

        /// <summary>
        /// Get related product
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="productId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("{productId}/related")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ProductAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetRelated(
            string lang,
            [FromRoute] long productId,
            [FromQuery][Range(1, 100)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new ProductGetRelatedInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                ProductId = productId
            }, lang));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpDelete]
        [Route("{productGroupId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long productGroupId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductDeleteInputModel
            {
                ProductGroupId = productGroupId
            }));
        }

        /// <summary>
        /// Set enabled
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{productGroupId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] long productGroupId, [FromBody] ProductSetEnabledInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductSetEnabledInputModel
            {
                ProductGroupId = productGroupId,
                Details = details
            }));
        }

        /// <summary>
        /// Update product
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{productGroupId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long productGroupId, [FromBody] ProductUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ProductUpdateInputModel
            {
                ProductGroupId = productGroupId,
                Details = details
            }));
        }
    }
}
