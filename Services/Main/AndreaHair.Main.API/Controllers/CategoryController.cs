﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Category;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("categories")]
    public class CategoryController : BaseController
    {
        public CategoryController(IMediator mediator,
          ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] CategoryCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Search paging
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<CategoryResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new CategorySearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }

        /// <summary>
        /// Get all enabled
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("all-enabled")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiArrayResponseModel<CategoryAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllEnabled([FromQuery] string lang)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new CategoryGetAllEnabledInputModel
            {
            }, lang));
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{categoryId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long categoryId, [FromBody] CategoryUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CategoryUpdateInputModel
            {
                CategoryId = categoryId,
                Details = details
            }));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpDelete]
        [Route("{categoryId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long categoryId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CategoryDeleteInputModel
            {
                CategoryId = categoryId
            }));
        }

        /// <summary>
        /// Get details
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{categoryId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<CategoryResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long categoryId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CategoryGetDetailsInputModel
            {
                CategoryId = categoryId
            }));
        }

        /// <summary>
        /// Set enabled
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [Authorize("AdminOnly")]
        [HttpPut]
        [Route("{categoryId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] long categoryId, [FromBody] CategorySetEnabledInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new CategorySetEnabledInputModel
            {
                CategoryId = categoryId,
                Details = details
            }));
        }

        /// <summary>
        /// Get all with product types
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("product-types")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<CategoryAllEnabledResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllWithProductTypes([FromQuery] string lang)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(new CategoryGetAllProductTypesInputModel
            {
            }, lang));
        }
    }
}