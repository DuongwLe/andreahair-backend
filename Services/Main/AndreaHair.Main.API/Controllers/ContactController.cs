﻿using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Contact;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("contacts")]
    public class ContactController : BaseController
    {
        public ContactController(IMediator mediator,
            ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Contact andreahair
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] ContactCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(input));
        }

        /// <summary>
        /// Search paging
        /// </summary>
        /// <return></return>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<ContactResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromQuery][Range(1, 50)] int pageSize = 50,
            [FromQuery][Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ContactSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber
            }));
        }

        /// <summary>
        /// Get details
        /// </summary>
        /// <return></return>
        [Authorize("AdminOnly")]
        [HttpGet]
        [Route("{contactId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ContactResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] long contactId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new ContactGetDetailsInputModel
            {
                ContactId = contactId
            }));
        }
    }
}

