﻿using System.Net;
using System.Threading.Tasks;
using AndreaHair.Core.Helpers;
using AndreaHair.Main.ApiAction;
using AndreaHair.Main.ApiModel;
using AndreaHair.Main.ApiModel.ApiInputModels.Auth;
using AndreaHair.Main.ApiModel.ApiInputModels.User;
using AndreaHair.Main.ApiModel.ApiResponseModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndreaHair.Main.API.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : BaseController
    {
        public UserController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Get Profile Details
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("my-profile")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<UserProfileResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ProfileDetail()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new UserProfileGetDetailsInputModel()));
        }
    }
}
