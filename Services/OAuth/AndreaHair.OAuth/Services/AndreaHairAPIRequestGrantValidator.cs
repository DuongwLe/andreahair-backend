﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AndreaHair.Commons.Constants;
using AndreaHair.EntityFramework.MainDb;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.EntityFrameworkCore;

namespace AndreaHair.OAuth.Services
{
    public class AndreaHairAPIRequestGrantValidator : IExtensionGrantValidator
    {
        private readonly MainDbContext _dbContext;
        private readonly ITokenValidator _validator;

        public AndreaHairAPIRequestGrantValidator(ITokenValidator validator, MainDbContext dbContext)
        {
            _validator = validator;
            _dbContext = dbContext;
        }

        public string GrantType => OAuthConstants.ExtendGrantType.ApiRequest;

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Raw.Get("userId")) || !Guid.TryParse(context.Request.Raw.Get("userId"), out var userId))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, OAuthConstants.ErrorMessage.CannotFindUser);
                return;
            }
            var user = await (from u in _dbContext.Users
                              where u.UserId == userId
                              select u).FirstOrDefaultAsync();
            if (user == null)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, OAuthConstants.ErrorMessage.CannotFindUser);
                return;
            }

            var customClaims = new List<Claim>()
            {
                new Claim(OAuthConstants.ClaimTypes.UserId, user.UserId.ToString()),
                new Claim(OAuthConstants.ClaimTypes.UserType, user.UserTypeId.ToString())
            };

            context.Result = new GrantValidationResult(
                subject: user.UserName,
                authenticationMethod: GrantType,
                claims: customClaims,
                identityProvider: "local",
                customResponse: new Dictionary<string, object>() { { "userId", user.UserId } }
            );
        }
    }
}