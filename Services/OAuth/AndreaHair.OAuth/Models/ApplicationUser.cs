﻿using Microsoft.AspNetCore.Identity;

namespace AndreaHair.OAuth.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}