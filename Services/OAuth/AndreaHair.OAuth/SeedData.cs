﻿using System.Collections.Generic;
using System.Linq;
using AndreaHair.Commons.Constants;
using IdentityServer4;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.Storage;
using IdentityServer4.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace AndreaHair.OAuth
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddOperationalDbContext(options =>
            {
                options.ConfigureDbContext = db => db.UseMySql(connectionString,
                    ServerVersion.AutoDetect(connectionString),
                    sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });
            services.AddConfigurationDbContext(options =>
            {
                options.ConfigureDbContext = db => db.UseMySql(connectionString,
                    ServerVersion.AutoDetect(connectionString),
                    sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });

            var serviceProvider = services.BuildServiceProvider();

            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<PersistedGrantDbContext>().Database.Migrate();

                var context = scope.ServiceProvider.GetService<ConfigurationDbContext>();
                context.Database.Migrate();
                EnsureSeedData(context);
            }
        }

        private static void EnsureSeedData(ConfigurationDbContext context)
        {
            if (!context.IdentityResources.Any())
            {
                Log.Debug("IdentityResources being populated");
                foreach (var resource in Config.IdentityResources.ToList())
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {
                Log.Debug("IdentityResources already populated");
            }

            if (!context.ApiScopes.Any())
            {
                Log.Debug("ApiScopes being populated");
                foreach (var scope in Config.ApiScopes.ToList())
                {
                    context.ApiScopes.Add(scope.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {
                Log.Debug("ApiScopes already populated");
            }

            //if (!context.ApiResources.Any())
            //{
            //    Log.Debug("ApiResources being populated");
            //    foreach (var resource in Config.ApiResources.ToList())
            //    {
            //        context.ApiResources.Add(resource.ToEntity());
            //    }
            //    context.SaveChanges();
            //}
            //else
            //{
            //    Log.Debug("ApiResources already populated");
            //}

            //if (!context.Clients.Any())
            //{
            //    Log.Debug("Clients being populated");
            //    foreach (var client in Config.Clients.ToList())
            //    {
            //        context.Clients.Add(client.ToEntity());
            //    }
            //    context.SaveChanges();
            //}
            //else
            //{
            //    Log.Debug("Clients already populated");
            //}
        }
    }

    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("main.read_write", "Full access to Main API"),
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource()
                {
                    Name = "andreahair_main",
                    DisplayName = "AndreaHair Main API",
                    Description = "Full access to AndreaHair Main API",
                    ApiSecrets = { new Secret("rxWIhjSdWa7XkCwXKkBAoiDHx9MKP44x".Sha256()) },
                    Enabled = true,
                    Scopes = { "main.read_write" },
                }
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "andreahair_default",
                    ClientName = "AndreaHair Default Client",
                    AllowedGrantTypes = new List<string> { GrantType.ClientCredentials, GrantType.ResourceOwnerPassword, OAuthConstants.ExtendGrantType.ApiRequest },
                    ClientSecrets = { new Secret("4IZclCoQ5TzJPXI5svhvLQGhOv8k3AQq".Sha256()) },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "main.read_write" },
                    AccessTokenType = AccessTokenType.Reference,
                    AllowOfflineAccess = true,
                    RequireClientSecret = true,
                }
            };
    }
}