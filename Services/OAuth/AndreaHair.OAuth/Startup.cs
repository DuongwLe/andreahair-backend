﻿using System;
using AndreaHair.Commons;
using AndreaHair.Commons.Constants;
using AndreaHair.EntityFramework;
using AndreaHair.OAuth.Services;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.Extensions.Hosting;
using AndreaHair.EntityFramework.MainDb;

namespace AndreaHair.OAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppSettings = new AppSettings();
            Configuration.Bind(AppSettings);
        }

        public IConfiguration Configuration { get; }
        public AppSettings AppSettings { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RouteOptions>(options =>
            {
                options.LowercaseUrls = true;
                options.AppendTrailingSlash = true;
            });
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });
            services.AddLogging();
            services.AddOptions();

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            services.AddIdentityServer()
                .AddSigningCredential(GetCertificate(AppSettings))
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseMySql(AppSettings.ConnectionStrings.IdentityDbConnection,
                            ServerVersion.AutoDetect(AppSettings.ConnectionStrings.IdentityDbConnection),
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseMySql(AppSettings.ConnectionStrings.IdentityDbConnection,
                            ServerVersion.AutoDetect(AppSettings.ConnectionStrings.IdentityDbConnection),
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    options.EnableTokenCleanup = true;
                    //options.TokenCleanupInterval = 30;
                })
                .AddInMemoryCaching()
                .AddClientStoreCache<IdentityServer4.EntityFramework.Stores.ClientStore>()
                .AddConfigurationStoreCache()
                .AddResourceStoreCache<IdentityServer4.EntityFramework.Stores.ResourceStore>()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddExtensionGrantValidator<AndreaHairAPIRequestGrantValidator>()
                .AddProfileService<ProfileService>();

            services.AddDbContext<MainDbContext>(
                options => options.UseMySql(
                    AppSettings.ConnectionStrings.MainDbConnection,
                    ServerVersion.AutoDetect(AppSettings.ConnectionStrings.MainDbConnection)
                )
                .EnableDetailedErrors(),
                ServiceLifetime.Scoped
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            // ref: https://github.com/IdentityServer/IdentityServer4/issues/1331#issuecomment-317049214
            var forwardOptions = new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                RequireHeaderSymmetry = false
            };
            forwardOptions.KnownNetworks.Clear();
            forwardOptions.KnownProxies.Clear();
            app.UseForwardedHeaders(forwardOptions);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();
            app.UseIdentityServer();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private X509Certificate2 GetCertificate(AppSettings appConfig)
        {
            var rawData = new byte[1024];
            var password = appConfig.SigningCredential.CertificateFilePassword;
            switch (appConfig.SigningCredential.CertificateFileLocation)
            {
                //case AppConstants.ConfigurationLocation.S3:
                //    var fileKey = appConfig.SigningCredential.CertificateFilePath;
                //    if (!string.IsNullOrEmpty(fileKey))
                //    {
                //        rawData = AppSettingBuilder.ReadS3ByteArrayAsync(fileKey).Result;
                //    }
                //    break;
                case AppConstants.ConfigurationLocation.LocalMachine:
                    var filepath = appConfig.SigningCredential.CertificateFilePath;
                    if (!string.IsNullOrEmpty(filepath))
                    {
                        rawData = File.ReadAllBytes(filepath);
                    }
                    break;
                default:
                    break;
            }

            return new X509Certificate2(rawData, password);
        }
    }
}

