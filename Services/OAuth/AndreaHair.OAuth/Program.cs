﻿using System;
using System.IO;
using System.Linq;
using AndreaHair.Commons;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace AndreaHair.OAuth
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var configuration = AppSettingBuilder.Build();

            var logFolder = Environment.GetEnvironmentVariable("ANDREAHAIR_LOG_FOLDER");
            if (string.IsNullOrEmpty(logFolder))
            {
                logFolder = AppContext.BaseDirectory;
            }
            logFolder = Path.Combine(logFolder, "oauth-logs");
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.Async(a => a.File(
                    Path.Combine(logFolder, "oauth.api..log"),
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff}] [{Level}] {Message}{NewLine}{Exception}"
                ))
                .CreateLogger();

            try
            {
                var host = new WebHostBuilder()
                    .UseConfiguration(configuration)
                    .UseKestrel()
                    .UseUrls($"http://0.0.0.0:{configuration["Port"]}")
                    .UseStartup<Startup>()
                    .Build();

                //
                // IMPORTANT:
                // 1. Make migration files at code time.
                // 2. Generate oauth-cert
                // 3. Run seeding database at the very first time of launching OAuth service: dotnet run /seed
                // See http://docs.identityserver.io/en/latest/quickstarts/5_entityframework.html#configuring-the-stores
                // See https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs
                //
                var seed = args.Contains("/seed");
                if (seed)
                {
                    args = args.Except(new[] { "/seed" }).ToArray();
                }

                if (seed)
                {
                    Log.Information("Seeding database...");
                    var connectionString = configuration.GetConnectionString("IdentityDbConnection");
                    SeedData.EnsureSeedData(connectionString);
                    Log.Information("Done seeding database.");
                    return 0;
                }

                Log.Information("Starting host...");
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly.");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}